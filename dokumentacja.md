---
title: Dokumentacja do pobrania
layout: page
permalink: "/dokumentacja/index.html"
ref: documentation
lang: pl
sortkey: 2
---

## Medyczne

### Diagnozy

1. <a href="/pliki/Medyczne/Badania_Genetyczne_20071123.pdf" target="_blank">Wyniki badań genetycznych, wykonanych 23 listopada 2007 r.</a>

2. <a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">Diagnozy i zalecenia wydane przez lek. medycyny prowadzącego kuracje na kandydozę w 2013 r, podczas wizyt w maju, listopadzie oraz grudniu 2013 r.</a>

3. Wyniki badań USG ukł. moczowego (zdjęcia, opis), wykonanych 29 stycznia 2014 r., zaraz po wydarzeniach
   * <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">Wersja w j. polskim, oryginał</a>
   * <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">Wersja w j. angielskim, tłumaczenie przysięgłe</a>

4. Wyniki badań USG ukł. moczowego (zdjęcia, opis), wykonanych 11 stycznia 2016 r.
   * <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">Wersja w j. polskim, tłumaczenie przysięgłe</a>
   * <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">Wersja w j. angielskim, oryginał</a>

5. Wyniki badań tomografii komputerowej, wykonanych 13 lutego 2018 r.
   * <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">Wersja w j. polskim, tłumaczenie przysięgłe</a>
   * <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">Wersja w j. niemieckim, oryginał</a>

6. Wyniki badań rezonansu magnetycznego ukł. nerwowego, wykonanych 12 marca 2018 r.
   * <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">Wersja w j. polskim, tłumaczenie przysięgłe</a>
   * <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank"> Wersja w j. angielskim, oryginał</a>

7. Wyniki badań USG ukł. nerwowego, wykonanych 6 kwietnia 2018 r.
   * <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">Wersja w j. polskim, tłumaczenie przysięgłe</a>
   * <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">Wersja w j. angielskim, oryginał</a>

8. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_Optical-Coherence-Tomography.pdf" target="_blank">Kopia podsumowania z badania Optycznej Koherentnej Tomografii (OCT) blizn po ranach kłutych, wykonanego dnia 6 kwietnia 2018 r.</a>

9. Orzeczenie lekarskie lekarza specjalisty neurologii i chirurgii plastycznej z konsultacji i badań wykonanych 6 i 20 kwietnia 2018 r.
   * <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">Wersja w j. polskim, tłumaczenie przysięgłe</a>
   * <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">Wersja w j. angielskim, oryginał</a>


### Obrazowanie

1. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_USG-of-nerves.pdf" target="_blank">Kopia wybranych zdjęć z badań USG ukł. nerwowego, wykonanych 6 kwietnia 2018 r.</a>

2. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_Neurologist-PlasticSurgeon_Clinical-Images.pdf" target="_blank">Kopia obrazów klinicznych z badań i konsultacji z lekarzem specjalista neurologii i chirurgii plastycznej, wykonane dnia 6 kwietnia 2018 r.</a>

3. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_MR-Neurography-of-Pelvis.pdf" target="_blank">Kopia wybranych klatek z danych DICOM badań rezonansu magnetycznego ukł. nerwowego, wykonanych 12 marca 2018 r.</a>

4. Dane DICOM z badań MRI i CT, wykonanych Mahajan Imaging w Delhi, Indiach, dnia 12 Marca 2018 r.

   * pCloud: <a href="https://my.pcloud.com/publink/show?code=XZWdP4kZa1akm5mWcsjAFEDlDwTpJB1tf1dX" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>

   * Jottacloud: <a href="https://www.jottacloud.com/s/2105c23f755f08544c58e6221888b17226f" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>

   * Google Drive: <a href="https://drive.google.com/open?id=1ZJjPMAHRr4Xbiz3LdpYl2SaocqGHHPFc" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>

   * MD5 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Mahajan-Imaging_MRI&CT_DICOM.zip.md5" target="_blank">e64a190463f0a2946cf2136690f95839</a>

   * SHA256 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Mahajan-Imaging_MRI&CT_DICOM.zip.sha256" target="_blank">add66c70beeeeb6536d6c1186b8882a0d02a2e21dbbc28588927f5818e61ba1c</a>


5. Dane DICOM z badania CT, wykonanego w Radiology Center Vienna w Wiedniu, Austrii, dnia 13 lutego 2018 r.

   * pCloud: <a href="https://my.pcloud.com/publink/show?code=XZGPP4kZ3zAExfdiPcbMlkCqctU3w7nh5Kk0" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>

   * Jottacloud: <a href="https://www.jottacloud.com/s/210c260e889cc9b479baa41564db371a271" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>

   * Google Drive: <a href="https://drive.google.com/open?id=1HnTyAGHQHLJkkGGCzoLhZUBNjfYRAFnE" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>

   * MD5 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Radiology-Center-Vienna_CT_DICOM.zip.md5" target="_blank">4eaba114782a2c209d2263384c9b54b2</a>

   * SHA256 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Radiology-Center-Vienna_CT_DICOM.zip.sha256" target="_blank">190f44f82a8f1c20f117ef6c2f48105ad428c811682f64ad72c9bafdeb904537</a>


### Zaświadczenia

1. <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">Zaświadczenie, że kandydoza nie jest przyczyna zaobserwowanych obrażeń ciała i uszkodzenia ukł. moczowo-płciowego, wystawione przez lek. medycyny prowadzącego kuracje na kandydozę w 2013 r.</a>

### Turystyka medyczna

#### Indie (2018)

1. <a href="/pliki/Medyczne/Turystyka/medical_tourism_india_visa_201802.pdf" target="_blank">Wiza medyczna do Indii (02.2018)</a>

2. <a href="/pliki/Medyczne/Turystyka/medical_tourism_india_plane_20180227.pdf" target="_blank">Przelot do Delhi (27.02.2018)</a>

3. <a href="/pliki/Medyczne/Turystyka/medical_tourism_india_plane_20180316.pdf" target="_blank">Przelot z Delhi (16.03.2018)</a>


#### Austria (2018)

1. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180211.pdf" target="_blank">Przejazd do Wiednia (11.02.2018)</a>

2. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180214.pdf" target="_blank">Przejazd z Wiednia (14.02.2018)</a>

3. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180405.pdf" target="_blank">Przejazd do Wiednia (05.04.2018)</a>

4. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_train_20180407.pdf" target="_blank">Przejazd z Wiednia (07.04.2018)</a>

5. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180419.pdf" target="_blank">Przejazd do Wiednia (19.04.2018)</a>

6. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180420.pdf" target="_blank">Przejazd z Wiednia (20.04.2018)</a>


### Zapytania dot. wykonania badań i opinii medyczno-sądowej w Polsce (2017 r.)

1. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20170622.pdf" target="_blank">Wiadomość z dnia 22 czerwca 2017 r.</a>

2. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20170626.pdf" target="_blank">Wiadomość z dnia 26 czerwca 2017 r.</a>

3. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171018.pdf" target="_blank">Wiadomość z dnia 18 października 2017 r.</a>

4. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171020.1.pdf" target="_blank">Wiadomość z dnia 20 października 2017 r.</a>

5. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171020.2.pdf" target="_blank">Wiadomość z dnia 20 październik 2017 r.</a>

6. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171023.pdf" target="_blank">Wiadomość z dnia 23 październik 2017 r.</a>

7. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171026.pdf" target="_blank">Wiadomość z dnia 26 października 2017 r.</a>

8. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171103.pdf" target="_blank">Wiadomość z dnia 3 listopada 2017 r.</a>

9. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171110.pdf" target="_blank">Wiadomość z dnia 10 listopada 2017 r.</a>

10. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171114.1.pdf" target="_blank">Wiadomość z dnia 14 listopada 2017 r.</a>

11. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171114.2.pdf" target="_blank">Wiadomość z dnia 14 listopada 2017 r.</a>

12. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171115.pdf" target="_blank">Wiadomość z dnia 15 listopada 2017 r.</a>

13. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171122.pdf" target="_blank">Wiadomość z dnia 22 listopada 2017 r.</a>

14. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20180222.pdf" target="_blank">Wiadomość z dnia 22 lutego 2018 r.</a>


### Zaniedbania

#### CM Medicover (01/2014)

1. <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_16-01-2014.pdf" target="_blank">Kopia odpisu z wizyty w CM Medicover w dniu 16.01.2014</a>

2. <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_22-01-2014.pdf" target="_blank">Kopia odpisu wizyty w CM Medicover w dniu 22.01.2014</a>


#### CM iMed24 (12/2014)

1. <a href="/pliki/Medyczne/MHSiemaszko_Uromed_skierowanie_Rezonans-Magnetyczny_20141120.pdf" target="_blank">Kopia skierowania na badania rezonansu magnetycznego **MIEDNICY** (całej miednicy, wraz z nerkami i bliznami w okolicy pachwiny!), z dnia 20 listopada 2014</a>

2. <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy_rachunek_20141212.pdf" target="_blank">Kopia rachunku za badania rezonansu magnetycznego **MIEDNICY MNIEJSZEJ**, wykonane w CM iMed24 dnia 12 grudnia 2014</a>

3. <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy-mniejszej-nie-calej_BZDURA_raport_20141212.pdf" target="_blank">Kopia wyników badań rezonansu magnetycznego **MIEDNICY MNIEJSZEJ**, wykonanych w CM iMed24 dnia 12 grudnia 2014</a>


#### Praga (12/2015)

1\. Kopia pozwu o zaniedbanie medyczne vs EPS i inni, z dnia 3.12.2018

 * <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf" target="_blank">Wersja w j. angielskim</a>

 * <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf" target="_blank">Wersja w j. czeskim</a>


2\. Kopia wezwania do zapłaty z dnia 28.01.2018

 * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">Wersja w j. polskim, tłumaczenie</a>

 * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160128.oryginal.pdf" target="_blank">Wersja w j. czeskim, oryginał</a>


3\. Kopia wezwania do zapłaty z dnia 29.02.2018

 * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160229.oryginal.pdf" target="_blank">Wersja w j. czeskim, oryginał</a>


#### Delhi (3/2018)

1. <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf" target="_blank">Kopia pozwu o zaniedbanie medyczne vs Apollo i inni, z sierpnia 2018 (**pozew ugodowo rozstrzygnięty!**)</a>

2. <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf" target="_blank">Kopia odpowiedzi na sprzeciw ze stycznia 2019 r.</a>


### Inne

 * <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_symptoms_infographic.pdf" target="_blank">Kopia infografiki obrazującej obrażenia ciała</a>

<hr class="thin-separator" />

## Prawa Człowieka

### ETPC (2019/05)

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG.20190521.pdf" target="_blank">Formularz zgłoszeniowy</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_Statement-of-the-facts_Annex.20190521.pdf" target="_blank">Załącznik do formularza zgloszeniowego, sekcji E</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_List-of-accompanying-documents_Annex.20190521.pdf" target="_blank">Załącznik do formularza zgloszeniowego, sekcji I</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-postage_20190521.pdf" target="_blank">Potwierdzenie nadania przesylki</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-fax-transmission_20190522.pdf" target="_blank">Potwierdzenie transmisji faksu</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Email_20190521.pdf" target="_blank">Kopia wiadomosci email</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-delivery_20190603.pdf" target="_blank">Potwierdzenie odbioru przesylki</a>

* <a href="/etpc-skarga-201905-zawartosc-przesylki/index.html" target="_blank">Komplet zdjęć (wraz z oryginalnymi metadanymi EXIF) dokumentujących zawartość przesyłki</a>


### RPO (2018/07/19)

* <a href="/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf" target="_blank">Wystąpienie Zespołu Prawa Karnego Biura Rzecznika Praw Obywatelskich do Prokuratora Regionalnego w Krakowie, z dnia 19 lipca 2018 r.</a>

<hr class="thin-separator" />

## Prawne
### Złożone

#### 2019

1. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rb.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Okręgowej w Krakowie Rafała Babińskiego", z dnia 6 maja 2019 r.</a>

2. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_db.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Prądnik Biały Dorotę Bojanowską", z dnia 6 maja 2019 r.</a>

3. <a href="/pliki/Prawne/falszowanie-nieprawda/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_falszowanie-nieprawda.pdf" target="_blank">"Zawiadomienie o prawdopodobieństwu popełnienia przestępstwa z art. 270 § 1 oraz art. 276 Kodeksu Karnego", z dnia 6 maja 2019 r.</a>

4. <a href="/pliki/Prawne/DdSPZiK/II-Kp-9-19-K_zgloszenie-do-DdSPZiK.20190220.pdf" target="_blank">"Wniosek o podjęcie interwencji" skierowany do Departamentu do Spraw Przestępczości Zorganizowanej i Korupcji dnia 20 lutego 2019 r.</a>

5. <a href="/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 156 §1, art. 158 §, art. 159, art. 193, art. 233 § 1, art. 234, art. 236 § 1, art. 238, art. 239 § 1, art. 258 § 1 Kodeksu Karnego przez p. Monikę Hudykę", z dnia 6 lutego 2019 r.</a>

6. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Prądnik Biały Beatę Sichelską", z dnia 4 lutego 2019 r.</a>

7. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez st. sierż. Komisariatu III Policji w Krakowie Monikę Górowską", z dnia 4 lutego 2019 r.</a>

8. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez asp. Komisariatu III Policji w Krakowie Roberta Cygana", z dnia 4 lutego 2019 r.</a>

9. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Prądnik Biały Tomasza Pawlika", z dnia 4 lutego 2019 r.</a>

10. <a href="/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez st. post. Komisariatu IV Policji w Krakowie Darię Curzydło", z dnia 2 stycznia 2019 r.</a>


#### 2018

1. <a href="/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf" target="_blank">"Zażalenie na postanowienie Prokuratora Rejonowego w Tarnowie z dnia 22 listopada 2018 r., wydane w sprawie o sygn. akt. PR 4 DS360.2018 w przedmiocie odmowy wszczęcia śledztwa, doręczone w dniu 20 grudnia 2018 r.", z dnia 27 grudnia 2018 r.</a>

2. <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20181211.pdf" target="_blank">"Wniosek o podjęcie interwencji" skierowany do Ministra Sprawiedliwości/Prokuratora Generalnego RP dnia 11 grudnia 2018 r.</a>

3. <a href="/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 156 §1, art. 157 §1, art. 160 §1, art. 162 §1, art. 192 §1, art. 193, art. 239, art. 258 oraz art. 268 Kodeksu Karnego, równocześnie naruszenie art. 30, art. 32, art. 38, art. 39, art. 40, art. 45, art. 47 oraz art. 77 Konstytucji RP, w zw. z art. 44 Konstytucji RP, jak również art. 2, art. 3, art. 6, art. 13 oraz art. 14 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności, w zw. z art. 9 Konstytucji RP" z dnia 11 grudnia 2018 r.</a>

4. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Okręgowej w Krakowie Marię Zębalę", z dnia 12 lipca 2018 r.</a>

5. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Krowodrza w Krakowie Małgorzatę Lipską", z dnia 12 lipca 2018 r.</a>

6. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Krowodrza w Krakowie Edytę Kulik", z dnia 12 lipca 2018 r.</a>

7. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf" target="_blank">"Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Krowodrza w Krakowie Bartłomieja Legutko", z dnia 12 lipca 2018 r.</a>

8. <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-rpo_20180613.pdf" target="_blank">"Wniosek o podjęcie interwencji w sprawie bezprawnych działań i zaniechań przez Prokuraturę Okręgową w Krakowie i Prokuraturę Rejonową Kraków-Krowodrza w Krakowie" skierowany do Rzecznika Praw Obywatelskich dnia 13 czerwca 2018 r.</a>

9. <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20180613.pdf" target="_blank">"Wniosek o interwencję w sprawie bezprawnych działań i zaniechań przez Prokuraturę Okręgową w Krakowie i Prokuraturę Rejonową Kraków-Krowodrza w Krakowie" skierowany do Ministra Sprawiedliwości/Prokuratora Generalnego RP dnia 13 czerwca 2018 r.</a>

10. <a href="/pliki/Prawne/Zlozone/mhs_skarga-na-przewleklosc-postepowania-karnego-przygotowawczego_20180605.pdf" target="_blank">"Skarga na naruszenie prawa strony do rozpoznania sprawy w postępowaniu karnym przygotowawczym bez nieuzasadnionej zwłoki”, z dnia 5.06.2018 r.</a>

11. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_bl.20180511.zlozone.pdf" target="_blank">Kopia pisma "Skarga na Prokuratora Bartłomieja Legutko" z dnia 11.05.2018</a>

12. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie_uzupelnienie.20180507.zlozone.pdf" target="_blank">Kopia pisma "Uzupełnienie wniosku o wyznaczenie do prowadzenia postępowania przez jednostkę spoza obszaru właściwości Prokuratury Rejonowej Kraków-Krowodrza w Krakowie" z dnia 7.05.2018</a>

13. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180504.zlozone.pdf" target="_blank">Kopia odpowiedzi na pismo z Prokuratury Rejonowej Kraków-Krowodrza w Krakowie z dnia 04.05.2018</a>

14. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie.20180417.zlozone.pdf" target="_blank">Kopia pisma "Wniosek o wyznaczenie do prowadzenia postępowania przez jednostkę spoza obszaru właściwości Prokuratury Rejonowej Kraków-Krowodrza w Krakowie .." z dnia 17.04.2018</a>

15. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_ek.20180413.zlozone.pdf" target="_blank">Kopia pisma "Skarga na prokuratora Edytę Kulik" z dnia 13.04.2018</a>

16. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-udzielenie-informacji.20180404.zlozone.pdf" target="_blank">Kopia pisma "Wniosek o udzielenie informacji" z dnia 4.04.2018</a>

17. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-doreczenie.20180404.zlozone.pdf" target="_blank">Kopia pisma "Wniosek o prawidłowe doręczenie postanowienia o odmowie wszczęcia śledztwa z dnia 14 kwietnia 2014 roku" z dnia 4.04.2018</a>

18. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zazalenie-na-bezczynnosc.20180404.zlozone.pdf" target="_blank">Kopia pisma "Zażalenie na bezczynność Prokuratora" z dnia 4.04.2018</a>

19. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180404.zlozone.pdf" target="_blank">Kopia odpowiedzi na pismo z Prokuratury Rejonowej Kraków-Krowodrza z dnia 03.04.2018</a>

20. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180129.zlozone.pdf" target="_blank">Kopia odpowiedzi na pismo z Prokuratury Okręgowej w Krakowie z dnia 16.02.2018</a>

21. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_wniosek-o-wylaczenie-prokuratorow.20180109.zlozone.pdf" target="_blank">Kopia pisma "Wniosek o wyłączenie prokuratorów Prokuratury Rejonowej w Krakowie" z dnia 9.01.2018</a>

22. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_zawiadomienie_20180109.zlozone.pdf" target="_blank">Kopia pisma "Zawiadomienie o popełnieniu przestępstwa z art. 156 §1, art. 157 §1, art. 160 §1, art. 162 §1, art. 192 §1 oraz art. 193 Kodeksu karnego, równocześnie naruszenie art. 2 („prawo do życia”), art. 3 („zakaz nieludzkiego lub poniżającego traktowania”), art. 6 („prawo do rzetelnego procesu sądowego”) oraz art. 14 („zakaz dyskryminacji”) Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności" złożonego osobiście w Biurze Obsługi Interesantów Prokuratury Krajowej przy ul. Rakowieckiej 26/30 w Warszawie dnia 9.01.2018</a>


#### 2014

1. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zawiadomienie_20140228.zlozone.pdf" target="_blank">Kopia pisma "Zawiadomienie o popełnieniu przestępstwa z art. 197 §1 k.k." z dnia 28.02.2014</a>


### Otrzymane

#### 2018

 1. <a href="/pliki/Prawne/Odpowiedzi/PK-II_Ko2_128.2018_20180116.pdf" target="_blank">Kopia pisma z Prokuratury Krajowej z dnia 16.01.2018</a>

 2. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180129.pdf" target="_blank">Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 29.01.2018</a>

 3. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180215.pdf" target="_blank">Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 15.02.2018</a>

 4. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180219.pdf" target="_blank">Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 19.02.2018</a>

 5. <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180307.pdf" target="_blank">Kopia pisma z Prokuratury Rejonowej Kraków-Krowodrza z dnia 07.03.2018</a>

 6. <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180413.pdf" target="_blank">Kopia pisma z Prokuratury Rejonowej Kraków-Krowodrza w Krakowie z dnia 13.04.2018</a>

 7. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Dsn-47.2018.Kkr_20180425.pdf" target="_blank">Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 25.04.2018</a>

 8. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Dsn-47.2018.Kkr_20180427.pdf" target="_blank">Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 27.04.2018</a>

 9. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Dsn-47.2018.Kkr_20180523.pdf" target="_blank">Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 23.05.2018</a>


#### 2014

 1. <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">Kopia postanowienia o odmowie wszczęcia śledztwa na postawie art. 17. § 1 pkt 1 kpk, z uzasadnieniem że to kandydoza jest przyczyną zaobserwowanego trwałego uszkodzenia ciała z dnia 14.04.2014</a>

<hr class="thin-separator" />

## Organizacje pozarządowe (NGOs)

### Kontakt osobisty (11/2017)

* <a href="/pliki/NGOs/in-person/ngos_contact_in-person_krakow-london_20171106.pdf" target="_blank">Przelot Kraków - Londyn (2017/11/06)</a>

* <a href="/pliki/NGOs/in-person/ngos_contact_in-person_london-brussels_20171108.pdf" target="_blank">Przejazd Londyn - Bruksela (2017/11/08)</a>

* <a href="/pliki/NGOs/in-person/ngos_contact_in-person_brussels-krakow_20171110.pdf" target="_blank">Przelot Bruksela - Kraków (2017/11/10)</a>


### Kontakt e-mail

a. <a href="https://www.amnesty.org/" target="_blank">"Amnesty International"</a>

 * <a href="/pliki/NGOs/emails/amnesty_20171122.pdf" target="_blank">/pliki/NGOs/emails/amnesty_20171122.pdf</a>


b. <a href="http://www.apt.ch/" target="_blank">"Association for the prevention of torture"</a>

 * <a href="/pliki/NGOs/emails/apt_20171030.pdf" target="_blank">/pliki/NGOs/emails/apt_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/apt_20171215.pdf" target="_blank">/pliki/NGOs/emails/apt_20171215.pdf</a>
 * <a href="/pliki/NGOs/emails/apt_20190522.pdf" target="_blank">/pliki/NGOs/emails/apt_20190522.pdf</a>


c. <a href="https://www.couragefound.org/about-the-courage-foundation/" target="_blank">"Courage Foundation"</a>

 * <a href="/pliki/NGOs/emails/couragefound_20171030.pdf" target="_blank">/pliki/NGOs/emails/couragefound_20171030.pdf</a>


d. <a href="https://dignityinstitute.org/" target="_blank">"The Danish Institute Against Torture"</a>

 * <a href="/pliki/NGOs/emails/dignity_20171030.pdf" target="_blank">/pliki/NGOs/emails/dignity_20171030.pdf</a>


e. <a href="http://www.mct-onlus.it/en/" target="_blank">"Doctors Against Torture Humanitarian Organization"</a>

 * <a href="/pliki/NGOs/emails/mct-onlus.it_20171218.pdf" target="_blank">/pliki/NGOs/emails/mct-onlus.it_20171218.pdf</a>


f. <a href="https://eldh.eu/en/" target="_blank">"European Association of Lawyers for Democracy & World Human Rights"</a>

 * <a href="/pliki/NGOs/emails/eldh_20190522.pdf" target="_blank">/pliki/NGOs/emails/eldh_20190522.pdf</a>


g. <a href="https://www.ecchr.eu/en/international-crimes-accountability/" target="_blank">"European Center for Constitutional and Human Rights"</a>

 * <a href="/pliki/NGOs/emails/ecchr_20190522.pdf" target="_blank">/pliki/NGOs/emails/ecchr_20190522.pdf</a>


h. <a href="https://www.freedomfromtorture.org/" target="_blank">"Freedom from Torture"</a>

 * <a href="/pliki/NGOs/emails/freedomfromtorture_20171218.pdf" target="_blank">/pliki/NGOs/emails/freedomfromtorture_20171218.pdf</a>


i. <a href="http://www.hfhr.pl/en/" target="_blank">"Helsińska Fundacja Praw Człowieka"</a>

 * <a href="/pliki/NGOs/emails/hfhr_20180112.pdf" target="_blank">/pliki/NGOs/emails/hfhr_20180112.pdf</a>


j. <a href="https://ijrcenter.org/" target="_blank">"The International Justice Resource Center (IJRC)"</a>

 * <a href="/pliki/NGOs/emails/ijrcenter_20171030.pdf" target="_blank">/pliki/NGOs/emails/ijrcenter_20171030.pdf</a>


k. <a href="http://www.ptpa.org.pl/" target="_blank">"Polskie Towarzystwo Prawa Antydyskriminacyjnego"</a>

 * <a href="/pliki/NGOs/emails/ptpa_20180119.1.pdf" target="_blank">/pliki/NGOs/emails/ptpa_20180119.1.pdf</a>
 * <a href="/pliki/NGOs/emails/ptpa_20180119.2.pdf" target="_blank">/pliki/NGOs/emails/ptpa_20180119.2.pdf</a>


l. <a href="http://www.redress.org/" target="_blank">"Redress - Ending torture, seeking justice for survivors"</a>

 * <a href="/pliki/NGOs/emails/redress_20171030.pdf" target="_blank">/pliki/NGOs/emails/redress_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/redress_20190522.pdf" target="_blank">/pliki/NGOs/emails/redress_20190522.pdf</a>


m. <a href="https://trialinternational.org/" target="_blank">"TRIAL International"</a>

 * <a href="/pliki/NGOs/emails/trialinternational_20171030.pdf" target="_blank">/pliki/NGOs/emails/trialinternational_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/trialinternational_20190523.pdf" target="_blank">/pliki/NGOs/emails/trialinternational_20190523.pdf</a>


n. <a href="https://www.fidh.org/en/" target="_blank">"Worldwide movement for human rights"</a>

 * <a href="/pliki/NGOs/emails/fidh_20171030.pdf" target="_blank">/pliki/NGOs/emails/fidh_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/fidh_20171107.2.pdf" target="_blank">/pliki/NGOs/emails/fidh_20171107.2.pdf</a>
 * <a href="/pliki/NGOs/emails/fidh_20171107.pdf" target="_blank">/pliki/NGOs/emails/fidh_20171107.pdf</a>


<hr class="thin-separator" />

## Komunikacja

### Certyfikowana komunikacja SMS (12/2013 - 04/2014)

  * <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">Kopia protokołu zabezpieczenia komunikacji SMS przez specjalistów z dziedziny informatyki śledczej wraz z fakturą i protokołem odbioru</a>

  * <a href="/pliki/Komunikacja/GT-E1200_721030078_raport.pdf" target="_blank">Kopia uwierzytelniona wiadomości SMS przychodzących z / wychodzących do nr. +48721030078, z okresu od 04/03/2014 do 30/03/2014</a>

  * <a href="/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf" target="_blank">Kopia uwierzytelniona wiadomości SMS przychodzących z nr. +48512355495, z dnia 28.12.2013, na kilka dni przed opisywanymi zdarzeniami</a>

  * <a href="/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf" target="_blank">Kopia uwierzytelniona wiadomości SMS przychodzących z / wychodzących do nr. +48503990172, z okresu od 18/12/2013 do 14/01/2014</a>


### Komunikacja email

  * <a href="/pliki/Komunikacja/MHSiemaszko_Email_Compensation-for-failed-surgery.20141105.pdf" target="_blank">Kopia wiadomości email otrzymanej od p. Tomasza Gibasa 5 listopada 2014 r.</a>


### Komunikacja Facebook messenger

  * <a href="/pliki/Komunikacja/Screenshot_20181219-172035_Messenger.pdf" target="_blank">Kopia fragmentu komunikacji na Facebook Messenger z Moniką Hudyką, kiedy dnia 3 lutego 2014 r. zainicjowała kontakt ze mną</a>


<hr class="thin-separator" />

## Wynajem (10/2013 - 2/2014)

### Mieszkanie (10/2013 - 2/2014)

  * <a href="/pliki/Wynajem/Szymanowskiego_umowa_wypowiedzenie.pdf" target="_blank">Kopia wypowiedzenia umowy wynajmu lokalu mieszkalnego przy ul. Szymanowskiego 5/10 w Krakowie z dnia 1.03.2014</a>

  * <a href="/pliki/Wynajem/Szymanowskiego_umowa.pdf" target="_blank">Kopia umowy wynajmu lokalu mieszkalnego przy ul. Szymanowskiego 5/10 w Krakowie</a>


### Skrytka adresowa

  * <a href="/pliki/Wynajem/MHSiemaszko_Address-box_Rental-agreement.pdf" target="_blank">Kopia umowy udostępnienia skrytki adresowej pod adresem ul. Karmelicka 55 w Krakowie, zawarta z przedstawicielem firmy Polskie Centrum Usług Sp. z o.o. p. Izabela Adamska w dniu 25.02.2014</a>

  * <a href="/pliki/Wynajem/MHSiemaszko_Address-box_Terms-conditions.pdf" target="_blank">Kopia załącznika do umowy udostępnienia skrytki adresowej–Regulamin Świadczenia Usług</a>

  * <a href="/pliki/Wynajem/MHSiemaszko_Address-box_Authorization.pdf" target="_blank">Kopia załącznika do umowy udostępnienia skrytki adresowej–Pełnomocnictwo</a>


<hr class="thin-separator" />

## Biznes (10/2013 - 2/2014)

1. <a href="/pliki/Wspolpraca/Zerochaos_wypowiedzenie-umowy.pdf" target="_blank">Kopia wypowiedzenia umowy współpracy z firmą Zerochaos z dnia 11.02.2014</a>

2. Kopia dokumentacji dotyczącej delegacji do Zurychu, Szwajcarii:
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf</a>


3. <a href="/pliki/Wspolpraca/Zerochaos_umowa.pdf" target="_blank">Kopia umowy współpracy z firmą Zerochaos z dnia 19.09.2013</a>


<hr class="thin-separator" />

## Inne

### Zaświadczenia o niekaralności

#### 2014

 * <a href="/pliki/Inne/mhs_no-criminal-record_2014.pdf" target="_blank">Zaświadczenie o niekaralności z 2014 roku</a>


#### 2015

 * <a href="/pliki/Inne/mhs_no-criminal-record_2015.pdf" target="_blank">Zaświadczenie o niekaralności z 2015 roku</a>


#### 2017

 * <a href="/pliki/Inne/mhs_no-criminal-record_2017.pdf" target="_blank">Zaświadczenie o niekaralności z 2017 roku</a>


#### 2018

 * <a href="/pliki/Inne/mhs_no-criminal-record_2018.pdf" target="_blank">Zaświadczenie o niekaralności z 2018 roku</a>


### Studia

* <a href="/pliki/Inne/AkademiaEkonomiczna_zawiadomienie_20041011.pdf" target="_blank">Zaświadczenie potwierdzające, że studiowałem na Uniwersytecie Ekonomicznym w Krakowie, Kierunek stosunki międzynarodowe - program studiów nie został ukończony z powodu zobowiązań zawodowych</a>


### Stan cywilny

* <a href="/pliki/Inne/mhs_marital-status-divorced_20180518.pdf" target="_blank">Zaświadczenie o stanie cywilnym z 2018 roku</a>

### USA (1988 - 2003)

#### FOIA

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_response.2017.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_response.2017.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_cover-letter.2017.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_cover-letter.2017.pdf</a>

#### INS

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_G-325a.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_G-325a.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-131.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-131.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-485.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-485.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765_receipt-2002.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765_receipt-2002.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_documents-2002.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_documents-2002.pdf</a>

#### Social Security

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Social-Security_statement-2000.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Social-Security_statement-2000.pdf</a>

#### IRS

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_1999-tax-return_refund.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_1999-tax-return_refund.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_2000-tax-return_refund.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_2000-tax-return_refund.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_w2_1998-2002.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_w2_1998-2002.pdf</a>

#### Certyfikaty

* <a href="/pliki/Inne/USA_1988-2003/Certificate-of-birth_K-Siemaszko.pdf" target="_blank">/pliki/Inne/USA_1988-2003/Certificate-of-birth_K-Siemaszko.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/Certificate-of-death_Z-Siemaszko.pdf" target="_blank">/pliki/Inne/USA_1988-2003/Certificate-of-death_Z-Siemaszko.pdf</a>
