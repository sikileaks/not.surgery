---
title: Terroryzm państwowy w XXI wieku
layout: post
date: '2019-07-23 18:48:00'
categories: inne
ref: video-presentation-july-2019
lang: pl
image: /pliki/Prezentacja-201907/not-surgery_presentation.jpg
---

## Jak masowa inwigilacja, sprzedawana jako środek do walki z terroryzmem, jest wykorzystywana do terroryzowania ludności i wdrażania "niewidzialnego" totalitaryzmu.

Krótkie podsumowanie w formie prezentacji wideo.

<div class="videoWrapper">
  <iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/AmwlRVHCslm4/"></iframe>
</div>

<div class="videoExtras">
  <strong>Pobierz</strong>: <a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.mp4" | relative_path }}" target="_blank">wersja MP4</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.webm" | relative_path }}" target="_blank">wersja WEBM</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-slides.pdf" | relative_path }}" target="_blank">slajdy (PDF)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-narration.pdf" | relative_path }}" target="_blank">narracja (PDF)</a>
</div>
