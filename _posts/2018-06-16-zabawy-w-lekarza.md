---
title: Krakowskich organów ścigania zabawy w lekarza
layout: post
date: '2018-06-16 22:15:00'
categories: medyczne
ref: lets-pretend
lang: pl
---

Organy ścigania nie maja uprawnień do wydawania opinii medycznych bez powołania biegłego i przeprowadzenia obdukcji.

Prokurator Bartłomiej Legutko ani st. post. Daria Curzydło, prowadzący postępowanie sprawdzające z zawiadomienia złożonego 28.02.2014 r. nie posiadali żadnej wiedzy specjalnej aby w odmowie wszczęcia śledztwa z 14.04.2014 r. (<a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a>) uzasadnić, iż kandydoza jest przyczyną trwałego uszkodzenia ciała.

Komplet dokumentacji medycznej zebranej od czasu wydarzeń w styczniu 2014 r. bezsprzecznie potwierdza działanie osób 3cich, wynikiem którego jest trwałe uszkodzenie ciała, w tym ukł. moczowo-płciowego i nerwowego, oraz blizny pozostałe po ranach kłutych (<a href="/medyczne/2018/06/16/rzeczywista-diagnoza.html" target="_blank">/medyczne/2018/06/16/rzeczywista-diagnoza.html</a>).

Przed wydarzeniami stycznia 2014 r. nie chorowałem na żadne ciężkie schorzenie i żaden z opisywanych objawów nie występował wcześniej – leczyłem się jedynie na kandydozę ukł. pokarmowego i przyjmowałem leki przepisane mi wtedy na to schorzenie przez lekarza z którym się konsultowałem (<a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.pdf</a>).

Uzasadnienie zawarte w odmowie wszczęcia śledztwa jest absurdem – kandydoza nie może być i nie jest przyczyna zaobserwowanych obrażeń ciała i uszkodzenia ukł. moczowo-płciowego i nerwowego oraz blizn na ciele (<a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a>). 

Co najważniejsze, biorąc pod uwagę iż okaleczenie dotyczy najbardziej intymnych części ciała, a groźby karalne wystosowane do mnie na kilka dni przed wydarzeniami stycznia 2014 r. i spełnione w wyniku wydarzeń zostały przynajmniej dwukrotnie zbagatelizowane ("*nie stanowią czynu zabronionego*"), włącznie z innymi rażącymi uchybieniami w działaniu krakowskich organów ścigania, które miały miejsce w każdej sprawie z mojego zawiadomienia w ostatnich 4 latach, świadczy nie tylko o tym iż było to działanie z premedytacją, ale jest to ewidentne kryminalne zachowanie mające na celu ukrycie przestępstwa.
