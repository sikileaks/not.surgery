---
title: Second medical negligence lawsuit was filed
layout: post
date: '2018-12-03 10:25:00'
categories: medical
ref: second-medical-negligence-lawsuit
lang: en
---

Second medical negligence lawsuit was filed. 

 * English language version: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf</a>
 * Czech translation: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf</a>


## Annexures

 1. Report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018 (Zpráva z vyšetření a konzultace s lékařem specializujícím se na neurologii a plastickou chirurgii, provedených dne 6. a 20. dubna 2018)
   * Original: <a href="/pliki/Prawne/EPSUK/Neurologist-PlasticSurgeon_Millesi-Center-Vienna_20180420.report.original.pdf" target="_blank">/pliki/Prawne/EPSUK/Neurologist-PlasticSurgeon_Millesi-Center-Vienna_20180420.report.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/Neurologist-PlasticSurgeon_Millesi-Center-Vienna_20180420.report.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/Neurologist-PlasticSurgeon_Millesi-Center-Vienna_20180420.report.CZ-translation.pdf</a>

 2. Clinical images from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018 (Klinické zobrazení dat z vyšetření a konzultace s lékařem specializujícím se na neurologii a plastickou chirurgii, provedených dne 6. a 20. dubna 2018)
   * <a href="/pliki/Prawne/EPSUK/MHSiemaszko_Neurologist-PlasticSurgeon_Clinical-Images.pdf" target="_blank">/pliki/Prawne/EPSUK/MHSiemaszko_Neurologist-PlasticSurgeon_Clinical-Images.pdf</a>

 3. Report from Ultrasound examination of nervous system, conducted on April 6, 2018 (Zpráva z ultrazvukového vyšetření nervového systému provedeného dne 6. dubna 2018)
   * Original: <a href="/pliki/Prawne/EPSUK/USG_PUC-Vienna_20180413.report.original.pdf" target="_blank">/pliki/Prawne/EPSUK/USG_PUC-Vienna_20180413.report.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/USG_PUC-Vienna_20180413.report.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/USG_PUC-Vienna_20180413.report.CZ-translation.pdf</a>

 4. Selected frames from Ultrasound examination of nervous system, conducted on April 6, 2018 (Vybrané snímky dat z ultrazvukového vyšetření nervového vyšetření provedeného dne 6. dubna 2018)
   * <a href="/pliki/Prawne/EPSUK/MHSiemaszko_USG-of-nerves.pdf" target="_blank">/pliki/Prawne/EPSUK/MHSiemaszko_USG-of-nerves.pdf</a>

 5. Report from Magnetic Resonance examination of nervous system, conducted on March 12, 2018 (Zpráva z vyšetření nervového systému magnetickou rezonancí provedeného dne 12. března 2018)
   * Original: <a href="/pliki/Prawne/EPSUK/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.report.original.pdf" target="_blank">/pliki/Prawne/EPSUK/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.report.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.report.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.report.CZ-translation.pdf</a>

 6. Selected frames from DICOM data from Magnetic Resonance examination of nervous system, conducted on March 12, 2018 (Vybrané snímky z DICOM dat z vyšetření nervového systému magnetickou rezonancí provedeného dne 12. března 2018)
   * <a href="/pliki/Prawne/EPSUK/MHSiemaszko_MR-Neurography-of-Pelvis.pdf" target="_blank">/pliki/Prawne/EPSUK/MHSiemaszko_MR-Neurography-of-Pelvis.pdf</a>

 7. Report from Computed Tomography examination of pelvis, conducted on February 13, 2018 (Zpráva z výpočetní tomografie oblasti pánve provedené dne 13. února 2018)
   * Original: <a href="/pliki/Prawne/EPSUK/CT_Radiology-Center-Vienna_20180213.report.original.pdf" target="_blank">/pliki/Prawne/EPSUK/CT_Radiology-Center-Vienna_20180213.report.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/CT_Radiology-Center-Vienna_20180213.report.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/CT_Radiology-Center-Vienna_20180213.report.CZ-translation.pdf</a>

 8. Selected images from Optical Coherence Tomography (OCT) examination of area where scars from stab/puncture wounds are visible, conducted on April 6, 2018 (Vybrané snímky z vyšetření optickou koherentní tomografií (OCT) oblasti, kde jsou patrné jizvy z bodnutí / vpichu, provedeného dne 6. dubna 2018)
   * <a href="/pliki/Prawne/EPSUK/MHSiemaszko_Optical-Coherence-Tomography.pdf" target="_blank">/pliki/Prawne/EPSUK/MHSiemaszko_Optical-Coherence-Tomography.pdf</a>

 9. Report from Ultrasound examination of urinary tract (images, description), conducted on January 11, 2016 (Zpráva z ultrazvukového vyšetření močové trubice (zobrazení, popis) provedeného dne 11.1.2016)
   * Original: <a href="/pliki/Prawne/EPSUK/USG_St-Medica_20160111.report.original.pdf" target="_blank">/pliki/Prawne/EPSUK/USG_St-Medica_20160111.report.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/USG_St-Medica_20160111.report.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/USG_St-Medica_20160111.report.CZ-translation.pdf</a>

10. Report from Ultrasound examination of urinary tract (images, description), conducted on January 29, 2014 (Zpráva z ultrazvukového vyšetření močové trubice (zobrazení, popis) provedeného dne 29.1.2016)
   * Original: <a href="/pliki/Prawne/EPSUK/USG_Ultramedica_20140129.report.original.pdf" target="_blank">/pliki/Prawne/EPSUK/USG_Ultramedica_20140129.report.original.pdf</a>
   * English translation: <a href="/pliki/Prawne/EPSUK/USG_Ultramedica_20140129.report.EN-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/USG_Ultramedica_20140129.report.EN-translation.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/USG_Ultramedica_20140129.report.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/USG_Ultramedica_20140129.report.CZ-translation.pdf</a>

11. Email from the Complainant, dated 2015/10/24 (Email žalobce ze dne 24.10.2015) 
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.CZ-translation.pdf</a>

12. Email from the Defendant No. 1, dated 2015/10/25 (Email obžalovaného č. 1 ze dne 25.10.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.CZ-translation.pdf</a>

13. Email from the Complainant, dated 2015/10/29 (Email žalobce ze dne 29.10.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Initial-Enquiry.24-25-29.10.2015.CZ-translation.pdf</a>

14. Email from the Defendant No. 1, dated 2015/11/05 (Email obžalovaného č. 1 ze dne 5.11.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_FirstOffer_05112015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_FirstOffer_05112015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_FirstOffer_05112015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_FirstOffer_05112015.CZ-translation.pdf</a>

15. Email from the Complainant, dated 2015/11/08 (Email žalobce ze dne 8.11.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.CZ-translation.pdf</a>

16. Email from the Defendant No. 1, dated 2015/11/12 (Email obžalovaného č. 1 ze dne 12.11.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_12112015.CZ-translation.pdf</a>

17. Email from the Complainant, dated 2015/11/12 (Email žalobce ze dne 12.11.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Context_12112015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Context_12112015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_Context_12112015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_Context_12112015.CZ-translation.pdf</a>

18. Email from the Complainant, dated 2015/11/29 (Email žalobce ze dne 29.11.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecision_29112015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecision_29112015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecision_29112015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecision_29112015.CZ-translation.pdf</a>

19. Email from the Defendant No. 1, dated 2015/11/30 (Email obžalovaného č. 1 ze dne 30.11.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecisionReply_30112015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecisionReply_30112015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecisionReply_30112015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOfferDecisionReply_30112015.CZ-translation.pdf</a>

20. Email from the Complainant, dated 2015/12/01 (Email žalobce ze dne 1.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.CZ-translation.pdf</a>

21. Email from the Defendant No. 1, dated 2015/12/03 (Email obžalovaného č. 1 ze dne 3.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.CZ-translation.pdf</a>

22. Email from the Defendant No. 1, dated 2015/12/07 (Email obžalovaného č. 1 ze dne 7.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_Dates_07122015.CZ-translation.pdf</a>

23. Email from the Complainant, dated 2015/12/08 (Email žalobce ze dne 8.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesReply_08122015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesReply_08122015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesReply_08122015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesReply_08122015.CZ-translation.pdf</a>

23. Email from the Defendant No. 1, dated 2015/12/08 (Email obžalovaného č. 1 ze dne 8.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesEPSReply_08122015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesEPSReply_08122015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesEPSReply_08122015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesEPSReply_08122015.CZ-translation.pdf</a>

24. Email from the Complainant, dated 2015/12/09 (Email žalobce ze dne 9.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesConfirmation_09122015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesConfirmation_09122015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesConfirmation_09122015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_SecondOffer_DatesConfirmation_09122015.CZ-translation.pdf</a>

25. Booking.com confirmation of 13-16 December, 2015, stay in Prague (Potvrzení rezervace ubytování v Praze prostřednictvím booking.com na dobu 13. – 16. prosince 2015)
   * <a href="/pliki/Prawne/EPSUK/EPS_Prague_accommodation_20151213-16.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Prague_accommodation_20151213-16.original.pdf</a>

26. Email from the Defendant No. 1, dated 2015/12/10 (Email obžalovaného č. 1 ze dne 10.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_MeetingDetails_10.12.2015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_MeetingDetails_10.12.2015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_MeetingDetails_10.12.2015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_MeetingDetails_10.12.2015.CZ-translation.pdf</a>

27. "Price List of Facultative Other Services", received in email from the Defendant No. 1, dated 2015/12/10 („Ceník ostatních fakultativních služeb“ Email obžalovaného č. 1 ze dne 2015/12/10)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Facultative-services-price-list.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Facultative-services-price-list.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Facultative-services-price-list.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Facultative-services-price-list.CZ-translation.pdf</a>

28. Email from the Complainant, dated 2015/12/14 (Email žalobce ze dne 14.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_RefundRequest_14-15.12.2015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_RefundRequest_14-15.12.2015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_RefundRequest_14-15.12.2015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_RefundRequest_14-15.12.2015.CZ-translation.pdf</a>

29. Email from the Defendant No. 1, dated 2015/12/30 (Email obžalovaného č. 1 ze dne 30.12.2015)
   * Original: <a href="/pliki/Prawne/EPSUK/EPS_Communication_FinalResults_30122015.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_FinalResults_30122015.original.pdf</a>
   * Czech translation: <a href="/pliki/Prawne/EPSUK/EPS_Communication_FinalResults_30122015.CZ-translation.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Communication_FinalResults_30122015.CZ-translation.pdf</a>

30. Invoice from the Defendant No. 1, dated 2015/12/30 (Faktura obžalovaného č. 1 ze dne 30.12.2015)
   * <a href="/pliki/Prawne/EPSUK/EPS_Diagnostic_care_statement.20151230.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Diagnostic_care_statement.20151230.original.pdf</a>

31. Report from USG examination and consultation conducted with the Defendant No. 3 on December 14, 2015, first received by the Complainant in email message dated 2015/12/30 (Zpráva z vyšetření USG a konzultace s obžalovaným č. 3 ze dne 14.12.2015, kterou žalobce poprvé obdržel emailem dne 30.12.2015)
   * <a href="/pliki/Prawne/EPSUK/EPS_Doctors_report.20151230.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Doctors_report.20151230.original.pdf</a>

32. Results from blood and urine examinations conducted at facility operated by the Defendant No. 2, first received by the Complainant in email message dated 2015/12/30 (Výsledky vyšetření krve a moči provedených v zařízení vedeném obžalovaným č.2, kterou žalobce obdržel v emailové zprávě dne 30.12.2015)
   * <a href="/pliki/Prawne/EPSUK/EPS_Final_results.20151230.original.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Final_results.20151230.original.pdf</a>

33. Demand for payment letter, dated 2016/01/28 (Upomínka ze dne 28.1.2016)
   * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160128.oryginal.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160128.oryginal.pdf</a>

34. Response to demand for payment letter, dated 2016/02/12 (Odpověď na upomínku ze dne 12.2.2016)
   * <a href="/pliki/Prawne/EPSUK/EPS_Korbel_Demand-for-payment_20160212.oryginal.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Korbel_Demand-for-payment_20160212.oryginal.pdf</a>

35. Second demand for payment letter, dated 2016/02/29 (Druhá upomínka ze dne 29.2.2016)
   * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160229.oryginal.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160229.oryginal.pdf</a>

36. Response to second demand for payment letter, dated 2016/03/10 (Odpověď na druhou upomínku ze dne 10.3.2016)
   * <a href="/pliki/Prawne/EPSUK/EPS_Korbel_Demand-for-payment_20160310.oryginal.pdf" target="_blank">/pliki/Prawne/EPSUK/EPS_Korbel_Demand-for-payment_20160310.oryginal.pdf</a>
