---
title: Video interview from August 2013
layout: post
date: '2019-07-23 18:27:00'
categories: other
ref: video-interview-from-august-2013
lang: en
---

Video interview recorded in August 2013, about **5 months before** the events of January 2014.

This was at a time when for almost 3 years already I was systematically suppressed, to the point of not being able to earn my living. I suffered severe starvation and homelessness, to name a few of the consequences of what was already going on at that time.

Please note that the title "injection with diabetes" has nothing to do with reality. This title was assigned, for reasons unknown to me, by the interviewer, Mr. M. Podlecki, with whom I have not had contact since then.

<div class="videoWrapper">
  <iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/hgcjM0WPbTWR/"></iframe>
</div>

<div class="videoExtras">
  <strong>Download</strong>: <a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.mp4" | relative_path }}" target="_blank">MP4 version</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.EN.srt" | relative_path }}" target="_blank">subtitles (SRT)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.EN.vtt" | relative_path }}" target="_blank">subtitles (VTT)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.transcript.EN.pdf" | relative_path }}" target="_blank">transcript (PDF)</a>
</div>
