---
title: Notification of a crime committed by M. Hudyka was filed
layout: post
date: '2019-02-06 14:00:00'
categories: legal
ref: hudyka-notification-of-crime
lang: en
---

On February 6th 2019 I filed a notification of a crime under art. 156 §1, art. 158 §, art. 159, art. 193, art. 233 § 1, art. 234, art. 236 § 1, art. 238, art. 239 § 1, art. 258 § 1 of the Penal Code committed by M. Hudyka: <a href="/pliki/Prawne//Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf" target="_blank">/pliki/Prawne//Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf</a>

screencasts of facebook messenger "invitation" received from hudyka on february 3 2014...:
 * <a href="/pliki/Komunikacja/20200416-234727.EZ-VideoRecorder.1.mp4" target="_blank">screencast #1</a>
 * <a href="/pliki/Komunikacja/20200416-235002.EZ-VideoRecorder.2.mp4" target="_blank">screencast #2</a>
 * <a href="/pliki/Komunikacja/2020-04-16-235248.mp4" target="_blank">screencast #3</a>

![M. Hudyka](/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Screenshot_20181219-172035_Messenger.jpg)
