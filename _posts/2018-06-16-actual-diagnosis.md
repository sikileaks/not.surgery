---
title: 'The real diagnosis: punctured groin and paralysis of the genitourinary and nervous system functions'
layout: post
date: '2018-06-16 18:30:00'
categories: medical
ref: actual-diagnosis
lang: en
---

Medical documentation collected since the events of January 2014 unquestionably confirms the action of 3rd parties, which resulted in permanent bodily injury, including genitourinary and nervous system damage, as well as scars left from puncture wounds in groin area.

1\. <a href=" /pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">Medical report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018</a>
>
> (…) We studied the provided recently performed Neuro MRI of the patient pelvis with our radiologist (…) The MRI showed a thickening of the left lateral
cutaneous femoral nerve at the area of the positive Tinel sign and a thickening of the left genitofemoral nerve at the anterior-medial aspect of the psoas muscle, a region just before the genital branch of the genitofemoral nerve enters the spermatic cord. In addition, docent (…) performed a high-resolution ultrasound study of the inguinal area on the left side which showed the same thickening of the nerve and scar formation. The distance from the hyperpigmented spots in the inguinal area on the left side to the point where the left lateral cutaneous femoral nerve shows a significant scar and an hour-glas deformation was measured with about 5cm.
>
> If we follow these findings and the assumption of a penetrating needle lesion at the left inguinal region including injection of an unknown substance, it is possible that a 5cm long needle is able to reach the left lateral cutaneous femoral nerve, the genitofemoral nerve, and as well the autonomeous nerval plexus which innervates the bladder of the patient. (…)
>
> (…) In summary, we found a clear pathology of the lateral cutaneous femoral nerve and the to a lower extent of the genitofemoral nerve on the left inguinal area. There are several punctiform skin hyperpigmentation in the inguinal area and the distance to the lesion of the most superficial nerve is about 5cm. The symptoms the patient suffered could be explained by the assumption of a needle attack including the injection of an unknown substance. (…)

 English language version, original: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>

 Clinical images from the visit:

![Clinical images from the Millesi Center visit](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_1.small.JPG "Clinical images from the Millesi Center visit")

![Clinical images from the Millesi Center visit](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_2.small.JPG "Clinical images from the Millesi Center visit")

![Clinical images from the Millesi Center visit](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_3.small.JPG "Clinical images from the Millesi Center visit")

![Clinical images from the Millesi Center visit](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_4.small.JPG "Clinical images from the Millesi Center visit")


 OCT (Optical Coherence Tomography) images:

![OCT images](/pliki/Medyczne/Zdjecia/OpticalCoherenceTomography/OCT_clinical-image.png "OCT images")

![OCT images](/pliki/Medyczne/Zdjecia/OpticalCoherenceTomography/OCT_hole-area_1.png "OCT images")

![OCT images](/pliki/Medyczne/Zdjecia/OpticalCoherenceTomography/OCT_hole-area_cross-section.png "OCT images")


2\. <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">Results from ultrasound examination of nervous system, conducted on April 6, 2018</a>
>
> (…) Ultrasound reveals a marked swelling of the femoral cutaneus lateral nerve at the level of the anterior superior iliac spine. The nerve swelling extends approximately for 15 mm. (…) The cross sectional diameter of the thickened nerve is 3 times above the normal value. The findings correspond to the previous MRI. (…)
>

 English language version, original: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>

 Selected images from USG of nervous system:

![Images from USG of nervous system](/pliki/Medyczne/Zdjecia/USGofNerves/USG-of-nerves_1.smaller.jpg "Images from USG of nervous system")

![Images from USG of nervous system](/pliki/Medyczne/Zdjecia/USGofNerves/USG-of-nerves_2.smaller.jpg "Images from USG of nervous system")


3\. <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">Results from magnetic resonance examination of nervous system, conducted on March 12, 2018</a>
>
> (…) Functional nerve imaging reveals hourglass-shaped restricted diffusion in the lateral cutaneous nerve of the thigh across the inguinal ligament. (…)
>
> (…) MR scan findings are suggestive of entrapment of the lateral cutaneous nerve of the left thigh due to scarring in the left tensor fascia lata with altered signal and restricted difusion (…) There is also thickening and altered signal in the left genitofemoral nerve. (…)
>

 English language version, original: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>

 Selected frames from DICOM data:

![MRI of nervous system - selected frames from DICOM data](/pliki/Medyczne/Zdjecia/MagneticResonanceNeurography/MR-Neurography_001.smaller.png "MRI of nervous system - selected frames from DICOM data")

![MRI of nervous system - selected frames from DICOM data](/pliki/Medyczne/Zdjecia/MagneticResonanceNeurography/MR-Neurography_002.smaller.png "MRI of nervous system - selected frames from DICOM data")

![MRI of nervous system - selected frames from DICOM data](/pliki/Medyczne/Zdjecia/MagneticResonanceNeurography/MR-Neurography_003.smaller.png "MRI of nervous system - selected frames from DICOM data")


4\. <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">Results from computed tomography examination of pelvis, conducted on February 13, 2018</a>
>
> (…) Condition after penetrating trauma left pelvic, dysaesthesia, neurological impairment. (…) The skin scar is also detectable by computer tomographicaily as low subcutaneous compression zone in the course via the proximal and anterior portion of the tensor fascia latae muscle.
>
> (…) Severe cutaneous scarring (…) around the left superior anterior iliac spur and neighboring parts of the tensor fascia lata muscle. We recommend a consultation of a specialized centre for reconstructive peripheral nerve surgery (e.g. Millesi Center) after neurological testing. (…)
>

 German language version, original: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>


5\. <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">Results from ultrasound examination of urinary tract (images, description), conducted on January 11, 2016</a>
>
> (…) Huge residual urine: 260ml (…) Diagnosis: Erectile disfunction, Neurogenic bladder disorder (…)
>

 English language version, original: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>


6\. <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">Results from ultrasound examination of urinary tract (images, description), conducted on January 29, 2014, immediately after the events</a>
>
> (…) Urinary bladder (…) Post-void retention ca. 390 ml!!! (…)
>

English language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>

Prior to the events of January 2014, I did not suffer from any serious illness, I only treated myself for candidiasis of the digestive system and was taking medications prescribed to me by the physician I consulted. (<a href="/pliki/Medyczne/Kandydoza_Intermed_2013.EN.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.EN.pdf</a>).

The absurd justification for this permanent bodily injury – including genitourinary and nervous system damage, as well as scars left from puncture wounds in groin area – provided in the decision refusing to initiate an investigation dated April 14 2014, (<a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_target">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a>, Polish language version, original), signed by prosecutor Bartlomiej Legutko and senior constable Daria Curzydło, claiming that candidiasis of the digestive system is the cause of these extensive injuries, not only demostrates the gross violations of procedural obligations by law enforcement authorities, who have no right to issue medico-legal opinions without appointing a medico-legal expert and performing a forensic examination, but criminal behavior aimed at concealing the crime.

In addition to the above-mentioned medical documentation, the physician who lead candidiasis treatment in year 2013 clearly stated – as a doctor having qualifications to issue such opinions – that candidiasis cannot and is not the cause of the observed bodily injuries and damage to genitourinary and nervous system as well as scars left from puncture wounds in groin area (<a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a>, Polish language version, original).
