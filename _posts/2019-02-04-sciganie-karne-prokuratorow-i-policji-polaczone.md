---
title: Zawiadomienia z art. 231 §1 k.k. przeciwko prokuratorom i policji w sprawie połączonej
layout: post
date: '2019-02-04 14:00:00'
categories: prawne
ref: criminally-prosecuting-law-enforcement-related
lang: pl
---

W dniu 4 lutego 2019 r. złożyłem kolejne 4 zawiadomienia o popełnieniu przestępstwa z art. 231 §1 k.k. prokuratorom i policji w sprawie połączonej: 

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf"  target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf</a>

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf</a>

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf</a>

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf</a>

W sumie to już 9 zawiadomień z art. 231 §1 k.k. przeciwko krakowskim organom procesowym (zob. <a href="/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html" target="_blank">/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html</a>
