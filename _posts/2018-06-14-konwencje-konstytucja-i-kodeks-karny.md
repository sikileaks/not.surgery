---
title: Co na to Deklaracja Praw Człowieka ONZ, Europejska Konwencja Praw Człowieka,
  Konstytucja RP i Kodeks Karny
layout: post
date: '2018-06-14 17:55:00'
categories: prawne
ref: conventions-constitution-and-penal-code
lang: pl
---

Wybrane artykuły Deklaracji Praw Człowieka ONZ, Europejskiej Konwencji Praw Człowieka, Konstytucji RP oraz Kodeksu Karnego, dotyczących procedur medycznych/zabiegów bez zgody, uszkodzenia układu płciowego oraz odmawiania pomocy medycznej.

<hr class="thin-separator" />

# Kodeks Karny
* Art. 156 §1. kk. 
> Kto powoduje ciężki uszczerbek na zdrowiu w postaci: <br/>
> 1) pozbawienia człowieka wzroku, słuchu, mowy, zdolności płodzenia, <br/>
> 2) innego ciężkiego kalectwa, ciężkiej choroby nieuleczalnej lub długotrwałej, choroby realnie zagrażającej życiu, trwałej choroby psychicznej, <br/> całkowitej albo znacznej trwałej niezdolności do pracy w zawodzie lub trwałego, istotnego zeszpecenia lub zniekształcenia ciała, podlega karze pozbawienia wolności od roku do lat 10.
> 

* Art. 162. §1. 
> Kto człowiekowi znajdującemu się w położeniu grożącym bezpośrednim niebezpieczeństwem utraty życia albo ciężkiego uszczerbku na zdrowiu nie udziela pomocy, mogąc jej udzielić bez narażenia siebie lub innej osoby na niebezpieczeństwo utraty życia albo ciężkiego uszczerbku na zdrowiu, podlega karze pozbawienia wolności do lat 3.
> 

* Art. 192. §1. 
> Kto wykonuje zabieg leczniczy bez zgody pacjenta, podlega grzywnie, karze ograniczenia wolności albo pozbawienia wolności do lat 2.
> 

<hr class="thin-separator" />

# Konstytucja Rzeczypospolitej Polskiej
* Art. 9. 
> Rzeczpospolita Polska przestrzega wiążącego ją prawa międzynarodowego.
> 

* Art. 30. 
> Przyrodzona i niezbywalna godność człowieka stanowi źródło wolności i praw człowieka i obywatela. Jest ona nienaruszalna, a jej poszanowanie i ochrona jest obowiązkiem władz publicznych.
> 

* Art. 39. 
> **Nikt nie może być poddany eksperymentom naukowym, w tym medycznym, bez dobrowolnie wyrażonej zgody.**
> 

* Art. 40. 
> Nikt nie może być poddany torturom ani okrutnemu, nieludzkiemu lub poniżającemu traktowaniu i karaniu. Zakazuje się stosowania kar cielesnych.
> 

* Art. 47. 
> Każdy ma prawo do ochrony prawnej życia prywatnego, rodzinnego, czci i dobrego imienia oraz do decydowania o swoim życiu osobistym.
> 

* Art. 77. 
> 1\. Każdy ma prawo do wynagrodzenia szkody, jaka została mu wyrządzona przez niezgodne z prawem działanie organu władzy publicznej. <br/>
> 2\. Ustawa nie może nikomu zamykać drogi sądowej dochodzenia naruszonych wolności lub praw.
> 

<hr class="thin-separator" />

# Europejska Konwencja Praw Człowieka
"Konwencja o ochronie praw człowieka i podstawowych wolności" 
<a href="http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU19930610284/T/D19930284L.pdf" target="_blank">http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU19930610284/T/D19930284L.pdf</a>
	
* Artykuł 2 – Prawo do życia 
> 1. Prawo każdego człowieka do życia jest chronione przez ustawę. (…)
> 

* Artykuł 3 – Zakaz tortur
> Nikt nie może być poddany torturom ani nieludzkiemu lub poniżającemu traktowaniu albo karaniu.
> 

<hr class="thin-separator" />

# Deklaracja Praw Człowieka ONZ
"Międzynarodowy Pakt Praw Obywatelskich i Politycznych z 19 grudnia 1966 r.” 
<a href="http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU19770380167/T/D19770167L.pdf" target="_blank">http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU19770380167/T/D19770167L.pdf</a>
	
* Artykuł 2. ust. 3 
> Każde z Państw Stron niniejszego Paktu zobowiązuje się  
> a) zapewnić każdej osobie, której prawa lub wolności uznane w niniejszym Pakcie zostały naruszone, skuteczny środek ochrony prawnej, nawet gdy naruszenie to zostało dokonane przez osoby działające w charakterze urzędowym 
> 
	 
* Artykuł 7
> Nikt nie będzie poddawany torturom lub okrutnemu, nieludzkiemu albo poniżającemu traktowaniu lub karaniu. **W szczególności nikt nie będzie poddawany, bez swej zgody swobodnie wyrażonej, doświadczeniom lekarskim lub naukowym** 
> 
	
* Artykuł 17. ust. 1 
> Nikt nie może być narażony na samowolna lub bezprawna ingerencje w jego życie prywatne, rodzinne, dom czy korespondencje ani tez na bezprawne zamachy na jego cześć i dobre imię.
> 


