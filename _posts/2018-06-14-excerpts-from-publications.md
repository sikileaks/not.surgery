---
title: Commentary on the provisions contained in the Geneva Convention, the UN Declaration of Human Rights, the European Convention on Human Rights and the Penal Code on the basis of selected excerpts from judgments 
layout: post
date: '2018-06-14 18:02:00'
categories: legal
ref: excerpts
lang: en
---

Commentary on the provisions contained in the Geneva Convention, the UN Declaration of Human Rights, the European Convention on Human Rights and the Penal Code on the basis of selected excerpts from judgments.

<hr class="thin-separator" />

"**Guide to Jurisprudence on Torture and Ill-treatment**"
  <a href="http://www.apt.ch/content/files_res/Article3_en.pdf" target="_blank">http://www.apt.ch/content/files_res/Article3_en.pdf</a>
	
> (...) Violations Due to a Lack of an Effective Investigation (...) in order to provide a plausible explanation of how the injuries were caused, the State must conduct an effective investigation into allegations of ill-treatment.

> The finding of a violation due to the lack of an effective investigation would appear to have arisen in order to address difficulties encountered by the requirement that allegations of ill-treatment must be supported by appropriate evidence. 
> In The Greek Case and Ireland v UK (discussed above), the Court and Commission held that the standard of proof for violations of Article 3 was proof “beyond reasonable doubt” that the ill-treatment had occurred.
> However, the imposition of this standard of proof fails to take into account the difficulty for victims in obtaining supporting evidence, because, for example, of the denial of access to medical treatment or legal counsel, or a lack of an effective complaints procedure.
> In Ireland v UK the Court appeared to have tried to address the dichotomy encountered between requiring proof beyond reasonable doubt and the difficulty in obtaining evidence from the alleged violator, i.e. the State authortities or its agents, that the ill-treatment had occurred. In this instance the Court held that, whilst the burden of proof was “beyond reasonable doubt”, it agreed with the Commission’s earlier decision that, to assess the evidence, proof may follow from“ the coexistent of sufficiently strong, clear and concordant inferences or of similar unrebutted presumptions of fact. In this context, the conduct of the Parties when evidence is being obtained has to be taken into account” (...)
>
> (...) it is evident that the Court is increasingly mindful of the difficulties facing victims in obtaining supporting evidence of ill-treatment. Consequently, it has imposed an obligation upon State authorities to carry out an effective investigation into allegations of ill-treatment. Without such a duty to investigate, perpetrators of ill-treatment would be free to act with apparent impunity. (...)
> 
> (...) The Court noted that an investigation should “be capable of leading to the identification and punishment of those responsible ”Without such a duty to investigate, the Court noted that “the general legal prohibition of torture and inhuman and degrading treatment and punishment, despite its fundamental importance, would be ineffective in practice and it would be possible in some cases for agents of the State to abuse the rights of those within their control with virtual impunity”. (...)
> 

<hr class="thin-separator" />

"**Defining threshold between torture and inhuman or degrading treatment**" 
  <a href="http://lr.bsulawss.org/en/wp-content/uploads/2015/06/bsu-lr-vol1-sadiqova.pdf" target="_blank">http://lr.bsulawss.org/en/wp-content/uploads/2015/06/bsu-lr-vol1-sadiqova.pdf</a>
	
> (...) Article 7 of International Covenant on Civil and Political Rights dealt with this issue in  a more comprehensive way compared to UDHR: 
> “No one shall be subjected to torture or to cruel, inhuman or degrading treatment or  punishment. **In particular, no one shall be subjected without  his free consent to medical or scientific experimentation**”. (...)
> (...) The different purposes that an act of ill-treatment must fulfill to be considered as  torture or cruel, inhuman and degrading treatment are the following:
> (...) 4. For intimidation and coercion; or 5. for discrimination (...)
> 
> (...) the decisive criteria for distinguishing torture from cruel, inhuman and degrading  treatment may best be understood to be the purpose of the conduct and the powerlessness of the victim, rather than the intensity of the pain or suffering inflicted. (...)
> 
> (...) The European Court of Human Rights (“Court”) considers that, in order to fall under the provision of Article 3 of the European Convention of Human Rights (“ECHR”), an act of ill-treatment, whether it is torture, inhuman or degrading treatment or punishment, must attain a minimum level of severity. The assessment of this threshold of severity is made in regard of the  specific circumstances of the case and the Court considers the following:
>	- duration of treatment; 
>	- physical effects of treatment;
>	- mental effects of treatment; and 
>	- sex, age and state of health of the victim. (...)
>	
> (...) Article 3 of ECHR prohibits three different forms of ill-treatment: torture, cruel and inhuman treatment, and aims to preserve personal dignity. As we see, these notions are not identical. In certain respects their legal consequences vary, especially when such acts enter  the area of international criminal law, such as the exercise of universal jurisdiction (“jus cogens”  norms). (...)
> 
> (...) **State involvement is one of the main keys in order to talk about the existence of the  torture** (...)
> 
> (...) Although the lines between torture and cruel, inhuman or degrading treatment may sometimes be unclear, the distinction between them is also crucial, because whilst cruel, inhuman and degrading treatment is prohibited officially, in most cases a State does not have the same extent of obligations to criminalize, investigate and prosecute acts of cruel, inhuman or degrading treatment that it has regarding torture. (...)
> 
> (...) As mentioned above ill-treatment must attain a minimum level of severity in order to  constitute cruel, inhuman or degrading treatment. However, the specific circumstances of the  incident or a particular act are very important to know whether that particular act or incident  constitutes torture or cruel, inhuman or degrading treatment. There exist several relevant factors, including the duration and effect of the treatment, the health, age and gender of the victim, as well as the particular treatment involved. Acts that fall under the threshold of cruel, inhuman or degrading treatment may, for our purposes, be called as ‘mistreatment’. (...)
> 
> (...) The US judicial bodies have made a clear distinction between cruel, inhuman and degrading treatment and torture. The US Department of Justice has previously suggested that the** following  acts would likely amount to torture**: (...) rape or sexual assault or **injury to an individual’s sexual organs, or threatening to do any of these sorts of acts**; (...)
> 

<hr class="thin-separator" />

"**A Comparative Analysis of Mental and Psychological Suffering as Torture, Inhuman or Degrading Treatment or Punishment under International Human Rights Treaty Law**" 
  <a href="http://projects.essex.ac.uk/ehrr/V4N1/Neziroglu.pdf" target="_blank">http://projects.essex.ac.uk/ehrr/V4N1/Neziroglu.pdf</a>

> (...) The main target of any kind of ill-treatment, physical or psychological, is human dignity. However, the  infliction of physical pain attracts more attention than mental suffering. Torture is the most serious form of  physical ill-treatment which directly hurts human dignity. The individual is reduced to a position of extreme  helplessness and distress, which causes his cognitive, emotional and behavioural functions to  deteriorate.
> 
> The victim is treated as an object at the hands of the torturer. At this point, the psychological destruction  of the victim becomes no less important than the physical pain. Cruel and inhuman treatment is somewhere between  physical and psychological ill-treatment, but includes both. When compared with torture and cruel and inhuman treatment, degrading treatment is much more related to  human psychology. It consists of subjective psychological elements such as humiliation and debasement. (...)
> 
> (...) There is a hierarchy between torture, inhuman and degrading treatment, or  punishment. Evans and Morgan refer to ‘a  progression,  starting  with  degrading  treatment’. Once ill-treatment reaches a certain threshold, it becomes inhuman. Torture is the most serious form. It is ‘at the apex of a pyramid of suffering’. Torture is an aggravated form of inhuman or degrading treatment and has a purpose, such as obtaining information or a confession, or to punish the victim. (...)
> 
> (...) The case studies prove that mental and psychological suffering alone may constitute torture, depending on its severity, seriousness and cruelty and the vulnerability of the victim. (...)
> 
> (...) The HRC considered the mock execution together with the denial of adequate medical care and decided that it constituted cruel and inhuman treatment within the meaning of Article 7 ICCPR. (...)
> 
> (...) A threshold of severity differentiates inhuman and degrading treatment. Any treatment must cause severe mental and physical suffering and has to be inflicted deliberately for it to be defined as inhuman. If the  suffering is inflicted for the benefit and with the consent of the recipient, then it is not a matter of ill-treatment. (...)
> 
> (...) Mental and Psychological Suffering as Degrading Treatment or Punishment Humiliation and debasement are   two fundamental concepts in the definition of degrading treatment. The treatment is accepted as degrading if  there is an intention to humiliate and if it grossly humiliates the victim before others or drives him to act  against his will or conscience. The Commission defines degrading treatment as an action that ‘lowers [the victim] in rank, position, reputation or character, whether in his own eyes or in the eyes of other people’, with the condition of reaching a certain level of severity. (...)
> 
> (...) The human rights system is based on the principle of respect for human dignity. The  mental forms of ill-treatment aim to destroy the moral integrity of the victim. Although  they do usually not leave any physical sign, the recovery from the mental suffering may take longer than the effects of physical methods. The jurisprudence of international human rights   treaty law proves that the severity of mental or psychological anguish, stress or discomfort may amount to torture. (...)
> 

<hr class="thin-separator" /> 

"**The Legal Prohibition Against Torture**"  
  <a href="https://www.hrw.org/news/2003/03/11/legal-prohibition-against-torture" target="_blank">https://www.hrw.org/news/2003/03/11/legal-prohibition-against-torture</a>
	
> (...) International law also prohibits mistreatment that does not meet the definition of torture, either because less severe physical or mental pain is inflicted, or because the necessary purpose of the ill-treatment is not present. It affirms the right of every person not to be subjected to cruel, inhuman or degrading treatment. (...) The prohibition against torture as well as cruel, inhuman or degrading treatment is not limited to acts causing physical pain or injury. It includes acts that cause mental suffering (...)
> (...) The prohibition against torture is well established under customary international law as jus cogens; that is, it has the highest standing in customary law and is so fundamental as to supercede all other treaties and customary laws (except laws that are also jus cogens). Criminal acts that are jus cogens are subject to universal jurisdiction, meaning that any state can exercise its jurisdiction, regardless of where the crime took place, the nationality of the perpetrator or the nationality of the victim. (...)
> (...) Common Article 3 to the Geneva Conventions, for example, bans "violence of life and person, in particular murder of all kinds, mutilation, cruel treatment and torture" as well as "outrages upon personal dignity, in particular humiliating and degrading treatment." (...)
> 

<hr class="thin-separator" />

"**Torture and Cruel, Inhuman or Degrading Treatment**"<br/>
 <a href="https://ihl-databases.icrc.org/customary-ihl/eng/docs/v1_rul_rule90" target="_blank">https://ihl-databases.icrc.org/customary-ihl/eng/docs/v1_rul_rule90</a>
	
> (...) **“torture or inhuman treatment” and “wilfully causing great suffering or serious injury to body or health” constitute grave breaches of the Geneva Conventions and are war crimes under the Statute of the International Criminal Court**. (...)
> 

<hr class="thin-separator" />

"**Patient's consent to perform a medical procedure**"<br/> 
 <a href="http://klaraandres.pl/2016/11/08/zgoda-pacjenta-na-wykonanie-zabiegu-medycznego/" target="_blank">http://klaraandres.pl/2016/11/08/zgoda-pacjenta-na-wykonanie-zabiegu-medycznego/</a>

> (…) Informational and legal obligations of a doctor, including a dentist, result mainly from Articles 31 and 34 of the Act of 5 December 1996 on the professions of a doctor and a dentist (Dz.U.2015 item 464): (…) Art. 34 1. Physician may perform an operation or use a method of treatment or diagnosis posing an increased risk to the patient, after obtaining his or her written consent. (…) from the Penal Code it follows that a medical procedure performed without the consent of the patient is an offense prosecuted at the request of the aggrieved party.(…)
> 

 
