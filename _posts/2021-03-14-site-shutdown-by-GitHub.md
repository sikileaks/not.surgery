---
title: Regarding recent shutdown of this site by GitHub
layout: post
date: '2021-03-14 01:04:00'
categories: other
ref: site-shutdown-by-github
lang: en
---

Due to recent shutdown of this site by GitHub (hosted thus far via GitHub pages), I'm in the process of implementing multiple alternative ways to access it.

This site is (obviously) moved already to a different hosting solution. Just in case shutdown happens again, for now here are two additional ways to access it:

 1) <a href="ipns://{{ site.ipns }}/" target="_blank">ipns://{{ site.ipns }}/</a> - utilizing IPNS (InterPlanetary Name System); IPFS (along with IPNS) is now built into "Brave Browser" (<a href="https://brave.com/brave-integrates-ipfs/" target="_blank">https://brave.com/brave-integrates-ipfs/</a>); alternatively, use "IPFS Desktop" (<a href="https://docs.ipfs.io/install/ipfs-desktop/" target="_blank">https://docs.ipfs.io/install/ipfs-desktop/</a>) or any other IPFS client with support for IPNS links;

 2) <a href="https://notsurgery.fuckyou.solutions/" target="_blank">https://notsurgery.fuckyou.solutions/</a> - this however relies on DNS and (in worst case scenario) is subject to network-level censorship


More to come!!!
