---
title: State terrorism in the 21st century
layout: post
date: '2019-07-23 18:48:00'
categories: other
ref: video-presentation-july-2019
lang: en
image: /pliki/Prezentacja-201907/not-surgery_presentation.jpg
---

## How mass surveillance, sold as means to fight terrorism, is being used to terrorize populations and implement “invisible” totalitarianism

Short summary in video presentation form.

<div class="videoWrapper">
  <iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/AmwlRVHCslm4/"></iframe>
</div>

<div class="videoExtras">
  <strong>Download</strong>: <a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.mp4" | relative_path }}" target="_blank">MP4 version</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.webm" | relative_path }}" target="_blank">WEBM version</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-slides.pdf" | relative_path }}" target="_blank">slides (PDF)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-narration.pdf" | relative_path }}" target="_blank">narration (PDF)</a>
</div>

<br/>

In the so called post-Snowden / post-Assange era, awareness of mass surveillance maybe nothing new to you.. but what if you found out that these same technologies, which were officially sold to the public as ways to prevent terrorism, are being used in the exact opposite way, i.e.:

 * To cover up crimes, including medical / scientific experimentation without consent
 * To deny your right to live, including preventing you from diagnosing your health condition
 * To deny your free choice of employment and means of subsistence
 * To prevent you from freely associating and cooperating with people who share your interests

In effect, instead of fighting terrorism, to terrorize, to cause inhuman or degrading treatment or punishment – turning upside down the most important international laws, those enshrined in the UN Declaration of Human Rights, the European Convention on Human Rights, as well as constitutions of most countries of this planet.

Wouldn't an “almost” perfect, invisible dictatorship / totalitarian regime be one where grievous crimes are taking place, but awareness of such is kept away from you, and, most importantly, even when you found out, you are “systematically” prevented from doing anything to stop such, and to fix the destruction which these caused?
