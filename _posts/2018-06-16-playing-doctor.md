---
title: Krakow law enforcement authorities playing doctor
layout: post
date: '2018-06-16 22:15:00'
categories: medical
ref: lets-pretend
lang: en
---

Law enforcement authorities have no right to issue medico-legal opinions without appointing a medico-legal expert and performing a forensic examination.

Neither prosecutor Bartlomiej Legutko nor senior constable Daria Curzydło, who were conducting pre-investigation regarding notification of a crime submitted on 28.02.2014, had any special knowledge to base their decision refusing to initiate an investigation dated April 14 2014 (<a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_target">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a>, Polish language version, original) on the grounds that candidiasis is the cause of the observed permanent bodily injuries.

Set of medical documentation collected since the events of January 2014 unquestionably confirms the action of 3rd parties, which resulted in permanent bodily injury, including genitourinary and nervous system damage, as well as scars left from puncture wounds in groin area (<a href="/medyczne/2018/06/16/actual-diagnosis.html" target="_blank">/medyczne/2018/06/16/actual-diagnosis.html</a>).

Prior to the events of January 2014, I did not suffer from any serious illness and none of the symptoms described were present – I only treated myself for candidiasis of the digestive system and was taking medications prescribed to me by the physician I consulted (<a href="/pliki/Medyczne/Kandydoza_Intermed_2013.EN.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.EN.pdf</a>).

Justification provided in the decision refusing to initiate an investigation is absurd – candidiasis cannot and is not the cause of the observed bodily injuries and damage to genitourinary and nervous system as well as scars left from puncture wounds in groin area (<a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a>, Polish language version, original). 

Most importantly, given that the mutilation concerns the most intimate parts of the body, and punishable threats I received a few days before the events of January 2014 and which were fulfilled as a result of these events, were trivialized at least twice (“*do not constitute a criminal offence*”) by Krakow law enforcement authorities, which together with other gross violations of procedural obligations in each notification of a crime I submitted in the last 4 years, proves not only that these were deliberately carried out, but it is evident criminal behavior aimed at concealing the crime.
