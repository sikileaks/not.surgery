---
title: Zmowa kryminalna czy “tylko“ zaniedbanie medyczne–przypadek błędnej diagnozy i odmowy opieki ze strony lekarzy Apollo Hospital
layout: post
date: '2018-12-06 00:01:00'
categories: medyczne
ref: apollo-criminal-collusion-or-medical-negligence
lang: pl
---

Oryginalna publikacja na razie tylko w j. angielskim: <a href="/medical/2018/12/06/apollo-criminal-collusion-or-medical-negligence.html" target="_blank">/medical/2018/12/06/apollo-criminal-collusion-or-medical-negligence.html</a>

***AKTUALIZACJA: Ten pozew jest już rozstrzygnięty. Kliknij po więcej informacji: <a href="/medyczne/2019/01/03/pierwszy-pozew-o-zaniedbanie-medyczne-rozstrzygniety.html" target="_blank">/medyczne/2019/01/03/pierwszy-pozew-o-zaniedbanie-medyczne-rozstrzygniety.html</a>***
