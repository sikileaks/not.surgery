---
title: New notification of a crime was filed
layout: post
date: '2018-12-11 14:51:00'
categories: legal
ref: new-notification-of-crime
lang: en
---

New notification of a crime was filed. 

<a href="/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf" target="_blank">/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf</a>
