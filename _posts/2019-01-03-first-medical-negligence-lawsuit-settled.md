---
title: First of medical negligence lawsuits settled
layout: post
date: '2019-01-03 00:22:10'
categories: medical
ref: first-medical-negligence-lawsuit-settled
lang: en
---

Lawsuit which was filed in mid-August 2018 in New Delhi District Court was successfully settled, just before Reply to Opposition along with additional evidence was to be filed in Court.

 * <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf" target="_blank">/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf</a>
 * <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.index.pdf" target="_blank">/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.index.pdf</a>


## Annexures

A. Copy of email communication with Opposite Party No. 4, between February 9th and February 15th 2018, prior to coming to India 
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180209-15.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180209-15.pdf</a>

B. Copy of referrals for diagnostic imaging examinations with brief description of symptoms, issued by Dr. Andrzej Gliwa
 <a href="/pliki/Prawne/Apollo/CMIntermed_referrals_20180220.pdf" target="_blank">/pliki/Prawne/Apollo/CMIntermed_referrals_20180220.pdf</a>

C. Copy of confirmations of booking of two diagnostic imaging examinations from February 15th 2018
 <a href="/pliki/Prawne/Apollo/Apollo_booking-of-examinations_20180215.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_booking-of-examinations_20180215.pdf</a>

D. Copy of legal notice from April 4th 2018
 <a href="/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018.pdf</a>

E. Copy of proof of posting of legal notice on April 4th 2018
 <a href="/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-posting.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-posting.pdf</a>

F. Copy of proof of delivery of legal notice on April 5th 2018
 <a href="/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-delivery.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-delivery.pdf</a>

G. Copy of email communication with Opposite Party No. 4 from June 20th 2018, asking regarding reply to legal notice
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180620.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180620.pdf</a>

H. Copy of email communication with Opposite Party No. 4 from June 25th 2018, asking again regarding reply to legal notice
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180625.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180625.pdf</a>

I. Copy of Whatsapp communication with Opposite Party No. 4 from June 25th 2018, asking again regarding reply to legal notice
 <a href="/pliki/Prawne/Apollo/Apollo_whatsapp_20180625.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_whatsapp_20180625.pdf</a>

J. Copy of payment receipt for sworn translation service from March 9th 2018 for the amount of 4000 INR
 <a href="/pliki/Prawne/Apollo/Modlingua_bill_20180309.pdf" target="_blank">/pliki/Prawne/Apollo/Modlingua_bill_20180309.pdf</a>

K. Copy of email communication with translator, between March 10th and March 13th 2018
 <a href="/pliki/Prawne/Apollo/Modlingua_email-communication_20180310-13.pdf" target="_blank">/pliki/Prawne/Apollo/Modlingua_email-communication_20180310-13.pdf</a>

L. Copy of sworn English translation of report from Computed Tomography examination of pelvis, conducted on February 13th 2018
 <a href="/pliki/Prawne/Apollo/CT-of-pelvis_Radiology-Center-Vienna_20180213.report.EN-translation.pdf" target="_blank">/pliki/Prawne/Apollo/CT-of-pelvis_Radiology-Center-Vienna_20180213.report.EN-translation.pdf</a>

M. Copy of visual description of symptoms Complainant provided to Opposite Parties in first email from February 9th 2018
 <a href="/pliki/Prawne/Apollo/MHSiemaszko_symptoms_infographic.pdf" target="_blank">/pliki/Prawne/Apollo/MHSiemaszko_symptoms_infographic.pdf</a>

N. Copy of report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6th and 20th 2018
 <a href="/pliki/Prawne/Apollo/Neurologist%26Plastic-Surgeon_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/Neurologist%26Plastic-Surgeon_Millesi-Center-Vienna_20180420.oryginal.pdf</a>

O. Copy of report from Optical Coherence Tomography examination conducted on April 6th 2018
 <a href="/pliki/Prawne/Apollo/OCT-of-scar_Vienna_20180406.original.pdf" target="_blank">/pliki/Prawne/Apollo/OCT-of-scar_Vienna_20180406.original.pdf</a>

P. Copy of report from Ultrasound examination of nervous system, conducted on April 6th 2018
 <a href="/pliki/Prawne/Apollo/USG-of-nerves_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/USG-of-nerves_PUC-Vienna_20180413.oryginal.pdf</a>

Q. Copy of report from Ultrasound examination of left lateral abdominal wall, conducted on March 8th 2018
 <a href="/pliki/Prawne/Apollo/USG-of-scar_Mahajan-Imaging_20180308.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/USG-of-scar_Mahajan-Imaging_20180308.oryginal.pdf</a>

R. Copy of report from Magnetic Resonance examination of nervous system, conducted on March 12th 2018
 <a href="/pliki/Prawne/Apollo/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>

S. Copy of report from MR & CT Urography, conducted on March 12th 2018
 <a href="/pliki/Prawne/Apollo/CT%26MR-Urography_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/CT%26MR-Urography_Mahajan-Imaging_20180312.oryginal.pdf</a>

T. Copy of report from Ultrasound examination of urinary tract, conducted on January 11th 2016
 <a href="/pliki/Prawne/Apollo/USG_St-Medica_20160111.report.original.pdf" target="_blank">/pliki/Prawne/Apollo/USG_St-Medica_20160111.report.original.pdf</a>

U. Copy of sworn English translation of report from Ultrasound examination of urinary tract, conducted on January 29th 2014
 <a href="/pliki/Prawne/Apollo/USG_Ultramedica_20140129.report.EN-translation.pdf" target="_blank">/pliki/Prawne/Apollo/USG_Ultramedica_20140129.report.EN-translation.pdf</a>

V. Copy of email communication with Opposite Party No. 2 from March 2nd 2018, summarizing in-person meeting with Opposite Party No. 2
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180302_1.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180302_1.pdf</a>

W. Copy of email communication with Opposite Party No. 4 and No. 2 from March 3rd 2018
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180303.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180303.pdf</a>

X. Copy of payment receipt for PET/CT examination to be conducted on March 6th 2018 at “The Pet Suite”/Department of Molecular Imaging & Nuclear Medicine of Apollo Hospital in New Delhi, India
 <a href="/pliki/Prawne/Apollo/Apollo_PET-CT_invoice_20180306.refund.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_PET-CT_invoice_20180306.refund.pdf</a>

Y. Copy of email communication with Opposite Party No. 4 from March 5th 2018
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180305.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180305.pdf</a>

Z. Copy of email communication with Opposite Party No. 4, between March 6th and March 7th 2018, regarding refund for canceled PET/CT examination
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180307.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180307.pdf</a>
