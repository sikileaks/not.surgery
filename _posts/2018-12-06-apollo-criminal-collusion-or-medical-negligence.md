---
title: Criminal collusion or “only“ medical negligence–a case of misdiagnosis and denial of care by Apollo Hospital doctors
layout: post
date: '2018-12-06 00:01:00'
categories: medical
ref: apollo-criminal-collusion-or-medical-negligence
lang: en
---

Awareness must be raised and pressure exerted so medical facilities which deem themselves as “best of the best” really serve up to that name and it is not just an empty slogan.

In the case of medical tourism, this is even more important because of the additional costs involved and time constraints, and in the event of misdiagnosis and denial of care–which both happened in this specific situation–potential damages are even greater. 

If crime is result of bodily injuries which are to be diagnosed, doctors should be particularly careful in properly utilizing their skills, because misdiagnosis, together with denial of care, only plays into hands of perpetrators of such injuries.

Below is short summary of this particular case. Lawsuit along with all evidence is published at <a href="/medical/2018/08/20/first-medical-negligence-lawsuit.html" target="_blank">/medical/2018/08/20/first-medical-negligence-lawsuit.html</a>

 1. __Before making travel arrangements and coming to India__
   * I confirmed availability of specific diagnostic imaging examinations at Apollo Hospital in New Delhi
   * I booked both examinations
   * I obtained referrals from Doctor for both examinations
   * I obtained medical visa, booked flight and accommodation
   
 
 2. __After I arrived in India__ 
   * First examination, conducted on March 5th 2018, was clearly a medical negligence issue
   * Second examination was supposed to be conducted on March 6th 2018, but after I made full payment, while already in hospital clothes ready to take the exam, Apollo staff informed me they are canceling the exam
   * Because of this, I absolutely had to conduct these examinations elsewhere–I came to India on a medical visa specifically to do these exams
   * Mahajan Imaging in New Delhi was able to perform high-resolution CT and 3 Tesla MRI including Neurography–not the same exams as those from referrals (e.g. PET/MRI), but similar, so I booked them
   * Mahajan Imaging conducted first USG examination on March 8th then CT and MRI examinations, including functional imaging of nerves, on March 12th–results  from these exams clearly corroborated with symptoms which Apollo Hospital dismissed / erroneously diagnosed
   * Before leaving India, I met with medical negligence advocate from New Delhi, describing this situation as well as providing all relevant documentation–he confirmed he sees this as medical negligence
 
 
 3. __After I returned from India__
   * I also conducted focused neurological examination and consultation with neurosurgeon, plastic surgeon and radiologist–which, altogether, give 9 medical reports from 8 different medical professionals, which all corroborate symptoms which Apollo Hospital doctors were made aware of in writing and in person, but in their report they claimed they see absolutely no problems
   * Together with advocate from India demand for payment letter was prepared and delivered to Apollo Hospital doctors on April 5th 2018
   * I waited close to 3 months for response from Apollo Hospital–they misinformed again, saying they sent a reply but no such reply was ever received by neither myself nor my legal representative
   * Together with advocate from India work on lawsuit started and in mid-August 2018 it was filed (<a href="/medical/2018/08/20/first-medical-negligence-lawsuit.html" target="_blank">/medical/2018/08/20/first-medical-negligence-lawsuit.html</a>)–because of costs involved (the failed PET/MRI examination by itself cost 60,500 INR), the fact that Apollo Hospital committed diagnostic error with first examination then canceled the other examination, the fact that I only made travel arrangements after receiving confirmation of availability from Apollo Hospital and booked both tests, the fact that I specifically came to India as a medical tourist, the fact that they not only refused to react in a timely manner and reply to demand for payment letter so this is settled out of court but even after close to 3 months, on June 20th, they misinformed about replying to that letter


 4. __After 7 months since receiving demand for payment letter and 3 months after lawsuit was filed, Apollo Hospital doctors__ 
   * Say they overlooked the demand for payment letter, despite claiming 5 months earlier that they replied to it, giving further proof of conflicting, erroneous statements,
   * Attempt to misstate facts or completely dismiss factual information, including all 9 medical reports from 8 different medical professionals, which clearly corroborate symptoms which I suffer from and came to India to have them further examined via specialist diagnostic imaging exams so I can receive comprehensive diagnosis and treatment recommendations
   * Attempt to dismiss the fact that their unprofessional behavior—including misdiagnosis and denial of care–caused damages
   * Ignore the fact that a major side effect of properly conducted medical examinations is not only a factual, comprehensive diagnosis and treatment recommendations, but also being able to prepare forensic expertise, considering all these injuries are a result of a criminal assault on me–that cause of this is a criminal issue they were also informed, yet they committed diagnostic error and denied conducting the other examination, which is definitely a big favor–not for me, but for those responsible for these injuries to my body
   

***UPDATE: This lawsuit is now settled. Click for more info: <a href="/medical/2019/01/03/first-medical-negligence-lawsuit-settled.html" target="_blank">/medical/2019/01/03/first-medical-negligence-lawsuit-settled.html</a>***

