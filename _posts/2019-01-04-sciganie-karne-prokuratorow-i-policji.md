---
title: Ściganie karne prokuratorów i policji ukrywającej przestępstwo wykonania zabiegu medycznego bez zgody
layout: post
date: '2019-01-04 00:35:10'
categories: legal
ref: criminally-prosecuting-law-enforcement
lang: pl
---

Jest wysoce mało prawdopodobne aby funkcjonariusze organów procesowych nie mieli świadomości, że swoim zachowaniem w postępowaniach z zawiadomień z dnia 28.02.2014 oraz 9.01.2018 przekraczają przysługujące im uprawnienia bądź nie dopełniają ciążących na nich obowiązków a liczne błędy i zaniechania popełnione jest niezmiernie trudno ocenić inaczej jak świadome działanie mające na celu uniemożliwienie mi przeprowadzenie rzetelnego śledztwa, zamykając tym samym drogę procesową i uzyskanie odszkodowania za ogromne szkody jakie poniosłem–sięgające setek tysięcy PLN. Co najważniejsze, zachowania te noszą znamiona celowego ukrywania przestępstwa, w tym m.in. wykonania zabiegu medycznego bez mojej zgody.

W związku z powyższym, przeciwko 4 krakowskim prokuratorom oraz 1 funkcjonariuszowi Policji zostały złożone zawiadomienia o popełnieniu przestępstwa z art. 231 Kodeksu Karnego:

 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf</a>
 * <a href="/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf" target="_blank">/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf</a>


Dnia 27 grudnia 2018 za pośrednictwem Prokuratury Rejonowej w Tarnowie do Sądu zostało skierowane zażalenie na postanowienie o odmowie wszczęcia śledztwa: <a href="/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf</a>. Biorąc pod uwagę, iż sprawa dotyczy trwałego uszkodzenia ciała w związku z wykonaniem zabiegu medycznego bez zgody, a następnie–m.in. poprzez przekraczanie uprawnień i zaniechania–próbę ukrycia tego przestępstwa przez "kolegów po fachu" Pani Prokurator, domyślam się że trudno było podjąć inna decyzję niż to zaskarżone postanowienie. Zobaczymy jak Sąd oceni zebrany materiał dowodowy i w zależności od tego następne kroki zostaną podjęte przeze mnie.
