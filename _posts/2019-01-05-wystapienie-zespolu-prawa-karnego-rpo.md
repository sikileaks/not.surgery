---
title: Zespół Prawa Karnego Biura Rzecznika Praw Obywatelskich zwrócił się do Prokuratora Regionalnego w Krakowie blisko 6 miesięcy temu
layout: post
date: '2019-01-05 00:22:10'
categories: prawne
ref: criminal-law-team-of-the-ombudsman-address
lang: pl
---

Sprawą rażących naruszeń przepisów w postępowaniach prowadzonych w związku z wydarzeniami stycznia 2014 r. zainteresował się Zespół Prawa Karnego Biura Rzecznika Praw Obywatelskich, zwracając się do Prokuratora Regionalnego w Krakowie z prośba o zbadanie akt sprawy pod kątem oceny ich prawidłowości.

> (…) Lektura udostępnionych przez Pana Michała SIEMASZKO kopii dokumentów bezsprzecznie potwierdza, że omawiane postępowanie przygotowawcze nie było prowadzone przeciwko określonej osobie i w efekcie zakończono je odmową wszczęcia śledztwa w fazie postępowania in rem. W powyższym układzie procesowym prawnie nie jest możliwe zaskarżenie przez Rzecznika Praw Obywatelskich zatwierdzonego w tym przypadku przez prokuratora postanowienia o odmowie wszczęcia śledztwa 2 Ds. 385/14 w drodze wniesienia nadzwyczajnego środka zaskarżenia–kasacji, o czym pisemnie poinformowano Pana Michała SIEMASZKO. Niewątpliwie bowiem przepisy Kodeksu postępowania karnego nie przewidują w ogóle możliwości nadzwyczajnego zaskarżenia kasacją postanowień wydanych przez prokuratora.
> 
> Niemniej jednak **już wstępna analiza** wniosku i załączonej do niego korespondencji oraz przekazanych Rzecznikowi kopii dokumentów dot. omawianej sprawy **nasuwa pewne wątpliwości. Chociażby proste porównanie treści sentencji postanowienia o odmowie wszczęcia śledztwa 2 Ds. 385/14 Prokuratury Rejonowej Kraków-Krowodrza w Krakowie z zawiadomieniem Pana SIEMASZKO z dnia 9 stycznia 2018 roku** o popełnieniu różnych przestępstw–skierowanych głownie przeciwko zdrowiu–**wskazuje, iż te czyny nie były w ogóle przedmiotem zainteresowania i prawnokarnego odniesienia się w sprawie 2 Ds. 385/14**. (…)
> 

Pełna wersja dostępna pod adresem: <a href="/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf" target="_blank">/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf</a>

Wbrew obowiązującym przepisom, tj. art. 17 (<a href="https://www.arslege.pl/ustawa-o-rzeczniku-praw-obywatelskich/k57/a5495/" target="_blank">"Obowiązek współdziałającej z Rzecznikiem organizacji, organu"</a>) ustawy o Rzeczniku Praw Obywatelskich, pomimo upływu blisko 6 miesięcy, Prokurator Regionalny nie udzielił jeszcze wyczerpujących odpowiedzi.
