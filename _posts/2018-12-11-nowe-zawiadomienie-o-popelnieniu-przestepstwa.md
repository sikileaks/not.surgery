---
title: Nowe zawiadomienie o popełnieniu przestępstwa złożone
layout: post
date: '2018-12-11 14:51:00'
categories: prawne
ref: new-notification-of-crime
lang: pl
---

W związku z błędnym uznaniem charakteru procesowego złożonego przeze mnie dnia 9.01.2018 zawiadomienia o popełnieniu przestępstwa za wniosek o wszczęcie śledztwa w sprawie sygn. akt 2 Ds. 385/14–pomimo tego, iż czyny wskazane nie były przedmiotem zainteresowania i prawnokarnego odniesienia w postępowaniu z zawiadomienia złożonego 28.02.2014– oraz nowych i istotnych dowodów i wątków, dnia 11 grudnia 2018 r. zostało złożone nowe zawiadomienie o popełnieniu przestępstwa z art. 156 §1, art. 157 §1, art. 160 §1, art. 162 §1, art. 192 §1, art. 193, art. 239, art. 258 oraz art. 268 Kodeksu Karnego, równocześnie naruszenie art. 30, art. 32, art. 38, art. 39, art. 40, art. 45, art. 47 oraz art. 77 Konstytucji RP, w zw. z art. 44 Konstytucji RP, jak również art. 2, art. 3, art. 6, art. 13 oraz art. 14 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności, w zw. z art. 9 Konstytucji RP: <a href="/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf" target="_blank">/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf</a>

Dla przypomnienia: biorąc pod uwagę przestępstwa jakich się dopuszczono, bezpośrednio z kodeksu postępowania karnego płynie wymóg rozpoczęcia postępowania przygotowawczego, a jeśli funkcjonariusze publiczni zamieszani są w przestępstwa wynikiem których jest trwałe uszkodzenie ciała–wg. obowiązującego polskie organy procesowe orzecznictwa sądów międzynarodowych–wyjaśnienie przyczyny ich powstania.

Jest wysoce mało prawdopodobne aby funkcjonariusze organów procesowych nie mieli świadomości, że swoim zachowaniem w postępowaniach z zawiadomień z dnia 28.02.2014 oraz 9.01.2018 przekraczają przysługujące im uprawnienia bądź nie dopełniają ciążących na nich obowiązków a liczne błędy i zaniechania popełnione jest niezmiernie trudno ocenić inaczej jak świadome działanie mające na celu uniemożliwienie mi przeprowadzenie rzetelnego śledztwa, zamykając tym samym drogę procesową i uzyskanie odszkodowania za ogromne szkody jakie poniosłem–sięgające setek tysięcy PLN. Co najważniejsze, zachowania te noszą znamiona celowego ukrywania przestępstwa, w tym m.in. wykonania zabiegu medycznego bez mojej zgody.

W związku z powyższym, przeciwko 4 krakowskim prokuratorom oraz 1 funkcjonariuszowi Policji zostały złożone zawiadomienia o popełnieniu przestępstwa z art. 231 Kodeksu Karnego: 
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf</a>
 * <a href="/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf" target="_blank">/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf</a>
