---
title: Odnośnie niedawnego zamknięcia tego serwisu przez GitHub
layout: post
date: '2021-03-14 01:04:00'
categories: inne
ref: site-shutdown-by-github
lang: pl
---

Ze względu na niedawne zamknięcie tego serwisu przez GitHub (do tej pory hostowany via GitHub pages), jestem w trakcie wdrażania alternatywnych metod uzyskania dostępu.

Serwis korzysta już (oczywiście) z innego rozwiązania hostingowego. Na wypadek ponownego wyłączenia, na razie istnieją dwa dodatkowe sposoby uzyskania do niego dostępu:

  1) <a href="ipns://{{ site.ipns }}/" target="_blank">ipns://{{ site.ipns }}/</a> - wykorzystując IPNS (InterPlanetary Name System); IPFS (razem z IPNS) jest teraz wbudowany już w „Brave Browser” (<a href="https://brave.com/brave-integrates-ipfs/" target="_blank">https://brave.com/brave-integrates-ipfs/</a>); możesz również użyć „IPFS Desktop” (<a href="https://docs.ipfs.io/install/ipfs-desktop/" target="_blank">https://docs.ipfs.io/install/ipfs-desktop/</a>) lub dowolnego innego klienta IPFS z obsługą łączy IPNS;

  2) <a href="https://notsurgery.fuckyou.solutions/" target="_blank">https://notsurgery.fuckyou.solutions/</a> - ta metoda dostępu jednak opiera się na DNS i (w najgorszym przypadku) podlega cenzurze na poziomie sieci

Więcej wkrótce!!!
