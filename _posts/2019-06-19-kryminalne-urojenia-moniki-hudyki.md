---
title: Kryminalne urojenia Moniki Hudyki
layout: post
date: '2019-06-19 20:53:00'
categories: prawne
ref: hudyka-criminal-delusions
lang: pl
---

Monika Hudyka–osoba ewidentnie chora umysłowo, która przyznała się w wiadomościach, które wymieniliśmy i których autentyczność została następnie poświadczona przez specjalistów z dziedziny informatyki sadowej, że cierpi na zaburzenia osobowości typu borderline–skontaktowała się ze mną ostatnio. Napisała do mnie maila–nie po to żeby przeprosić czy wyjaśnić ale–po to by grozić mi pozwem o zniesławienie, jeśli nie usunę treści publikowanych w tym serwisie.

Monika, podobnie jak spora część polskiego społeczeństwa, cierpi na urojenia / choroby psychiczne–ani ona, ani ci, którzy podążają za tym szalonym człowiekiem, nie pojmują chyba, w czym uczestniczyli i nadal uczestniczą, ani wziąć odpowiedzialności za swoje działania. Zamiast tego nadal zaprzeczają i grożą, w tym przypadku pozwem o zniesławienie.

Biedna, rozbita Monika i jej kryminalnie obłąkana ekipa musi zmierzyć się z faktem, iż wszystkie informacje publikowane na tej stronie są w 100% weryfikowalne, pochodzą od specjalistów z ich dziedzin, w tym światowej klasy lekarzy, i zacząć się leczyć. Wasze miejsce jest w więzieniu lub w zakładzie psychiatrycznym.

 * Kopia wiadomości email otrzymanej od Moniki Hudyki w kwietniu 2019 r.: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Hudyka.20190408.pdf" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Hudyka.20190408.pdf</a>

 * Kopia zawiadomienia o popełnieniu przestępstwa z art. 156 §1, art. 158 §, art. 159, art. 193, art. 233 § 1, art. 234, art. 236 § 1, art. 238, art. 239 § 1, art. 258 § 1 Kodeksu Karnego przez p. Monikę Hudykę: <a href="/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf</a>

 * Kopia wiadomości SMS przychodzących / wychodzących na numer telefonu Moniki Hudyki, w okresie od 04.03.2014 r. Do 30.03.2014 r., poświadczona przez specjalistów z dziedziny informatyki sadowej: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_GT-E1200_721030078_raport.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_GT-E1200_721030078_raport.SIGNED.pdf</a>

 * Kopia protokołu zabezpieczenia komunikacji SMS przez specjalistów z dziedziny informatyki sadowej wraz z fakturą i protokołem odbioru: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>

 * Kopia fragmentu komunikacji na Facebook Messenger z Moniką Hudyką, kiedy dnia 3 lutego 2014 r. zainicjowała kontakt ze mną: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Screenshot_20181219-172035_Messenger.jpg" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Screenshot_20181219-172035_Messenger.jpg</a>

screencasts of facebook messenger "invitation" received from hudyka on february 3 2014...:
 * <a href="/pliki/Komunikacja/20200416-234727.EZ-VideoRecorder.1.mp4" target="_blank">screencast #1</a>
 * <a href="/pliki/Komunikacja/20200416-235002.EZ-VideoRecorder.2.mp4" target="_blank">screencast #2</a>
 * <a href="/pliki/Komunikacja/2020-04-16-235248.mp4" target="_blank">screencast #3</a>

![M. Hudyka](/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Screenshot_20181219-172035_Messenger.jpg)
