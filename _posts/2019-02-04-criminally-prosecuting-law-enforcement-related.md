---
title: Notifications of crime under art. 231 §1 of the Penal Code against public prosecutors and police in a related matter
layout: post
date: '2019-02-04 14:00:00'
categories: legal
ref: criminally-prosecuting-law-enforcement-related
lang: en
---

On February 4th 2019 r. I filed 4 additional notifications of crimes under art. 231 §1 of the Penal Code against public prosecutors and police in a related matter:

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf"  target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf</a>

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf</a>

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf</a>

 * <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf</a>

So far it is 9 notifications of a crime under art. 231 §1 of the Penal Code against Krakow "law enforcement" authorities (see <a href="/legal/2019/01/04/criminally-prosecuting-law-enforcement.html" target="_blank">/legal/2019/01/04/criminally-prosecuting-law-enforcement.html</a>

