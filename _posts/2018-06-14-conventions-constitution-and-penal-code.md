---
title: What do the UN Declaration of Human Rights, the European Convention on Human Rights, the Constitution of the Republic of Poland and the Penal Code have to say about this?
layout: post
date: '2018-06-14 17:55:00'
categories: legal
ref: conventions-constitution-and-penal-code
lang: en
---

Selected articles of the UN Declaration of Human Rights, the European Convention on Human Rights, the Polish Constitution and the Penal Code, concerning medical procedures/treatments without consent, damage to the genital system and refusing medical assistance.

<hr class="thin-separator" />

# The Penal Code
* Article 156. para. 1 – Grievous bodily harm 
> Whoever causes grievous bodily harm in a form which: <br/>
> 1) deprives a human being of sight, hearing, speech or the ability to procreate, or <br/>
> 2) inflicts on another a serious crippling injury, an incurable or prolonged illness, an illness actually dangerous to life, a permanent mental illness, a permanent total or substantial incapacity to work in an occupation, or a permanent serious bodily disfigurement or deformation <br/>
> shall be subject to the penalty of the deprivation of liberty for a term of between 1 and 10 years. 
> 

* Article 162. para. 1 – Not granting assistance 
> Whoever does not render assistance to a person who is in a situation threatening an immediate danger of loss of life, serious bodily injury, or a serious impairment thereof, when he so do without exposing himself or another person to the danger of loss of life or serious harm to health shall be subject to the penalty of deprivation of liberty for up to 3 years.
> 

* Article 192. para. 1 – Medical operation without consent 
> Whoever performs a medical operation without the consent of the patient shall be subject to a fine, the penalty of restriction of liberty or the penalty of deprivation of liberty for up to 2 years.
> 

<hr class="thin-separator" />

# The Constitution Of The Republic Of Poland
* Article 9. 
> The Republic of Poland shall respect international law binding upon it.
> 

* Article 30. 
> The inherent and inalienable dignity of the person shall constitute a source of freedoms and rights of persons and citizens. It shall be inviolable. The respect and protection thereof shall be the obligation of public authorities.
> 

* Article 39. 
> **No one shall be subjected to scientific experimentation, including medical experimentation, without his voluntary consent.**
> 

* Article 40. 
> No one may be subjected to torture or cruel, inhuman, or degrading treatment or punishment. The application of corporal punishment shall be prohibited.
> 

* Article 47. 
> Everyone shall have the right to legal protection of his private and family life, of his honour and good reputation and to make decisions about his personal life.
> 

* Article 77. 
> 1\. Everyone shall have the right to compensation for any harm done to him by any action of an organ of public authority contrary to law. <br/>
> 2\. Statutes shall not bar the recourse by any person to the courts in pursuit of claims alleging infringement of freedoms or rights.
> 

<hr class="thin-separator" />

# European Convention on Human Rights
“Convention for the Protection of Human Rights and Fundamental Freedoms” 
<a href="https://www.coe.int/en/web/conventions/full-list/-/conventions/treaty/005" target="_blank">https://www.coe.int/en/web/conventions/full-list/-/conventions/treaty/005</a>
	
* Article 2 – Right to life 
> Everyone's right to life shall be protected by law. (…)
> 

* Article 3 – Prohibition of torture
> No one shall be subjected to torture or to inhuman or degrading treatment or punishment.
> 

<hr class="thin-separator" />

# The UN Declaration of Human Rights
“The International Covenant on Civil and Political Rights of December 19 1996” 
<a href="https://treaties.un.org/Pages/ViewDetails.aspx?src=TREATY&mtdsg_no=IV-4&chapter=4&lang=en" target="_blank">https://treaties.un.org/Pages/ViewDetails.aspx?src=TREATY&mtdsg_no=IV-4&chapter=4&lang=en</a>
	
* Article 2, para. 3
> Each State Party to the present Covenant undertakes: 
> a) To ensure thet any person whose rights or freedoms as herein recognized are violated shall have an effective remedy, notwithstanding that the violation has been committed by persons acting in an officiai capacity; 
> 
	 
* Article 7.
> No one shall bе subjected to torture or to cruel, inhuman or degrading treatment or punishment. **In particular, no one shall be subjected without his free consent to medical or scientific experimentation.**
> 
	
* Article 17, para. 1 
> No one shall be subjected to arbitrary or unlawful interference with his privacy, family, home or correspondence, nor to unlawful attacks on his honour and reputation.
> 

