---
title: Criminal Law Team of the Office of the Ombudsman addressed Krakow Regional Prosecutor close to 6 months ago
layout: post
date: '2019-01-05 00:22:10'
categories: legal
ref: criminal-law-team-of-the-ombudsman-address
lang: en
---

The Criminal Law Team of the Office of the Ombudsman became interested in the case of flagrant violations of the provisions of law in proceedings conducted in connection with the events of January 2014, asking the Regional Prosecutor in Kraków to examine the case files in terms of assessing their correctness.

> (…) Reading copies of documents made available by Mr Michał SIEMASZKO undoubtedly confirms that the discussed preparatory proceedings were not conducted against a specific person and as a result they were ended with the refusal to initiate an investigation at the stage of in rem proceedings. In the above procedural system, it is legally impossible for the Ombudsman to appeal against the decision, approved in this case by the public prosecutor, refusing to open an investigation 2 Ds. 385/14 by way of an extraordinary appeal-cassation measure, of which Mr Michał SIEMASZKO was informed in writing. Undoubtedly, the provisions of the Code of Criminal Procedure do not provide at all for the possibility of an extraordinary appeal by cassation of decisions issued by the prosecutor.
>
> Nevertheless, **even a preliminary analysis** of the application and the correspondence attached thereto, as well as copies of documents provided to the Ombudsman concerning the case in question **raises some doubts. For example, a simple comparison of the content of the sentence of the decision refusing to initiate an investigation of 2 Ds. 385/14 of the Kraków-Krowodrza Regional Prosecutor's Office with Mr. SIEMASZKO's notification of 9 January 2018** of committing various crimes directed mainly against health **indicates that these acts were not of interest and criminal-law reference in the case of 2 Ds. 385/14 at all**. (…)
>

Full version available at: <a href="/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf" target="_blank">/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf</a>

Contrary to the applicable regulations, i.e. Article 17 (<a href="https://www.arslege.pl/ustawa-o-rzeczniku-praw-obywatelskich/k57/a5495/" target="_blank">"Obligation of the organisation or body cooperating with the Ombudsman"</a>) of the Act on the Ombudsman, despite the passage of nearly 6 months, the Regional Prosecutor has not yet provided comprehensive answers.

