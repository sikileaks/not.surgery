---
title: 'Rzeczywista diagnoza: podziurawiona pachwina i paraliż funkcji ukł. moczowo-płciowego
  i nerwowego'
layout: post
date: '2018-06-16 18:30:00'
categories: medyczne
ref: actual-diagnosis
lang: pl
---

Komplet dokumentacji medycznej zebranej od czasu wydarzen w styczniu 2014 r. bezsprzecznie potwierdza działanie osób 3cich, wynikiem którego jest trwałe uszkodzenie ciała, w tym ukł. moczowo-płciowego i nerwowego, oraz blizny pozostałe po ranach kłutych.

1\. <a href=" /pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">Tłumaczenie przysięgłe orzeczenia lekarskiego lekarza specjalisty neurologii i chirurgii plastycznej z konsultacji i badań wykonanych 6 i 20 kwietnia 2018 r.</a>
> 
> (…) Zanalizowaliśmy wraz z naszym radiologiem (…) wyniki niedawno wykonanego badania rezonansu magnetycznego układu nerwowego miednicy pacjenta. Badanie to wykazało zgrubienie lewego nerwu skórnego uda bocznego w obszarze, gdzie występował objaw Tinela oraz zgrubienie lewego nerwu udowo-płciowego przy przednio-przyśrodkowej powierzchni mięśnia lędźwiowego większego, niedaleko miejsca, gdzie gałąź płciowa nerwu udowo-płciowego dochodzi do powrózka nasiennego. Dodatkowo, docent (…) wykonał badanie ultrasonograficzne o wysokiej rozdzielczości obszaru lewej pachwiny, które wykazało takie samo pogrubienie nerwu, oraz utworzenie się blizny. Odległość pomiędzy przebarwionymi punktami w obszarze lewej pachwiny a miejscem, gdzie widać wyraźną bliznę lewego nerwu skórnego bocznego uda oraz deformację o kształcie klepsydry wynosi, jak zmierzono, około 5 cm.
> 
> Biorąc pod uwagę wyniki badań oraz zakładając, że doszło do zmiany na skutek wkłucia igły w rejonie lewej pachwiny oraz wstrzyknięcia nieznanej substancji, można stwierdzić, że igłą o długości 5 cm sięgnięto do lewego nerwu skórnego bocznego uda, nerwu udowo-płciowego, oraz splotu autonomicznego unerwiającego pęcherz pacjenta. (…)
> 
> (…) Podsumowując, stwierdzono ewidentną patologie lewego nerwu skórnego bocznego uda, oraz, w mniejszym stopniu, nerwu udowo-płciowego w obszarze lewej pachwiny. Na skórze w okolicy pachwinowej występuje kilka przebarwionych punktów w odległości około 5 cm od zmiany w nerwie położonym najbliżej powierzchni. Symptomy odczuwane przez pacjenta można wytłumaczyć przy założeniu, ze został on zaatakowany z użyciem igły w celu wstrzyknięcia nieznanej substancji. (…)

 Oryginał w j. angielskim: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>

 Zdjęcia kliniczne z wizyty: 
 
![Zdjęcia kliniczne z wizyty w Millesi Center](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_1.small.JPG "Zdjęcia kliniczne z wizyty w Millesi Center")

![Zdjęcia kliniczne z wizyty w Millesi Center](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_2.small.JPG "Zdjęcia kliniczne z wizyty w Millesi Center")

![Zdjęcia kliniczne z wizyty w Millesi Center](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_3.small.JPG "Zdjęcia kliniczne z wizyty w Millesi Center")

![Zdjęcia kliniczne z wizyty w Millesi Center](/pliki/Medyczne/Zdjecia/ClinicalImage/Millesi/Clinical-image_Millesi_4.small.JPG "Zdjęcia kliniczne z wizyty w Millesi Center")

 
  Zdjęcia OCT (Optical Coherence Tomography): 
 
![Zdjęcia OCT](/pliki/Medyczne/Zdjecia/OpticalCoherenceTomography/OCT_clinical-image.png "Zdjęcia OCT")

![Zdjęcia OCT](/pliki/Medyczne/Zdjecia/OpticalCoherenceTomography/OCT_hole-area_1.png "Zdjęcia OCT")

![Zdjęcia OCT](/pliki/Medyczne/Zdjecia/OpticalCoherenceTomography/OCT_hole-area_cross-section.png "Zdjęcia OCT")


2\. <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">Tłumaczenie przysięgłe wyników badań USG ukł. nerwowego, wykonanych 6 kwietnia 2018 r.</a>
> 
> (…) USG pokazuje znaczną opuchliznę lewego nerwu skórnego bocznego uda na poziomie kolca biodrowego przedniego górnego. Obrzęk nerwu rozciąga się na długości około 15 mm. (…) Średnica przekroju poprzecznego pogrubionego nerwu trzykrotnie przekracza wymiary prawidłowe. Wyniki pokrywają się z uprzednio przeprowadzonym badaniem rezonansu magnetycznego. (…)
> 
 
 Oryginał w j. angielskim: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>

 Wybrane zdjęcia USG ukł. nerwowego: 
 
 ![Zdjęcia USG ukł. nerwowego](/pliki/Medyczne/Zdjecia/USGofNerves/USG-of-nerves_1.smaller.jpg "Zdjęcia USG ukł. nerwowego")

![Zdjęcia USG ukł. nerwowego](/pliki/Medyczne/Zdjecia/USGofNerves/USG-of-nerves_2.smaller.jpg "Zdjęcia USG ukł. nerwowego")


3\. <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">Tłumaczenie przysięgłe wyników badań rezonansu magnetycznego ukł. nerwowego, wykonanych 12 marca 2018 r.</a>
> 
> (…) Funkcjonalne obrazowanie nerwu wykazuje ograniczona dyfuzje w kształcie klepsydry w nerwie skórnym bocznym uda poprzez więzadło pachwinowe. 
> 
> (…) Wyniki rezonansu magnetycznego wskazują na uwięźnięcie nerwu skórnego bocznego uda lewego ze względu na zbliznowacenie w mięśniu naprężacza powięzi szerokiej ze zmienionym sygnałem oraz ograniczona dyfuzja (…) Występuje także pogrubienie oraz zmieniony sygnał w lewym nerwie płciowo-udowym. (…)
> 
  
Oryginał w j. angielskim: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>

Wybrane klatki z danych DICOM: 

![Rezonans magnetyczny ukł. nerwowego - wybrane klatki z danych DICOM](/pliki/Medyczne/Zdjecia/MagneticResonanceNeurography/MR-Neurography_001.smaller.png "Rezonans magnetyczny ukł. nerwowego - wybrane klatki z danych DICOM")

![Rezonans magnetyczny ukł. nerwowego - wybrane klatki z danych DICOM](/pliki/Medyczne/Zdjecia/MagneticResonanceNeurography/MR-Neurography_002.smaller.png "Rezonans magnetyczny ukł. nerwowego - wybrane klatki z danych DICOM")

![Rezonans magnetyczny ukł. nerwowego - wybrane klatki z danych DICOM](/pliki/Medyczne/Zdjecia/MagneticResonanceNeurography/MR-Neurography_003.smaller.png "Rezonans magnetyczny ukł. nerwowego - wybrane klatki z danych DICOM")


4\. <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">Tłumaczenie przysięgłe wyników badań tomografii komputerowej, wykonanych 13 lutego 2018 r.</a>
> 
> (…) Stan po urazie penetrującym miednicy z lewej strony, dyzestezje, ograniczenie funkcji neurologicznych. (…) Blizna jest widoczna także w badaniu (…) Zbliznowacenie wokół lewego przedniego górnego kolca biodrowego oraz sąsiednich części mięśnia naprężacza powięzi szerokiej. (…)
> 
 
Oryginał w j. niemieckim: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>
 
 
5\. <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">Tłumaczenie przysięgłe wyników badań USG ukł. moczowego (zdjęcia, opis), wykonanych 11 stycznia 2016 r.</a>
> 
> (...) Duże ilości zalegającego moczu: 260 ml (...) Diagnoza: Zaburzenia wzwodu, Neurogenne zaburzenia czynności pęcherza moczowego (...)
> 

Oryginał w j. angielskim: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>


6\. <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">Wyniki badań USG ukł. moczowego (zdjęcia, opis), wykonanych 29 stycznia 2014 r., zaraz po wydarzeniach</a>
> 
> (...) Pęcherz moczowy (...) Zaleganie po mikcji ok. 390 ml!!! (...)
> 
       
Tłumaczenie przysięgłe na j. angielski: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>

Przed wydarzeniami stycznia 2014 nie chorowałem na żadne ciężkie schorzenie, leczyłem się jedynie na kandydozę ukł. pokarmowego i przyjmowałem leki przepisane mi wtedy na to schorzenie przez lekarza z którym się konsultowałem (<a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.pdf</a>). 

Absurdalne uzasadnienie trwałego uszkodzenia ciała – w tym ukł. moczowo-płciowego i nerwowego oraz blizn pozostałych po ranach kłutych – zawarte w odmowie wszczęcia śledztwa z 14.04.2014 r. (<a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_target">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a>), podpisanej przez prokuratora Bartłomieja Legutko i st. post. Darie Curzydło, tj. że to kandydoza układu pokarmowego jest powodem obrażeń ciała, świadczy nie tylko o rażącym przekroczeniu uprawnień organów ścigania, które bez powołania biegłego i przeprowadzeniu obdukcji nie mają prawa wydawać diagnoz medycznych, ale kryminalnym zachowaniu mającym na celu ukrycie przestępstwa.

Oprócz wyżej wymienionej dokumentacji medycznej, sam lekarz prowadzący kuracje na kandydozę jasno stwierdził – jako lekarz posiadając kwalifikacje do wydawania takich opinii – iż  kandydoza nie może być i nie jest przyczyna zaobserwowanych obrażeń ciała i uszkodzenia ukł. moczowo-płciowego i nerwowego oraz blizn na ciele (<a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a>).
