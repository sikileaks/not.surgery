---
title: Drugi pozew o zaniedbanie medyczne złożony
layout: post
date: '2018-12-03 10:25:00'
categories: medyczne
ref: second-medical-negligence-lawsuit
lang: pl
---

Drugi pozew o zaniedbanie medyczne złożony. 

 * Wersja w j. angielskim: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf</a>
 * Wersja w j. czeskim: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf</a>
 
