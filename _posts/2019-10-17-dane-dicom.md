---
title: Dane DICOM
layout: post
date: '2019-10-17 04:40:00'
categories: medyczne
ref: dicom-data
lang: pl
image: /pliki/Medyczne/Zdjecia/MagneticResonanceNeurography/MR-Neurography_001.png
---

## Dane DICOM z badań MRI i CT, wykonanych Mahajan Imaging w Delhi, Indiach, dnia 12 Marca 2018 r.

 Pobierz z: 

 * pCloud: <a href="https://my.pcloud.com/publink/show?code=XZWdP4kZa1akm5mWcsjAFEDlDwTpJB1tf1dX" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>, lub

 * Jottacloud: <a href="https://www.jottacloud.com/s/2105c23f755f08544c58e6221888b17226f" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>, lub

 * Google Drive: <a href="https://drive.google.com/open?id=1ZJjPMAHRr4Xbiz3LdpYl2SaocqGHHPFc" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>


 MD5 checksum:

  * Pobierz: <a href="/pliki/Medyczne/Zdjecia/DICOM/Mahajan-Imaging_MRI&CT_DICOM.zip.md5" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip.md5</a>, lub

  * Skopiuj: e64a190463f0a2946cf2136690f95839

  * Następnie, zweryfikuj: np. <a href="https://www.lifewire.com/validate-md5-checksum-file-4037391" target="_blank">https://www.lifewire.com/validate-md5-checksum-file-4037391</a>


 SHA256 checksum: 

  * Pobierz: <a href="/pliki/Medyczne/Zdjecia/DICOM/Mahajan-Imaging_MRI&CT_DICOM.zip.sha256" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip.sha256</a>, lub

  * Skopiuj: add66c70beeeeb6536d6c1186b8882a0d02a2e21dbbc28588927f5818e61ba1c

  * Następnie, zweryfikuj: np. <a href="https://www.lifewire.com/validate-md5-checksum-file-4037391" target="_blank">https://www.lifewire.com/validate-md5-checksum-file-4037391</a> 


## Dane DICOM z badania CT, wykonanego w Radiology Center Vienna w Wiedniu, Austrii, dnia 13 lutego 2018 r.

 Pobierz z: 

 * pCloud: <a href="https://my.pcloud.com/publink/show?code=XZGPP4kZ3zAExfdiPcbMlkCqctU3w7nh5Kk0" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>, lub

 * Jottacloud: <a href="https://www.jottacloud.com/s/210c260e889cc9b479baa41564db371a271" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>, lub

 * Google Drive: <a href="https://drive.google.com/open?id=1HnTyAGHQHLJkkGGCzoLhZUBNjfYRAFnE" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>


 MD5 checksum:

  * Pobierz: <a href="/pliki/Medyczne/Zdjecia/DICOM/Radiology-Center-Vienna_CT_DICOM.zip.md5" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip.md5</a> 

  * Skopiuj: 4eaba114782a2c209d2263384c9b54b2

  * Następnie, zweryfikuj: np. <a href="https://www.lifewire.com/validate-md5-checksum-file-4037391" target="_blank">https://www.lifewire.com/validate-md5-checksum-file-4037391</a>


 SHA256 checksum: 

  * Pobierz: <a href="/pliki/Medyczne/Zdjecia/DICOM/Radiology-Center-Vienna_CT_DICOM.zip.sha256" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip.sha256</a> 

  * Skopiuj: 190f44f82a8f1c20f117ef6c2f48105ad428c811682f64ad72c9bafdeb904537

  * Następnie, zweryfikuj: np. <a href="https://www.lifewire.com/validate-md5-checksum-file-4037391" target="_blank">https://www.lifewire.com/validate-md5-checksum-file-4037391</a>
