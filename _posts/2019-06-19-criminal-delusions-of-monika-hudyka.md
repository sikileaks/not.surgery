---
title: Criminal delusions of Monika Hudyka
layout: post
date: '2019-06-19 20:53:00'
categories: legal
ref: hudyka-criminal-delusions
lang: en
---

Monika Hudyka–apparently a mentally ill person, who admitted in the messages we exchanged and whose authenticity was later certified by IT forensics specialists, that she is suffering from borderline personality disorder–contacted me recently. She wrote me an email–not to apologize, or clarify but–threatening to sue me for defamation if I do not remove content published on this site.

Monika, like large part of Polish society, suffers from delusions / mental illness–neither she or those following this insane person can apparently comprehend what they participated and still participate in, neither take responsibility for their actions. Instead, they continue to deny and issue treats, in this case to sue for defamation.

Poor, broken Monika and her team of criminally insane must come to grips with the fact that all of the information published on this site is 100% verifiable, comes from specialists in their fields, including world-class doctors, and start healing themselves. Your place is in prison or mental institution.

 * Copy of the email received from Monika Hudyka in April 2019: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Hudyka.20190408.pdf" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Hudyka.20190408.pdf</a>

 * Copy of notification of a crime under art. 156 §1, art. 158 §, art. 159, art. 193, art. 233 § 1, art. 234, art. 236 § 1, art. 238, art. 239 § 1, art. 258 § 1 of the Penal Code committed by M. Hudyka: <a href="/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf</a>

 * Copy of SMS messages incoming from / outgoing to Monika Hudyka's phone number, in the period from 04/03/2014 to 30/03/2014, certified by IT forensics specialists: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_GT-E1200_721030078_raport.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_GT-E1200_721030078_raport.SIGNED.pdf</a>

 * Copy of protocol of securing above mentioned SMS communication by IT forensics specialists along with invoice and acceptance protocol: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>

 * Copy of excerpt from Facebook Messenger communication with Monika Hudyka, when on 3 February 2014 she initiated contact with me: <a href="/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Screenshot_20181219-172035_Messenger.jpg" target="_blank">/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Screenshot_20181219-172035_Messenger.jpg</a>

screencasts of facebook messenger "invitation" received from hudyka on february 3 2014...:
 * <a href="/pliki/Komunikacja/20200416-234727.EZ-VideoRecorder.1.mp4" target="_blank">screencast #1</a>
 * <a href="/pliki/Komunikacja/20200416-235002.EZ-VideoRecorder.2.mp4" target="_blank">screencast #2</a>
 * <a href="/pliki/Komunikacja/2020-04-16-235248.mp4" target="_blank">screencast #3</a>

![M. Hudyka](/pliki/Prawne/Hudyka/zalaczniki/Komunikacja/Screenshot_20181219-172035_Messenger.jpg)
