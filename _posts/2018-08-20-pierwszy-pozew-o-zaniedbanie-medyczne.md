---
title: Pierwszy z pozwów o zaniedbanie medyczne złożony
layout: post
date: '2018-08-20 02:00:25'
categories: medyczne
ref: first-medical-negligence-lawsuit
lang: pl
---

Pierwszy z pozwów o zaniedbanie medyczne złożony.

 * Pozew: <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf" target="_blank">/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf</a>

 * Index: <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Index.EN.20180730.pdf" target="_blank">/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Index.EN.20180730.pdf</a>


## Załączniki 

1. Copy of sworn English translation of report from Ultrasound examination of urinary tract, conducted on January 29, 2014
 <a href="/pliki/Prawne/Apollo/USG_Ultramedica_20140129.report.EN-translation.pdf" target="_blank">/pliki/Prawne/Apollo/USG_Ultramedica_20140129.report.EN-translation.pdf</a>
 
2. Copy of report from Ultrasound examination of urinary tract, conducted on January 11, 2016
 <a href="/pliki/Prawne/Apollo/USG_St-Medica_20160111.report.original.pdf" target="_blank">/pliki/Prawne/Apollo/USG_St-Medica_20160111.report.original.pdf</a>

3. Copy of sworn English translation of report from Computed Tomography (CT) examination of pelvis, conducted on February 13, 2018, at Radiology Center in Vienna, Austria, received on March 12th 2018
 <a href="/pliki/Prawne/Apollo/CT-of-pelvis_Radiology-Center-Vienna_20180213.report.EN-translation.pdf" target="_blank">/pliki/Prawne/Apollo/CT-of-pelvis_Radiology-Center-Vienna_20180213.report.EN-translation.pdf</a>
 
4. Copy of report from Magnetic Resonance (MRI) examination including neurography and soft tissue assessment around scar area, conducted on March 5th 2018 at “The Pet Suite”/Department of Molecular Imaging & Nuclear Medicine of Apollo Hospital in New Delhi, India
 <a href="/pliki/Prawne/Apollo/Apollo_PET-MRI_report_20180306.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_PET-MRI_report_20180306.pdf</a>
 
5. Copy of report from Ultrasound examination of left lateral abdominal wall, conducted on March 8, 2018 at Mahajan Imaging in New Delhi, India
 <a href="/pliki/Prawne/Apollo/USG-of-scar_Mahajan-Imaging_20180308.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/USG-of-scar_Mahajan-Imaging_20180308.oryginal.pdf</a>
  
6. Copies of report from Magnetic Resonance (MRI) examination of nervous system, conducted on March 12, 2018 at Mahajan Imaging in New Delhi, India
 <a href="/pliki/Prawne/Apollo/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>
  
7. Copy of report from MR & CT Urography, conducted on March 12, 2018
 <a href="/pliki/Prawne/Apollo/CT&MR-Urography_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/CT&MR-Urography_Mahajan-Imaging_20180312.oryginal.pdf</a>

8. Copy of report from Ultrasound examination of nervous system, conducted on April 6, 2018
 <a href="/pliki/Prawne/Apollo/USG-of-nerves_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/USG-of-nerves_PUC-Vienna_20180413.oryginal.pdf</a>

9. Copy of report from Optical Coherence Tomography examination conducted on April 6, 2018
 <a href="/pliki/Prawne/Apollo/OCT-of-scar_Vienna_20180406.original.pdf" target="_blank">/pliki/Prawne/Apollo/OCT-of-scar_Vienna_20180406.original.pdf</a>

10. Copy of report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018
 <a href="/pliki/Prawne/Apollo/Neurologist&Plastic-Surgeon_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Prawne/Apollo/Neurologist&Plastic-Surgeon_Millesi-Center-Vienna_20180420.oryginal.pdf</a>

11. Copy of confirmations of booking of two diagnostic imaging examinations at “The Pet Suite”/Department of Molecular Imaging & Nuclear Medicine of Apollo Hospital in New Delhi, India from February 15th 2018, prior to coming to India
 <a href="/pliki/Prawne/Apollo/Apollo_booking-of-examinations_20180215.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_booking-of-examinations_20180215.pdf</a>
 
12. Copy of referrals for diagnostic imaging examinations with brief description of symptoms, issued by Dr. Andrzej Gliwa
 <a href="/pliki/Prawne/Apollo/CMIntermed_referrals_20180220.pdf" target="_blank">/pliki/Prawne/Apollo/CMIntermed_referrals_20180220.pdf</a>

13. Copy of email exchange between Complainant and Opposite Party No. 4 from February 9th 2018, February 13th 2018 and February 15th 2018, prior to coming to India
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180209-15.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180209-15.pdf</a>

14. Copy of email exchange between Complainant and Opposite Party No. 2 from March 2nd 2018
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180302.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180302.pdf</a> 

15. Copy of email exchange between Complainant and Opposite Party No. 4 from March 3rd 2018
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180303.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180303.pdf</a>

16. Copy of email exchange between Complainant and Opposite Party No. 4 from March 5th 2018, regarding confirmation of PET/CT examination schedule
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180305.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180305.pdf</a>

17. Copy of email exchange between Complainant and Opposite Party No. 4, between March 6th 2018 and March 7th 2018, regarding refund for cancelled PET/CT examination
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180307.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180307.pdf</a>

18. Copy of email exchange between Complainant and translator, between March 10th 2018 and March 13th 2018
 <a href="/pliki/Prawne/Apollo/Modlingua_email-communication_20180310-13.pdf" target="_blank">/pliki/Prawne/Apollo/Modlingua_email-communication_20180310-13.pdf</a>

19. Copy of email exchange between Complainant and Opposite Party No. 4 from June 20th 2018, asking regarding reply to legal notice
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180620.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180620.pdf</a>

20. Copy of email exchange between Complainant and Opposite Party No. 4 from June 25th 2018, asking again regarding reply to legal notice
 <a href="/pliki/Prawne/Apollo/Apollo_email_20180625.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_email_20180625.pdf</a>
 
21. Copy of WhatsApp exchange between Complainant and Opposite Party No. 4 from June 25th 2018, asking again regarding reply to legal notice
 <a href="/pliki/Prawne/Apollo/Apollo_whatsapp_20180625.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_whatsapp_20180625.pdf</a>
 
22. Copy of Legal Notice served to Opposite Parties on April 5th 2018, well over 4 months before filing lawsuit
 <a href="/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018.pdf</a>

23. Copy of proof of posting of legal notice to Opposite Parties on April 4, 2018
 <a href="/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-posting.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-posting.pdf</a>

24. Copy of proof of delivery of legal notice to Opposite Parties on April 5, 2018
 <a href="/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-delivery.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_Legal-Notice_April-2018_Proof-of-delivery.pdf</a>
 
25. Copy of the payment receipt for PET/MRI examination conducted on March 5th 2018 at “The Pet Suite”/Department of Molecular Imaging & Nuclear Medicine of Apollo Hospital in New Delhi, India
 <a href="/pliki/Prawne/Apollo/Apollo_PET-MRI_invoice_20180305.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_PET-MRI_invoice_20180305.pdf</a>
 
26. Copy of payment receipt for PET/CT examination to be conducted on March 6th 2018 at “The Pet Suite”/Department of Molecular Imaging & Nuclear Medicine of Apollo Hospital in New Delhi, India, cancelled same day by Apollo staff
 <a href="/pliki/Prawne/Apollo/Apollo_PET-CT_invoice_20180306.refund.pdf" target="_blank">/pliki/Prawne/Apollo/Apollo_PET-CT_invoice_20180306.refund.pdf</a>
 
27. Copy of payment receipt for sworn translation service from March 9th 2018 for the amount of 4000 INR
 <a href="/pliki/Prawne/Apollo/Modlingua_bill_20180309.pdf" target="_blank">/pliki/Prawne/Apollo/Modlingua_bill_20180309.pdf</a>
 
28. Copy of payment receipt for examinations conducted on March 12, 2018 at Mahajan Imaging in New Delhi, India
 <a href="/pliki/Prawne/Apollo/Mahajan-Imaging_MRI&CT_invoice_20180312.pdf" target="_blank">/pliki/Prawne/Apollo/Mahajan-Imaging_MRI&CT_invoice_20180312.pdf</a>


***AKTUALIZACJA: Ten pozew jest już rozstrzygnięty. Kliknij po więcej informacji: <a href="/medyczne/2019/01/03/pierwszy-pozew-o-zaniedbanie-medyczne-rozstrzygniety.html" target="_blank">/medyczne/2019/01/03/pierwszy-pozew-o-zaniedbanie-medyczne-rozstrzygniety.html</a>***
