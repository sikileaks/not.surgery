---
title: Wywiad wideo z sierpnia 2013 r.
layout: post
date: '2019-07-23 18:27:00'
categories: inne
ref: video-interview-from-august-2013
lang: pl
---

Wywiad wideo zarejestrowany w sierpniu 2013 r., około **5 miesięcy przed** wydarzeniami stycznia 2014 r.

Było to w czasie, gdy przez prawie 3 lata byłem systematycznie tłumiony, do tego stopnia, że nie byłem w stanie zarobić na życie. Poważne niedożywienie i bezdomność to tylko niektore z konsekwencji tego co już wtedy miało miejsce.

Proszę zauważyć, że tytuł "wstrzyknięcie cukrzycy" nie ma nic wspólnego z rzeczywistością. Tytuł ten został nadany, z nieznanych mi przyczyn, przez osobę przeprowadzającą ze mną wywiad, Pana M. Podleckiego, z którym od tego czasu nie miałem kontaktu.

<div class="videoWrapper">
  <iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/hgcjM0WPbTWR/"></iframe>
</div>

<div class="videoExtras">
  <strong>Pobierz</strong>: <a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.mp4" | relative_path }}" target="_blank">wersja MP4</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.PL.srt" | relative_path }}" target="_blank">napisy (SRT)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.PL.vtt" | relative_path }}" target="_blank">napisy (VTT)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Wywiad-2013/MHSiemaszko_wywiad-2013.transcript.PL.pdf" | relative_path }}" target="_blank">transkrypt (PDF)</a>
</div>
