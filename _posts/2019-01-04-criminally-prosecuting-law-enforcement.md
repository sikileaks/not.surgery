---
title: Criminal prosecution of public prosecutors and police concealing crime of medical procedure without consent
layout: post
date: '2019-01-04 00:35:10'
categories: legal
ref: criminally-prosecuting-law-enforcement
lang: en
---

Notifications of a crime against public prosecutors and police concealing crime of medical procedure without consent:

 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf</a>
 * <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf</a>
 * <a href="/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf" target="_blank">/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf</a>


To be assessed by Court: 
 * <a href="/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf</a>

For more information, please refer to the original publication <a href="/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html" target="_blank">/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html</a> (in polish language only at this time, but automatic translation should be sufficient).
 
