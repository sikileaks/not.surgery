1 
00:00:00,000 --> 00:00:03,583 
>> MARK PODLECKI: Good morning, I am speaking with Michael Siemaszko.

2 
00:00:04,341 --> 00:00:12,934 
If you do not remember who is Michael, I advise you to watch the old materials that we recorded in 2009 with Peter Bein from Canada.

3 
00:00:13,324 --> 00:00:22,028 
Michael, we met on the occasion of the of anti-vaccine campaing in 2009, you were the one who run the group on Facebook...

4 
00:00:22,148 --> 00:00:27,874 
... you were the person, who was attempting to register association "Civilization of Life". 

5 
00:00:27,994 --> 00:00:35,640 
>> MICHAEL SIEMASZKO: We worked together in 2009 on the occasion of campaign whose aim was to stop forced vaccinations.

6 
00:00:35,677 --> 00:00:40,704 
I run i.e. a group on Facebook at that time, as well as an internet website, "Świńska Sprawa" ("Swine Case").

7 
00:00:41,282 --> 00:00:44,709 
>> MARK: You were i.e. the organizer of this big meeting at the Groteska Theatre.

8 
00:00:44,775 --> 00:00:48,963 
>> MICHAEL: Yes, we co-organized a meeting with Mrs. Jane Burgermeister at the Groteska Theatre.

9 
00:00:49,180 --> 00:00:53,208 
... we also co-organized a meeting with Peter Bein in Krakow.

10 
00:00:53,544 --> 00:00:59,622 
I helped Mr. Peter, Mr. Mark. We conducted leaflet, poster, internet campaigns.

11 
00:00:59,658 --> 00:01:06,120 
I also wanted to register an association, in order to move these matters forward and so that this is not a one-time act...

12 
00:01:06,240 --> 00:01:22,103 
... but to be able to act in diverse topics, because it was not only about vaccinations and it still isn't, but various other methods of systematic depopulation of which people are unaware. 

13 
00:01:22,448 --> 00:01:26,917 
My association, "Civilization of Life", was sabotaged...

14 
00:01:27,728 --> 00:01:30,803 
... it was not even registered, because I had supposititious people.

15 
00:01:31,090 --> 00:01:36,858 
But all of this started a little earlier, after the death of my daughter in year 2007.

16 
00:01:36,978 --> 00:01:38,658 
>> MARK: Yes, I did not know whether you want to talk about this...

17 
00:01:38,778 --> 00:01:44,873 
... I knew about this death. I did not know these details that I know now, which to me are really shocking. 

18 
00:01:44,993 --> 00:01:48,669 
Ladies and gentlemen, what we are saying is only a part of what we can release. 

19 
00:01:48,942 --> 00:01:52,596 
A lot of things we cannot release, for a number of reasons - you would simply not believe in these things...

20 
00:01:52,793 --> 00:01:59,656 
... Daughter of Michael and Mary, in the eighth month of pregnancy, was murdered.

21 
00:02:01,946 --> 00:02:06,455 
>> MICHAEL: Because it was my first child, through all of the first months of pregnancy we would go for USG...

22 
00:02:06,493 --> 00:02:10,720 
... systematic examination was being done, once every two weeks, at least once a month. 

23 
00:02:11,777 --> 00:02:18,767 
In the sixth month of pregnancy, some issues related to her heart came out. We deliberated traveled to Warsaw to a specialist.

24 
00:02:19,153 --> 00:02:23,634 
It has been verified and he was not particularly concerned about the situation.

25 
00:02:25,347 --> 00:02:32,007 
Tola's mother was taken to maternity hospital, where she spent a month's time. 

26 
00:02:32,266 --> 00:02:37,536 
Myself, witnessing her birth, I also took part in all of this from the very beginning.

27 
00:02:37,783 --> 00:02:44,230 
Immediately after delivery, Tola was taken to Children's Hospital in Prokocim and died after 2 weeks. 

28 
00:02:45,149 --> 00:02:48,379 
She had multiple surgeries, i.e. a surgery of the intestine, heart surgery.

29 
00:02:49,168 --> 00:02:56,797 
Unfortunately, only after 3 years, after I split up with Ms. Maria, various details regarding the genesis of this began to come out. 

30 
00:02:56,917 --> 00:03:02,751 
It turned out that the child has been deliberately killed during pregnancy through administration of pharmacological agents.

31 
00:03:02,871 --> 00:03:05,778 
This was not an abortion, because abortion is permitted only up to the 3 month of pregnancy. 

32 
00:03:05,813 --> 00:03:10,248 
>> MARK: (Children's Hospital in Prokocim) is an American hospital, from which many children with autism came out. I know personally children with autism. 

33 
00:03:10,273 --> 00:03:13,341 
>> MICHAEL: I do not know whether the hospital took part in any of this. I do not know, I can not confirm.

34 
00:03:13,782 --> 00:03:23,921 
But this took place in the 6th or 7th month of pregnancy when pharmacological agents, which caused the defects with which the child was born, where given to her or she took them herself. 

35 
00:03:24,203 --> 00:03:27,458 
However, the hospital itself - I can not say a lot of good things about this hospital, neither.

36 
00:03:27,740 --> 00:03:34,226 
In any case, all of the documents related to my child were taken at the time when I split with Mrs. Maria. 

37 
00:03:34,627 --> 00:03:37,607 
All that remained was the death certificate and the birth certificate of my child.

38 
00:03:38,977 --> 00:03:48,121 
What's more, it turned out that my closest relatives were also involved, i.e. with what happened with Tola, as well as in the theft of several hundred thousand PLN. 

39 
00:03:48,150 --> 00:03:51,550 
>> MARK: Well, I remember you very wealthy. You had very cool bikes...

40 
00:03:51,869 --> 00:03:56,925 
>> MARK: ... you were coming (to our meetings) with latest model of laptop, Macintosh if I'm not mistaken.

41 
00:03:57,045 --> 00:04:01,026 
You did not have financial problems. You looked better.

42 
00:04:01,146 --> 00:04:06,163 
And now, I must admit to you honestly, Michael came to me because he had nothing to eat.

43 
00:04:06,283 --> 00:04:13,912 
>> MICHAEL: I came to Mr. Mark, because after 3 years from 2010, when I was doing what I could... 

44 
00:04:14,032 --> 00:04:20,963 
... systematically being banned and, in fact, persecuted.

45 
00:04:21,272 --> 00:04:31,320 
Perhaps let's go back to year 2010. I was told by at least 2-3 people, with whom I wanted to sort out all of these issues sensibly after splitting with Mrs. Maria,...

46 
00:04:31,440 --> 00:04:36,520 
...that because of the anti-vaccine campaign, that I should stop dealing with these issues, otherwise I will have problems. 

47 
00:04:36,949 --> 00:04:44,799 
I am not going to, was not going to and will not stop dealing with these issues, for as long as these issues exist and until such things are taking place. 

48 
00:04:44,859 --> 00:04:54,254 
So, for my part, nothing will change. For as long as I live, I will not stop dealing with matters that affect me and potentially the children, which I would like to have.

49 
00:04:54,348 --> 00:05:00,173 
>> MARK: Exactly, wasn't it your child, killed, wasn't one of the reasons why you started the anti-vaccine campaign?

50 
00:05:00,217 --> 00:05:10,666 
>> MICHAEL: Of course it was, but the anti-vaccine campaign was just one of the issues. I wanted to continue this activity under the aegis of the association, which was sabotaged. 

51 
00:05:10,911 --> 00:05:19,051 
Because there are a lot of these topics. I.e. it's about GMO food after all, about geoengineering, it's about forced vaccinations. 

52 
00:05:19,171 --> 00:05:22,542 
>> MARK: I have to admit that Michael is very familiar with these topics. These will be next talks.

53 
00:05:22,662 --> 00:05:26,463 
>> MARK: For now, let's focus on what's happening with you Michael, 

54 
00:05:26,547 --> 00:05:30,053 
In your group on Facebook, there were about one thousand people, if not more, right? 

55 
00:05:30,173 --> 00:05:35,021 
>> MICHAEL: In the group, yes, there were more than a thousand people, while the site was visited by tens of thousands of people.

56 
00:05:35,050 --> 00:05:39,866 
However, that what has taken place since that time, perhaps I'll mention:  

57 
00:05:40,361 --> 00:05:46,696 
On at least 5 or 6 occasions, when I was renting an apartment or room in various places in Poland...

58 
00:05:46,816 --> 00:05:53,072 
... at least several times there was an entry to the place where I lived during my absence or while I was sleeping.

59 
00:05:53,346 --> 00:05:54,809 
>> MARK: ... for which you have evidence?

60 
00:05:54,929 --> 00:06:00,929 
>> MICHAEL: I have pictures of some of these situations. Besides, I know what the effect was on my body.

61 
00:06:00,982 --> 00:06:05,849 
I have been vaccinated at least twice, without my permission, of course.

62 
00:06:06,132 --> 00:06:09,594 
And in all of this, Mark, the most important thing is to talk about this.

63 
00:06:09,651 --> 00:06:14,006 
I am doing what I can do, but the idea is to talk about this...

64 
00:06:14,041 --> 00:06:19,925 
... because the people who are doing these things, rely on the fact that this is not visible, that they can do anything. 

65 
00:06:20,045 --> 00:06:24,441 
... while you have no means, in any way, to strike back...

66 
00:06:24,561 --> 00:06:28,937 
... because, well, quite simply for the last 3 or 4 years I could not afford to hire an attorney...

67 
00:06:28,984 --> 00:06:34,830 
... or to draw up the appropriate legal expertises to begin enforcing what is mine.

68 
00:06:34,950 --> 00:06:41,323 
So most important is to talk about this and, in the meantime, of course, yes, I would like 'get back on my feet' financially. 

69 
00:06:41,698 --> 00:06:49,893 
Despite the fact that I'm doing what I can, I had at least four or five supposititious projects - these are further losses in the tens of thousands of zlotys (PLN)...

70 
00:06:49,936 --> 00:06:56,600 
... where, in spite of performing my duties, communication with my superior, I was suddenly being dismissed from the project.

71 
00:06:56,831 --> 00:07:01,212 
Well, I don't konw, these people think that when I start working at some corporation...

72 
00:07:01,332 --> 00:07:08,594 
... suddenly, these topics, like compulsory vaccinations, geoengineering, or genetically modified food, or many other topics...

73 
00:07:08,981 --> 00:07:13,471 
... I will say that this is in any way acceptable, or what has taken place before is acceptable.

74 
00:07:13,591 --> 00:07:15,255 
It is not and will not be acceptable.

75 
00:07:15,281 --> 00:07:19,224 
So, when I cooperate with someone, performing my duties... 

76 
00:07:19,344 --> 00:07:29,939 
... my obligations towards that person are only such as per agreement. I have no obligation in any way, ideologically or in any other way, to subordinate myself to that person. So, that's how this looks. 

77 
00:07:30,422 --> 00:07:32,635 
But, most importantly, is to talk about this, right?

78 
00:07:33,495 --> 00:07:38,916 
>> MARK: I see that you have prepared some materials here on your laptop, which must be powered from the network, because the battery is not working.  

79 
00:07:39,172 --> 00:07:43,631 
>> MICHAEL: Well, the laptop is in such condition as it is. However, money is acquired, right?

80 
00:07:44,907 --> 00:07:51,533 
I was on my own from the nineteenth year of my life, so just for these past three years it is the first such period, 

81 
00:07:51,653 --> 00:07:56,005 
when - and only by the fact that this is a deliberate action - 

82 
00:07:56,452 --> 00:08:00,366 
this is stalking, this is gang stalking, this is systematic. 

83 
00:08:00,486 --> 00:08:07,069 
>> MARK: I'll interrupt you for a moment, I apologize. For those of you who do not believe that something like this could happen, I will give an example of Jane Burgermeister. 

84 
00:08:07,189 --> 00:08:09,261 
(This) woman is hiding at the moment. 

85 
00:08:09,392 --> 00:08:12,580 
She moved to Berlin. We do not know in which country she is at this point.

86 
00:08:13,024 --> 00:08:16,364 
I asked a clairvoyant. She said that "somewhere in Europe".

87 
00:08:16,852 --> 00:08:22,951 
Jane Burgermeister was attacked, and a lot of times, through means of which Michael is speaking of.

88 
00:08:23,113 --> 00:08:28,085 
This is a very strong person. Michael is also a very strong person. I suspect that's why these people are alive.

89 
00:08:28,513 --> 00:08:35,200 
Michael, are you comparing your situation with what is going on with Jane Burgermeister, about whom it started to be quiet? This is a dangerous situation.

90 
00:08:35,320 --> 00:08:42,948 
>> MICHAEL: Mark, maybe mention that Jane was also being dismissed from work suddenly. Her father was killed.

91 
00:08:43,439 --> 00:08:47,311 
She had various problems. Her closest friends suddenly turned away from her.

92 
00:08:47,431 --> 00:08:56,725 
Simply put, so to speak, deliberate actions aimed at silencing, muting and discrediting given person.

93 
00:08:56,845 --> 00:09:00,557 
>> MARK: And discredit her by the way, because it was also being said that she does not exist. 

94 
00:09:00,755 --> 00:09:07,396 
>> MICHAEL: Exactly. And what's more, I was repeatedly threatened, like "We will erase you", 

95 
00:09:07,613 --> 00:09:12,087 
"You are being hunted" as well as other punishable threats. 

96 
00:09:12,623 --> 00:09:16,000 
So called "Hunger games", which has been confirmed repeatedly.
 
97 
00:09:16,661 --> 00:09:22,855 
>> MARK: I will interrupt again, Michael, you speak English very well. You switch to English, but why. 

98 
00:09:22,975 --> 00:09:29,960 
Because this whole thing, it is not only about Poland, because the forces with which we play are these illuminati forces.

99 
00:09:30,459 --> 00:09:33,066 
These are international usurious connections.

100 
00:09:33,186 --> 00:09:34,108 
>> MICHAEL: Of course.

101 
00:09:34,179 --> 00:09:38,537 
>> MARK: And that's why Michael is such a dangerous person, because you know English very well.

102 
00:09:38,657 --> 00:09:43,329 
>> MICHAEL: Well, I do know English. Just for the past 20 years I'm learning and using English language.

103 
00:09:43,714 --> 00:09:50,549 
It is also i.e. one of the things that I can still offer - i.e. translations - but we'll talk about this later.

104 
00:09:50,580 --> 00:09:56,197 
Besides, information with links will be attached to this (video) material, where my ads are posted, 

105 
00:09:56,224 --> 00:10:04,481 
I'm not asking for money by no means, I'm just asking for (work) orders and that is all, because I'm not disabled so that I could not work. 

106 
00:10:04,601 --> 00:10:12,121 
The point is that so far I have not contacted Mark, because I did not know that on so many fronts this is organized so perfidiously, 

107 
00:10:12,162 --> 00:10:18,548 
Where, despite my best efforts, best intentions, I am constantly being flunked and blocked.

108 
00:10:18,807 --> 00:10:25,000 
So, at the end of August (year 2013), a few weeks ago I contacted Mark, because I simply did not know what to do anymore. 

109 
00:10:25,394 --> 00:10:29,997 
When another project it turned out that suddenly I do not get an order, where I offered bids.

110 
00:10:30,381 --> 00:10:35,171 
I did what I could. Again, for example, I cannot get through (on the phone). My emails are not arriving.

111 
00:10:35,185 --> 00:10:41,364 
>> MARK: Not only are not arriving, but are being exploited, because I know that someone is impersonating you, to give you problems, put simply. 

112 
00:10:41,484 --> 00:10:42,595 
>> MICHAEL: That also, that as well.

113 
00:10:42,767 --> 00:10:46,317 
So, perhaps we'll continue. "Hunger games" or food games. 

114 
00:10:46,333 --> 00:10:52,162 
The idea is that, since these actions were systematic - stalking, blocking earning opportunities, sudden dismissals, 

115 
00:10:52,282 --> 00:10:56,256 
robbing, many many others. Possibilities of a normal communication (being blocked).

116 
00:10:56,304 --> 00:11:02,919 
Games which are taking place for the last several years are intentional and systematic. The result of which many times, even very often, I had nothing to eat. 

117 
00:11:03,039 --> 00:11:07,231 
I barely lived on bread with butter and water. And even with (obtaining) this sometimes there was a problem. 

118 
00:11:07,453 --> 00:11:16,381 
It was summer now so I had episodes where, thanks to the fact that I found a field with blackberries, I could live on blackberries for two weeks. 

119 
00:11:18,409 --> 00:11:19,897 
>> MARK: Yes, because you're a vegetarian.

120 
00:11:19,950 --> 00:11:24,417 
>> MICHAEL: I'm a vegetarian, yes, but over the winter, autumn, well, it is hard to find such places, right? 

121 
00:11:25,907 --> 00:11:31,390 
On the whole, these are frequent violations of human rights. I felt like a slave. I still feel like a slave. 

122 
00:11:31,405 --> 00:11:41,315 
As if, to say the least, my life belonged to someone else. I do not know whether to the State - I'm talking about the government - or whoever else is involved in this. 

123 
00:11:41,809 --> 00:11:47,541 
My life belongs to me. I have the right to decide about my life. I have the right to decide with whom and where I want to live.

124 
00:11:47,921 --> 00:11:54,890 
If I'm putting my effort into something, I want to see the results of this work and not that my work is constantly being hijacked by someone else. 

125 
00:11:55,181 --> 00:12:00,969 
That my money is being stolen from me. That my health is being destroyed.  Where I have no possibility, in any way, to "get back on my feet", 

126 
00:12:01,261 --> 00:12:06,614 
to hire a lawyer or to draw up appropriate legal expertises, because all the time I'm financially, so to speak, in a hole. 

127 
00:12:07,225 --> 00:12:09,618 
The whole time I'm in the so-called "survival mode". 

128 
00:12:09,738 --> 00:12:11,700 
So, perhaps we should move on to the next point.

129 
00:12:11,820 --> 00:12:16,048 
Twice, I was arrested and it was a litmus test of sorts, a bit.

130 
00:12:16,603 --> 00:12:21,376 
It happened to me a few times that I was stealing food from the store, because I needed to eat. 

131 
00:12:21,397 --> 00:12:27,297 
It wasn't summer and I realized that if such things are taking place, why should I have nothing to eat? 

132 
00:12:27,601 --> 00:12:29,959 
So, a few times it happened that, indeed, I stole food from the store. 

133 
00:12:29,973 --> 00:12:37,369 
>> MARK: Ladies and gentlemen, IT specialist, perfectly familiar with English, who could earn 60,000 pounds (GBP) a year.

134 
00:12:37,489 --> 00:12:41,224 
>> MICHAEL: I earned 10,000 PLN in 2010 (per month), per invoice. Such invoices I was issuing.

135 
00:12:41,258 --> 00:12:46,630 
I receive proposals from England for 60,000 pounds (per year), but I can not afford, for instance, to go there. 

136 
00:12:46,715 --> 00:12:48,402 
>> MARK: Relocation?
>> MICHAEL: The relocation I cannot afford.

137 
00:12:48,424 --> 00:12:51,053 
>> MICHAEL: I cannot afford to rent a flat or to pay for food.

138 
00:12:51,063 --> 00:12:53,251 
>> MARK: Besides, you do not know where you'll end up also.

139 
00:12:53,298 --> 00:12:57,806 
>> MICHAEL: Besides, I do not know whether this will not be another supposititious project, because this already happened at least five or six times.

140 
00:12:58,073 --> 00:13:04,675 
I'm not sure who is inviting me, so without financial collateral, I will not possibly borrow that money. 

141 
00:13:04,847 --> 00:13:09,307 
>> MARK: I think that you are doing very well, that you are trying to protect yourself, because we know who we're dealing with.
>> MICHAEL: I have to protect myself.

142 
00:13:09,382 --> 00:13:12,307 
>> MARK: You've been vaccinated. Tell me about this, because this is something terrifying.

143 
00:13:12,323 --> 00:13:19,214 
>> MICHAEL: Yes, at least twice during sleep, there was a walk in into the place where I lived - renting a flat or a room - and I was vaccinated. 

144 
00:13:19,510 --> 00:13:23,926 
This showed itself in such a way that for two - three weeks I was sweating, 

145 
00:13:24,046 --> 00:13:29,837 
I had breathing problems. Later, I had cognitive problems. 

146 
00:13:29,877 --> 00:13:32,682 
In vaccines, there are i.e. heavy metals. 

147 
00:13:33,286 --> 00:13:39,326 
>> MARK: They have a variety of ways. I wrote about how I was attacked by - I suspected - infrasonic waves. 

148 
00:13:39,811 --> 00:13:43,740 
I found a crew that looked like a Mossad crew in my apartment.

149 
00:13:44,111 --> 00:13:47,024 
And, supposedly they were checking voltage, whether it is in accordance with a European standard...

150 
00:13:47,241 --> 00:13:49,684 
...something that the power company laughed at - there was nothing like that. 

151 
00:13:49,901 --> 00:13:52,221 
I believe you Michael - (they) attempted to eliminate me. 

152 
00:13:52,265 --> 00:13:57,000 
>> MICHAEL: You know Mark, I believe you, you believe me, but these are things that I confirmed with at least 5 - 6 individual people.

153 
00:13:57,058 --> 00:14:02,031 
Since year 2010 I've been to meetings with many people, where I came with a list and I talked to these people... 

154 
00:14:02,151 --> 00:14:06,905 
...and these people in turn were confirming me these things. These are not things which Mark or I myself have dreamed up. 

155 
00:14:07,230 --> 00:14:11,027 
Mark is also in contact with other people with whom he talks and these people are confirming this...

156 
00:14:11,060 --> 00:14:16,360 
...so, this is not one - two persons, these are people who independently are confirming the same information.

157 
00:14:16,480 --> 00:14:21,856 
I was told already 3 or 4 years ago by some people with whom I am still in touch...

158 
00:14:21,976 --> 00:14:27,370 
...things that I had to arrive at myself for the last 3 years, because they were so unbelievable to me... 

159 
00:14:27,490 --> 00:14:34,432 
...only by experience, through the extraction of material from the subconscious, only then it started to occur to me, because it concerned my closest relatives. 

160 
00:14:34,552 --> 00:14:40,922 
So, perhaps more about the arrests. The first time, when I mentioned one of the places where I lived...

161 
00:14:41,195 --> 00:14:50,517 
...where my health was quite strongly impaired - that is, on the Kostrzewskiego street in Krakow. 

162 
00:14:50,851 --> 00:14:57,202 
When I mentioned about Kostrzewskiego street, I was quickly released, although prior to that I was threatened with a night in custody.

163 
00:14:57,322 --> 00:15:04,735 
The second time however, when I mentioned someone else, where I had a supposititious project, in Brzozow, in the subcarpathian voivodeship...

164 
00:15:05,053 --> 00:15:12,662 
...this person, all of a sudden, even though I was performing my duties, began to threaten me with being beat up and told me to get out of the office...

165 
00:15:12,782 --> 00:15:16,624 
...of course, I went to a Police station and reported a criminal offence. 

166 
00:15:16,744 --> 00:15:22,248 
When I mentioned this, Policemen suddenly released me, giving me the lowest possible mandate. 

167 
00:15:22,368 --> 00:15:26,893 
>> MARK: Twenty zlotys (PLN). 
>> MICHAEL: Twenty zlotys, yes. Despite the fact that they also threatened me that I would be put into custody.

168 
00:15:27,013 --> 00:15:35,837 
>> MICHAEL: So, this says that it is probably not organized crime, but either organized crime cooperating with law enforcement authorities that has/had a part in this...

169 
00:15:35,957 --> 00:15:43,632 
...and we all know how the illuminati work, right - Police is really only following orders...

170 
00:15:43,690 --> 00:15:50,429 
...they are obliged to follow orders. Fortunately, not all cops are corrupt, some of them can think.

171 
00:15:50,476 --> 00:15:52,346 
>> MARK: Most of them are not (corrupt).

172 
00:15:52,466 --> 00:15:55,021 
>> MICHAEL: Unfortunately, some have no idea at all about what they're doing...

173 
00:15:55,303 --> 00:16:01,248 
...and that they are carrying out orders of people who, despite the fact that they are in high positions, are criminals and psychopaths.

174 
00:16:03,219 --> 00:16:11,163 
>> MARK: About what I have written many times.
>> MICHAEL: About what Mark has written many times and what was confirmed by their actions over the last few years. So, that's how this looks more or less in a nutshell.

175 
00:16:11,283 --> 00:16:16,725 
>> MICHAEL: Mark, question (me).
>> MARK: Now perhaps let's move on to the key point - how one can help you? 

176 
00:16:17,802 --> 00:16:21,349 
>> MICHAEL: As I'm saying, I'm not a disabled person. 

177 
00:16:21,375 --> 00:16:30,153 
I simply need, as soon as possible, to collect adequate resources to, for example, detox myself from accumulated toxins, the result of the vaccinations.

178 
00:16:31,226 --> 00:16:33,023 
>> MARK: You want to detox your body.
>> MICHAEL: I want to detox my body.

179 
00:16:33,064 --> 00:16:36,333 
>> MICHAEL: I.e. because of what took place, I have diabetes.

180 
00:16:36,453 --> 00:16:41,045 
Immunizations cause type I diabetes. I have that because of what took place. 
>> MARK: And you did not have (that).

181 
00:16:41,088 --> 00:16:47,076 
>> MICHAEL: By no means did I have (that) before. Because of intrusion into my body I have type I diabetes. 

182 
00:16:47,515 --> 00:16:53,386 
Four months ago I was ordered (by doctor) more than 15 blood tests, unfortunately I could not afford to do these tests. And I also have i.e. candidiasis.

183 
00:16:53,770 --> 00:17:01,469 
And of course to get out of the "survival mode". I barely have enough for rent and food, so I simply need to have standing orders (for my services). 

184 
00:17:01,589 --> 00:17:03,341 
>> MARK: So you can survive.
>> MICHAEL: So I can survive, yes.

185 
00:17:03,362 --> 00:17:09,872 
>> MARK: I'll just interrupt Michael for a moment. A few days ago, I've was at clairvoyant's - more, at exorcist's.

186 
00:17:09,992 --> 00:17:18,987 
She does not even need to see a photo of a given person, in order to track down that person she needs a mental image. And she visualized Michael. 

187 
00:17:19,107 --> 00:17:26,349 
And I asked this person, this exorcist, about this topic. She said clearly: Mr. Michael needs a clearing. 

188 
00:17:26,796 --> 00:17:31,742 
I don't know what else is in him - whether he is chipped or something else - but this would be a confirmation of what you are talking about. 

189 
00:17:31,862 --> 00:17:37,987 
>> MICHAEL: I.e., I'm clearing myself - we can proceed to that in a moment. I'm clearing by energetic body regularly, almost every day. 

190 
00:17:38,063 --> 00:17:40,227 
>> MARK: But you're not sure whether by any chance you were not chipped.

191 
00:17:41,382 --> 00:17:50,465 
>> MICHAEL: With regards to that, then absolutely. So, it's more in this direction. By no means do I have anything to do with Satanism or with such energies, quite the contrary. 

192 
00:17:50,812 --> 00:17:56,768 
On the other hand, how you can help me - there will surely be a note included with this material. I offer i.e. services... 

193 
00:17:56,804 --> 00:18:00,154 
>> MARK: Indeed, tell us. Perhaps let's switch the language - Let's switch to English, OK?

194 
00:18:00,274 --> 00:18:03,245 
You can present what you can say in English, OK? 

195 
00:18:03,365 --> 00:18:07,662 
Let's talk about your translations - what you can do in English. 

196 
00:18:07,782 --> 00:18:15,871 
>> MICHAEL: Well, I offer translations. Since I use and learn and practice English language for over 20 years... 

197 
00:18:15,897 --> 00:18:17,413 
>> MARK: Have you ever been to USA? 

198 
00:18:17,533 --> 00:18:22,722 
>> MICHAEL: Yes, I've been in and out of the USA since I was about 8 years old. 

199 
00:18:23,796 --> 00:18:30,538 
I lived there between 1996 and 2003, continuosly for 7 years. 

200 
00:18:30,971 --> 00:18:37,566 
I've finished my high school in the United States, I've studied a little bit...
>> MARK: You are well educated in English.

201 
00:18:37,800 --> 00:18:51,000 
>> MICHAEL: ...but since my father died in 1999, since '98 I was on my own and I worked in information technology, as a software developer, as many different roles. 

202 
00:18:51,200 --> 00:18:56,000 
>> MARK: Let's switch back to Polish. 
Many people do not understand what you're talking about. Tell us next about your IT education. 

203 
00:18:56,100 --> 00:19:17,000 
>> MICHAEL: My education - in quotes, because I was learning by doing, by experience, because I could not afford to pay for college, whereas since '98 I've been working in IT, I participated in over 30 projects, most of them were successfully completed.

204 
00:19:17,100 --> 00:19:31,000 
For seven years, I lived and worked in the United States, where I finished high school, then from year '98 I've been working in IT. In 2003, I went back to Poland, so I've been in Poland for over 10 years. 

205 
00:19:31,100 --> 00:19:38,000 
So, what I can offer is i.e. English tutoring, translations from/to English language, proofreading.
>> MARK: You speak of IT translations? 

206 
00:19:38,100 --> 00:19:49,500 
>> MICHAEL: Well, of course, specialized translations, by all means - specialized and general translations. And also, of course, anything that has to do with information technology, that is, web site development, software...

207 
00:19:49,750 --> 00:20:10,000 
>> MARK: Online store? 
>> MICHAEL: ... online stores, CMSes, database applications, consulting - IT consulting, Internet marketing - virtually anything related to computers or the Internet. Thanks to my over 10 years of IT experience, I worked in most industries - more than 30 projects were carried out, as I have already mentioned. 

208 
00:20:10,100 --> 00:20:21,000 
But, for the last 6 years, I'm also dealing with other topics - in addition to English language, in addition to IT, I'm also working with energy medicine / so called alternative (medicine) as well as wellness. 

209 
00:20:21,100 --> 00:20:23,000 
>> MARK: Have you finished any courses as well? 

210 
00:20:23,100 --> 00:20:36,000 
>> MICHAEL: I've finished i.e. a herbal remedies course, with diploma obviously. For over 6 years I've been a practitioner in energy medicine. I own specialized devices - one such device costs more than 12 thousand zloty (PLN).

211 
00:20:36,100 --> 00:20:43,000 
Naturally, I could sell them, if only people had a little more awareness about energy medicine. 

212 
00:20:43,100 --> 00:20:47,000 
I have 3 such devices - two are held in lien, because of financial problems I had to pawn them. 

213 
00:20:47,100 --> 00:20:55,500 
One I left for myself because, well, without these devices I do not know in fact whether I would still be living at all. Because, what took place allows me to do clearing on a regular basis. 

214 
00:20:55,750 --> 00:20:56,800 
>> MARK: Zapper, i.e.?

215 
00:20:57,100 --> 00:21:03,000 
>> MICHAEL: Zapper, of course, but also energy medicine. Thus, in spite of repeated intrusions into my body, I'm still alive. 

216 
00:21:05,100 --> 00:21:17,000 
There will also be included a link to the site (www.vibrationalhealing.pl) which I have for the last several months. All of the information will be there, so if anyone is interested in services in this area, I'm thankful for all the help. 

217 
00:21:17,100 --> 00:21:28,000 
>> MARK: I urge all owners of herbal shops - if you would like to help Michael, simply offer him a job order. There won't be any big fees for this, right?

218 
00:21:28,100 --> 00:21:53,000 
>> MICHAEL: At vibrationalhealing.pl obviously I also have information that there is a minimum amount, but there's no maximum amount. If someone wants to show appreciation for my work for the last 6 years, when I was working mostly for free and paid out of my pocket - these are tens of thousands or hundreds of thousands of dollars that have been invested in what I did - I do not speak only about the anti-vaccine campaign. 

219 
00:21:54,100 --> 00:22:21,000 
And, of course, what also occurred as repercussions, by those who believe that they have the right to decide what you can and cannot talk about. So, if someone can find gratitude in this way, I do not have anything against (that) obviously. On the other hand, I do not sit on the street or at a booth with beer and ask for money. All the time I am doing what I can, so if anyone can show their gratitude, I will also be grateful.

220 
00:22:22,100 --> 00:22:39,500 
>> MARK: Ladies and gentlemen, there are so many books to be translated into Polish and Polish into English. If someone wants to e.g. publish their book in English, we can assist them. Michael is perfect when it comes to translation. Well, and we need such people. It is simply unbelievable that someone like Michael at the moment this simply looks like begging.

221 
00:22:40,100 --> 00:23:05,000 
>> MICHAEL: Well, maybe not begging, but this is also, of course, a way for me to protect myself. On the other hand, I have no choice but to start talking about these matters. I expect that at last I will able to afford to resolve sort these matters out properly. Because, these are repeated violations of human rights. It is systematic humiliations, blocking (me)... 

222 
00:23:05,100 --> 00:23:22,000 
>> MARK: Perhaps I should mention that also, i.e. thanks to you, this level of public awareness shot up exorbitantly since year 2009. At the momnent, I can see on my blog how many people understand important issues and before, when we were starting - they were disbelieving. 

223 
00:23:22,100 --> 00:23:38,000 
>> MICHAEL: I certainly have some contribution in this, of course. Because, as I say, despite the fact that the 'swine case' - this site was suspended in year 2010 - but I work all the time on many other fronts. Perhaps I will not go into some of the topics, because... 

224 
00:23:38,100 --> 00:23:40,000 
>> MARK: Perhaps in (our) next conversations? 

225 
00:23:40,100 --> 00:23:58,000 
>> MICHAEL: Perhaps in the next talks. However, as I say - thank you Mark that we could meet and maybe during our next talks we'll have the opportunity to talk about these topics. Because, as we already mentioned - it's not only the vaccinations. There are a lot of these topics. And it really comes down to large-scale depopulation.

226 
00:23:58,100 --> 00:24:16,000 
>> MARK: Ladies and gentlemen, this is about our lifes. About the lifes of us all - your children, your grandchildren. If at all we'll live up to that time, when there are grandchildren. No, I think we will win after all, Michael, and I think that the possibility of publishing these kinds of talks on the Internet, even using a service that is in possession of these people - 'YouTube' - also proves that we are strong.

227 
00:24:16,100 --> 00:24:45,000 
>> MICHAEL: First and foremost, it's necessary to speak up - not to be afraid to speak up. And, of course, to arrive - through experience and one's own work - at what is the truth, because in our times, it is very difficult to capture what is real and what is virtual. So, the most important thing is not get intimidated. In spite of all that is taking place, there are certain values that should be sustained.

228 
00:24:46,100 --> 00:25:10,500 
So, no one will shut my mouth, despite all that took place. And thanks to Mark, I hope, we will reach a larger group of people and, as I say, it's enough if you give me a job order. I did translations, I'm doing (these) all the time. Only there are simply so few of them that I'm unable to survive from these. However, by all means, books, exactly, or any sort of publications...

229 
00:25:10,750 --> 00:25:12,500 
>> MARK: Bilateral? 

230 
00:25:13,100 --> 00:25:24,500 
>> MICHAEL: ... Bilateral. From English language to Polish language, from Polish language to English language. These are things that I can do without a problem. I can also do light physical work, of course. On the other hand, I cannot unfortunately, due to my health, go for construction or some such thing normally yet. 

231 
00:25:25,100 --> 00:25:40,000 
>> MARK: No, that's not the point. Dear Ladies and Gentlemen - contact with Michael will be given in the link below, so it will shown during the course of this interview, when assembled. However, I'm already inviting you for another conversation with Michael, this time it will about... 

232 
00:25:40,100 --> 00:25:53,000 
>> MICHAEL: A little lighter topic.
>> MARK: ... a little lighter topic. We'll be talking about these things that plague us, precisely. You are an excellent specialist. I can see Michael's knowledge. You've deepened your knowledge in the last 4 years. 

233 
00:25:53,100 --> 00:26:03,000 
>> MICHAEL: I'm constantly expanding on my knowledge. And the so called 'rabbit hole' is so deep that the deeper you enter, the more you realize that you know nothing. 

234 
00:26:03,100 --> 00:26:13,000 
You have to remember that the people who are laughing, saying that it is a conspiracy of some sort, or that these are conspiracy theories, have no idea what they are talking about...

235 
00:26:13,100 --> 00:26:14,200 
>> MARK: Or, they are agents. 

236 
00:26:14,500 --> 00:26:32,000 
>> MICHAEL: Either they're agents or have no idea what they're talking about. It is said that 'ignorance is bliss'. These publications are normally available to all - it's enough to spend 15 minutes daily to get informed. Or, simply, to learn humility and to not say anything, instead of talking nonsense. So, that is my advice. Thank you very much. 

237 
00:26:32,100 --> 00:26:34,000 
>> MARK: Thank you very much for the interview.


