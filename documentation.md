---
title: Documentation to download
layout: page
permalink: "/documentation/index.html"
ref: documentation
lang: en
sortkey: 2
---

## Medical

### Diagnoses

1. <a href="/pliki/Medyczne/Badania_Genetyczne_20071123.pdf" target="_blank">Results of genetic tests carried out on 23 November 2007</a> (Polish language version, original)

2. <a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">Diagnoses and recommendations issued by physician who lead candidiasis treatment in year 2013, during visits in May, November and December 2013</a> (Polish language version, original)

3. Results from ultrasound examination of urinary tract (images, description), conducted on January 29, 2014, immediately after the events

   * <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">Polish language version, original</a>

   * <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">English language version, sworn translation</a>


4. Results from ultrasound examination of urinary tract (images, description), conducted on January 11, 2016

   * <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">Polish language version, sworn translation</a>

   * <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">English language version, original</a>


5. Results from computed tomography examination of pelvis, conducted on February 13, 2018

   * <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">Polish language version, sworn translation</a>

   * <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">German language version, original</a>


6. Results from magnetic resonance examination of nervous system, conducted on March 12, 2018

   * <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">Polish language version, sworn translation</a>

   * <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">English language version, original</a>


7. Results from ultrasound examination of nervous system, conducted on April 6, 2018

   * <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">Polish language version, sworn translation</a>

   * <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">English language version, original</a>


8. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_Optical-Coherence-Tomography.pdf" target="_blank">Selected images from Optical Coherence Tomography (OCT) examination of area where multiple puncture wounds are visible, conducted on April 6, 2018</a>

9. Medical report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018

   * <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">Polish language version, sworn translation</a>

   * <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">English language version, original</a>



### Imaging

1. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_USG-of-nerves.pdf" target="_blank">Selected frames from USG examination of nervous system, conducted on April 6, 2018</a>

2. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_Neurologist-PlasticSurgeon_Clinical-Images.pdf" target="_blank">Clinical images from examinations and consultations with physicians specializing in neurology, plastic surgery and radiology, conducted on April 6 and 20, 2018</a>

3. <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_MR-Neurography-of-Pelvis.pdf" target="_blank">Selected frames from DICOM data from MRI examination of nervous system, conducted on March 12, 2018</a>

4. DICOM data from MRI & CT examinations, conducted at Mahajan Imaging in New Delhi, India, on March 12, 2018

   * pCloud: <a href="https://my.pcloud.com/publink/show?code=XZWdP4kZa1akm5mWcsjAFEDlDwTpJB1tf1dX" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>

   * Jottacloud: <a href="https://www.jottacloud.com/s/2105c23f755f08544c58e6221888b17226f" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>

   * Google Drive: <a href="https://drive.google.com/open?id=1ZJjPMAHRr4Xbiz3LdpYl2SaocqGHHPFc" target="_blank">Mahajan-Imaging_MRI&CT_DICOM.zip</a>

   * MD5 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Mahajan-Imaging_MRI&CT_DICOM.zip.md5" target="_blank">e64a190463f0a2946cf2136690f95839</a>

   * SHA256 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Mahajan-Imaging_MRI&CT_DICOM.zip.sha256" target="_blank">add66c70beeeeb6536d6c1186b8882a0d02a2e21dbbc28588927f5818e61ba1c</a>


5. DICOM data from CT examination conducted at Radiology Center Vienna in Vienna, Austria, on February 13, 2018

   * pCloud: <a href="https://my.pcloud.com/publink/show?code=XZGPP4kZ3zAExfdiPcbMlkCqctU3w7nh5Kk0" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>

   * Jottacloud: <a href="https://www.jottacloud.com/s/210c260e889cc9b479baa41564db371a271" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>

   * Google Drive: <a href="https://drive.google.com/open?id=1HnTyAGHQHLJkkGGCzoLhZUBNjfYRAFnE" target="_blank">Radiology-Center-Vienna_CT_DICOM.zip</a>

   * MD5 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Radiology-Center-Vienna_CT_DICOM.zip.md5" target="_blank">4eaba114782a2c209d2263384c9b54b2</a>

   * SHA256 checksum: <a href="/pliki/Medyczne/Zdjecia/DICOM/Radiology-Center-Vienna_CT_DICOM.zip.sha256" target="_blank">190f44f82a8f1c20f117ef6c2f48105ad428c811682f64ad72c9bafdeb904537</a>


### Affidavits

1. <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">Affidavit issued by physician who lead candidiasis treatment in year 2013, stating that candidiasis is not the cause of the observed bodily injuries and damage to genitourinary</a> (Polish language version, original)

### Medical tourism


#### India (2018)

1. <a href="/pliki/Medyczne/Turystyka/medical_tourism_india_visa_201802.pdf" target="_blank">Medical visa to India (02.2018)</a>

2. <a href="/pliki/Medyczne/Turystyka/medical_tourism_india_plane_20180227.pdf" target="_blank">Flight to New Delhi, India (27.02.2018)</a>

3. <a href="/pliki/Medyczne/Turystyka/medical_tourism_india_plane_20180316.pdf" target="_blank">Flight from New Delhi, India (16.03.2018)</a>


#### Austria (2018)

1. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180211.pdf" target="_blank">Transport to Vienna, Austria (11.02.2018)</a>

2. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180214.pdf" target="_blank">Transport from Vienna, Austria (14.02.2018)</a>

3. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180405.pdf" target="_blank">Transport to Vienna, Austria (05.04.2018)</a>

4. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_train_20180407.pdf" target="_blank">Transport from Vienna, Austria (07.04.2018)</a>

5. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180419.pdf" target="_blank">Transport to Vienna, Austria (19.04.2018)</a>

6. <a href="/pliki/Medyczne/Turystyka/medical_tourism_austria_bus_20180420.pdf" target="_blank">Transport from Vienna, Austria (20.04.2018)</a>


### Inquiries regarding conducting examinations and medico-legal opinion in Poland (2017)

1. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20170622.pdf" target="_blank">Message dated 22.06.2017</a>

2. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20170626.pdf" target="_blank">Message dated 26.06.2017</a>

3. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171018.pdf" target="_blank">Message dated 18.10.2017</a>

4. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171020.1.pdf" target="_blank">Message dated 20.10.2017</a>

5. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171020.2.pdf" target="_blank">Message dated 20.10.2017</a>

6. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171023.pdf" target="_blank">Message dated 23.10.2017</a>

7. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171026.pdf" target="_blank">Message dated 26.10.2017</a>

8. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171103.pdf" target="_blank">Message dated 3.11.2017</a>

9. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171110.pdf" target="_blank">Message dated 10.11.2017</a>

10. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171114.1.pdf" target="_blank">Message dated 14.11.2017</a>

11. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171114.2.pdf" target="_blank">Message dated 14.11.2017</a>

12. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171115.pdf" target="_blank">Message dated 15.11.2017</a>

13. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20171122.pdf" target="_blank">Message dated 22.11.2017</a>

14. <a href="/pliki/Medyczne/Zapytania/medical_inquiries_pl_20180222.pdf" target="_blank">Message dated 22.02.2018</a>


### Negligence

#### CM Medicover (01/2014)

1. <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_16-01-2014.pdf" target="_blank">Documentation regarding visit to the Medicover Medical Center in Krakow, Poland, shortly after events of January 2014, on January 16 2014, during which doctors ordered examinations and medicines that had nothing to do with the symptoms described</a>

2. <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_22-01-2014.pdf" target="_blank">Documentation regarding visit to the Medicover Medical Center in Krakow, Poland, shortly after events of January 2014, on January 22 2014, during which doctors ordered examinations and medicines that had nothing to do with the symptoms described</a>


#### CM iMed24 (12/2014)

1. <a href="/pliki/Medyczne/MHSiemaszko_Uromed_skierowanie_Rezonans-Magnetyczny_20141120.pdf" target="_blank">Referral for magnetic resonance imaging (MRI) of **PELVIS** (entire pelvis, including the kidneys and scars in the groin area), dated 20.11.2014</a>

2. <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy_rachunek_20141212.pdf" target="_blank">Bill for magnetic resonance imaging (MRI) of **LOWER PELVIS** (EXCLUDING kidneys and scars in the groin area!), conducted in iMed24 Medical Center in Krakow, Poland on December 12, 2014</a>

3. <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy-mniejszej-nie-calej_BZDURA_raport_20141212.pdf" target="_blank">Results from magnetic resonance imaging (MRI) of **LOWER PELVIS** (EXCLUDING kidneys and scars in the groin area!), conducted in iMed24 Medical Center in Krakow, Poland on December 12, 2014</a>


#### Praga (12/2015)

1\. Medical negligence lawsuit, Michal Siemaszko vs European Patient Service s.r.o. et al, filed on 3.12.2018

 * <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf" target="_blank">English language version</a>

 * <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf" target="_blank">Czech language version</a>


2\. Demand for payment letter, dated 28.01.2018

 * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">Polish language version, translation</a>

 * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160128.oryginal.pdf" target="_blank">Czech language version, original</a>


3\. Demand for payment letter, dated 29.02.2018

 * <a href="/pliki/Prawne/EPSUK/EPS_Lotterova_Demand-for-payment_20160229.oryginal.pdf" target="_blank">Czech language version, original</a>


#### Delhi (3/2018)

1. <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf" target="_blank">Medical negligence lawsuit, Michal Siemaszko vs Apollo et al, filed in August 2018 (**this lawsuit has been settled!**)</a>

2. <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf" target="_blank">Reply to Opposition in medical negligence lawsuit, Michal Siemaszko vs Apollo et al, dated January 2019</a>


### Other

* <a href="/pliki/Medyczne/Zdjecia/MHSiemaszko_symptoms_infographic.pdf" target="_blank">Infographic depicting the permanent bodily injuries</a>


<hr class="thin-separator" />

## Human Rights

### ECHR (2019/05)

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG.20190521.pdf" target="_blank">Application form dated May 21st 2019</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_Statement-of-the-facts_Annex.20190521.pdf" target="_blank">Annex to Application form, section E. Statement of the Facts</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_List-of-accompanying-documents_Annex.20190521.pdf" target="_blank">Annex to Application Form, section I. List of accompanying documents</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-postage_20190521.pdf" target="_blank">Proof of postage, dated May 21st 2019</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-fax-transmission_20190522.pdf" target="_blank">Proof of successful fax transmission, dated May 22nd 2019</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Email_20190521.pdf" target="_blank">Email sent to ECHR, dated May 21st 2019</a>

* <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-delivery_20190603.pdf" target="_blank">Proof of package delivery, dated June 3rd 2019</a>

* <a href="/echr-complaint-201905-package-contents/index.html" target="_blank">Complete set of photographs (along with original EXIF metadata) depicting contents of the package</a>


### Polish Ombudsman (2018/07/19)

* <a href="/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf" target="_blank">Address of the Criminal Law Team of the Office of the Polish Ombudsman to Krakow Regional Prosecutor, dated 19.07.2018</a>


<hr class="thin-separator" />

## Legal

### Submitted

#### 2019

1. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rb.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Rafał Babiński", dated 06.05.2019</a>

2. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_db.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Dorota Bojanowska", dated 06.05.2019</a>

3. <a href="/pliki/Prawne/falszowanie-nieprawda/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_falszowanie-nieprawda.pdf" target="_blank">"Notification of a crime under art. 270 § 1 and 276 of the Polish Penal Code", dated 06.05.2019</a>

4. <a href="/pliki/Prawne/DdSPZiK/II-Kp-9-19-K_zgloszenie-do-DdSPZiK.20190220.pdf" target="_blank">"Request for intervention" addressed to the Department for Organised Crime and Corruption, dated 20.02.2019</a>

5. <a href="/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf" target="_blank">"Notification of a crime under art. 156 §1, art. 158 §, art. 159, art. 193, art. 233 § 1, art. 234, art. 236 § 1, art. 238, art. 239 § 1, art. 258 § 1 of the Polish Penal Code committed by Monika Hudyka", dated 6.02.2019</a>

6. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Beata Sichelska", dated 04.02.2019</a>

7. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by Police officer Monika Górowska", dated 04.02.2019</a>

8. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by Police officer Robert Cygan", dated 04.02.2019</a>

9. <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Tomasz Pawlik", dated 04.02.2019</a>

10. <a href="/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by Police officer Daria Curzydło", dated 02.01.2019</a>


#### 2018

1. <a href="/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf" target="_blank">"Appeal against the decision of the District Prosecutor in Tarnów of November 22, 2018, refusing to initiate an investigation, issued in case no. 1. PR 4 DS360.2018, delivered on December 20 2018", dated 27.12.2018</a>

2. <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20181211.pdf" target="_blank">"Request for intervention" addressed to the Minister of Justice / Prosecutor General of Republic of Poland, dated 11.12.2018</a>

3. <a href="/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf" target="_blank">"Notification of a crime under Articles 156 §1, 157 §1, 160 §1, 162 §1, 192 §1, 193, 239, 258 and 268 of the Polish Penal Code, infringement of art. 30, 32, 38, 39, 40, 45, 47 oraz 77 of the Constitution of the Republic of Poland, in connection with art. 44 of the Constitution of the Republic of Poland, as well as violations of Articles 2 („Right to life”), 3 („Prohibition of inhuman and degrading treatment”), 6 („Right to a fair trial”), 13 („Right to an effective remedy”) and 14 („Prohibition of discrimination”) of the European Convention on Human Rights, in connection with art. 9 of the Constitution of the Republic of Poland", dated 11.12.2018</a>

4. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Maria Zębala", dated 12.07.2018</a>

5. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Małgorzata Lipska", dated 12.07.2018</a>

6. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Edyta Kulik", dated 12.07.2018</a>

7. <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf" target="_blank">"Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Bartłomiej Legutko", dated 12.07.2018</a>

8. <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-rpo_20180613.pdf" target="_blank">"Request for intervention" addressed to the Polish Ombudsman, dated 13.06.2018</a>

9. <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20180613.pdf" target="_blank">"Request for intervention" addressed to the Minister of Justice / Prosecutor General of Republic of Poland, dated 13.06.2018</a>

10. <a href="/pliki/Prawne/Zlozone/mhs_skarga-na-przewleklosc-postepowania-karnego-przygotowawczego_20180605.pdf" target="_blank">"Complaint for infringement of a party’s right to hear the case in preparatory criminal proceedings without undue delay", dated 5.06.2018</a>

11. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_bl.20180511.zlozone.pdf" target="_blank">Copy of the document “Complaint against the Prosecutor Bartlomiej Legutko”, dated 11.05.2018</a> (Polish language version, original)

12. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie_uzupelnienie.20180507.zlozone.pdf" target="_blank">Copy of the document “Addendum to the application for designation of entity outside the jurisdiction of Krakow-Krowodrza Regional Prosecutor's Office to conduct investigation”, dated 7.05.2018</a> (Polish language version, original)

13. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180504.zlozone.pdf" target="_blank">Copy of the response to the letter from the Krakow-Krowodrza Regional Prosecutor's Office, dated 04.05.2018</a> (Polish language version, original)

14. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie.20180417.zlozone.pdf" target="_blank">Copy of the document “Application for designation of entity outside the jurisdiction of Krakow-Krowodrza Regional Prosecutor's Office to conduct investigation”, dated 17.04.2018</a> (Polish language version, original)

15. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_ek.20180413.zlozone.pdf" target="_blank">Copy of the document “Complaint against the Prosecutor Edyta Kulik”, dated 13.04.2018</a> (Polish language version, original)

16. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-udzielenie-informacji.20180404.zlozone.pdf" target="_blank">Copy of the document “Request for information”, dated 4.4.2018</a> (Polish language version, original)

17. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-doreczenie.20180404.zlozone.pdf" target="_blank">Copy of the document “Request for correct delivery of the decision refusing to initiate an investigation issued April 14th 2014”, dated 4.04.2018</a> (Polish language version, original)

18. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zazalenie-na-bezczynnosc.20180404.zlozone.pdf" target="_blank">Copy of the document “Complaint for inaction of the Prosecutor”, dated 4.04.2018</a> (Polish language version, original)

19. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180404.zlozone.pdf" target="_blank">Copy of the response to the letter from the Krakow-Krowodrza Regional Prosecutor's Office, dated 03.04.2018</a> (Polish language version, original)

20. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180129.zlozone.pdf" target="_blank">Copy of the response to the letter from the District Prosecutor's Office in Krakow, dated 16.02.2018</a> (Polish language version, original)

21. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_wniosek-o-wylaczenie-prokuratorow.20180109.zlozone.pdf" target="_blank">Copy of the document “Request for exclusion of prosecutors of the Regional Prosecutor's Office”, dated 9.01.2018</a> (Polish language version, original)

22. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_zawiadomienie_20180109.zlozone.pdf" target="_blank">Copy of the document “Notification of a crime under Articles 156 §1, 157 §1, 160 §1, 162 §1, 192 §1 and 193 of the Penal Code, as well as violations of Articles 2 („the right to life”), 3 („prohibition of inhuman and degrading treatment”), 6 („the right to a fair trial”) and 14 („the prohibition of discrimination”) of the European Convention on Human Rights”, submitted in person at the Stakeholder Service Office of the National Prosecutor's Office at Rakowiecka 26/30 in Warsaw on January 9, 2018</a> (Polish language version, original)


#### 2014

1. <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zawiadomienie_20140228.zlozone.pdf" target="_blank">Copy of the notification of a crime under art. 197 §1 of the Penal Code, dated 28.02.2014</a> (Polish language version, original)


### Received

#### 2018

 1. <a href="/pliki/Prawne/Odpowiedzi/PK-II_Ko2_128.2018_20180116.pdf" target="_blank">Copy of the letter from the National Prosecutor's Office dated 16.01.2018</a> (Polish language version, original)

 2. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180129.pdf" target="_blank">Copy of the letter from the District Prosecutor's Office in Krakow dated 29.01.2018</a> (Polish language version, original)

 3. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180215.pdf" target="_blank">Copy of the letter from the District Prosecutor's Office in Krakow dated 15.02.2018</a> (Polish language version, original)

 4. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180219.pdf" target="_blank">Copy of the letter from the District Prosecutor's Office in Krakow dated 19.02.2018</a> (Polish language version, original)

 5. <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180307.pdf" target="_blank">Copy of the letter from the Krakow-Krowodrza Regional Prosecutor's Office dated 07.03.2018</a> (Polish language version, original)

 6. <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180413.pdf" target="_blank">Copy of the letter from the Krakow-Krowodrza Regional Prosecutor's Office dated 13.04.2018</a> (Polish language version, original)

 7. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Dsn-47.2018.Kkr_20180425.pdf" target="_blank">Copy of the letter from the District Prosecutor's Office in Krakow dated 25.04.2018</a> (Polish language version, original)

 8. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Dsn-47.2018.Kkr_20180427.pdf" target="_blank">Copy of the letter from the District Prosecutor's Office in Krakow dated 27.04.2018</a> (Polish language version, original)

 9. <a href="/pliki/Prawne/Odpowiedzi/PO-III_Dsn-47.2018.Kkr_20180523.pdf" target="_blank">Copy of the letter from the District Prosecutor's Office in Krakow dated 23.05.2018</a> (Polish language version, original)


#### 2014

 1. <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">Copy of the decision refusing to initiate an investigation pursuant to art. 17. § 1 point 1 of the Code of Penal Procedure, on the grounds that candidiasis is the cause of the observed permanent bodily injuries, dated 14.04.2014</a> (Polish language version, original)


<hr class="thin-separator" />

## Non-Governmental Organizations (NGOs)

### In-person contact (11/2017)

* <a href="/pliki/NGOs/in-person/ngos_contact_in-person_krakow-london_20171106.pdf" target="_blank">Cracow-London flight (2017/11/06)</a>

* <a href="/pliki/NGOs/in-person/ngos_contact_in-person_london-brussels_20171108.pdf" target="_blank">London-Brussels train (2017/11/08)</a>

* <a href="/pliki/NGOs/in-person/ngos_contact_in-person_brussels-krakow_20171110.pdf" target="_blank">Brussels-Cracow flight (2017/11/10)</a>


### E-mail contact

a. <a href="https://www.amnesty.org/" target="_blank">"Amnesty International"</a>

 * <a href="/pliki/NGOs/emails/amnesty_20171122.pdf" target="_blank">/pliki/NGOs/emails/amnesty_20171122.pdf</a>


b. <a href="http://www.apt.ch/" target="_blank">"Association for the prevention of torture"</a>

 * <a href="/pliki/NGOs/emails/apt_20171030.pdf" target="_blank">/pliki/NGOs/emails/apt_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/apt_20171215.pdf" target="_blank">/pliki/NGOs/emails/apt_20171215.pdf</a>
 * <a href="/pliki/NGOs/emails/apt_20190522.pdf" target="_blank">/pliki/NGOs/emails/apt_20190522.pdf</a>


c. <a href="https://www.couragefound.org/about-the-courage-foundation/" target="_blank">"Courage Foundation"</a>

 * <a href="/pliki/NGOs/emails/couragefound_20171030.pdf" target="_blank">/pliki/NGOs/emails/couragefound_20171030.pdf</a>


d. <a href="https://dignityinstitute.org/" target="_blank">"The Danish Institute Against Torture"</a>

 * <a href="/pliki/NGOs/emails/dignity_20171030.pdf" target="_blank">/pliki/NGOs/emails/dignity_20171030.pdf</a>


e. <a href="http://www.mct-onlus.it/en/" target="_blank">"Doctors Against Torture Humanitarian Organization"</a>

 * <a href="/pliki/NGOs/emails/mct-onlus.it_20171218.pdf" target="_blank">/pliki/NGOs/emails/mct-onlus.it_20171218.pdf</a>


f. <a href="https://eldh.eu/en/" target="_blank">"European Association of Lawyers for Democracy & World Human Rights"</a>

 * <a href="/pliki/NGOs/emails/eldh_20190522.pdf" target="_blank">/pliki/NGOs/emails/eldh_20190522.pdf</a>


g. <a href="https://www.ecchr.eu/en/international-crimes-accountability/" target="_blank">"European Center for Constitutional and Human Rights"</a>

 * <a href="/pliki/NGOs/emails/ecchr_20190522.pdf" target="_blank">/pliki/NGOs/emails/ecchr_20190522.pdf</a>


h. <a href="https://www.freedomfromtorture.org/" target="_blank">"Freedom from Torture"</a>

 * <a href="/pliki/NGOs/emails/freedomfromtorture_20171218.pdf" target="_blank">/pliki/NGOs/emails/freedomfromtorture_20171218.pdf</a>


i. <a href="http://www.hfhr.pl/en/" target="_blank">"Helsinki Foundation for Human Rights"</a>

 * <a href="/pliki/NGOs/emails/hfhr_20180112.pdf" target="_blank">/pliki/NGOs/emails/hfhr_20180112.pdf</a>


j. <a href="https://ijrcenter.org/" target="_blank">"The International Justice Resource Center (IJRC)"</a>

 * <a href="/pliki/NGOs/emails/ijrcenter_20171030.pdf" target="_blank">/pliki/NGOs/emails/ijrcenter_20171030.pdf</a>


k. <a href="http://www.ptpa.org.pl/" target="_blank">"Polish Society of Anti-Discrimination Law"</a>

 * <a href="/pliki/NGOs/emails/ptpa_20180119.1.pdf" target="_blank">/pliki/NGOs/emails/ptpa_20180119.1.pdf</a>
 * <a href="/pliki/NGOs/emails/ptpa_20180119.2.pdf" target="_blank">/pliki/NGOs/emails/ptpa_20180119.2.pdf</a>


l. <a href="http://www.redress.org/" target="_blank">"Redress - Ending torture, seeking justice for survivors"</a>

 * <a href="/pliki/NGOs/emails/redress_20171030.pdf" target="_blank">/pliki/NGOs/emails/redress_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/redress_20190522.pdf" target="_blank">/pliki/NGOs/emails/redress_20190522.pdf</a>


m. <a href="https://trialinternational.org/" target="_blank">"TRIAL International"</a>

 * <a href="/pliki/NGOs/emails/trialinternational_20171030.pdf" target="_blank">/pliki/NGOs/emails/trialinternational_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/trialinternational_20190523.pdf" target="_blank">/pliki/NGOs/emails/trialinternational_20190523.pdf</a>


n. <a href="https://www.fidh.org/en/" target="_blank">"Worldwide movement for human rights"</a>

 * <a href="/pliki/NGOs/emails/fidh_20171030.pdf" target="_blank">/pliki/NGOs/emails/fidh_20171030.pdf</a>
 * <a href="/pliki/NGOs/emails/fidh_20171107.2.pdf" target="_blank">/pliki/NGOs/emails/fidh_20171107.2.pdf</a>
 * <a href="/pliki/NGOs/emails/fidh_20171107.pdf" target="_blank">/pliki/NGOs/emails/fidh_20171107.pdf</a>


<hr class="thin-separator" />

## Communication

### Certified SMS communication (12/2013 - 04/2014)

* <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">Protocol of securing SMS communication which took place in the period between December 2013 and end of March 2014, performed by IT forensics specialists in August 2017, along with invoice and acceptance protocol</a>

* <a href="/pliki/Komunikacja/GT-E1200_721030078_raport.pdf" target="_blank">SMS messages incoming from / outgoing to number +48721030078, in the period from 04/03/2014 to 30/03/2014, certified by IT forensics specialists in August 2017</a>

* <a href="/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf" target="_blank">SMS messages incoming from / outgoing to number +48512355495, from December 28, 2013, a few days before the described events, certified by IT forensics specialists in August 2017</a>

* <a href="/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf" target="_blank">SMS messages incoming from / outgoing to number +48503990172, in the period from 18/12/2013 to 14/01/2014, certified by IT forensics specialists in August 2017</a>

### E-mail communication

  * <a href="/pliki/Komunikacja/MHSiemaszko_Email_Compensation-for-failed-surgery.20141105.pdf" target="_blank">E-mail message received from Mr. Tomasz Gibas on November 5, 2014 regarding the alleged "failed surgery"</a>

### Facebook messenger communication

  * <a href="/pliki/Komunikacja/Screenshot_20181219-172035_Messenger.pdf" target="_blank">Fragment of communication on Facebook Messenger with Monika Hudyka, when on 3 February 2014 she initiated contact with me </a>

<hr class="thin-separator" />

## Rental (10/2013 - 2/2014)

### Flat (10/2013 - 2/2014)

* <a href="/pliki/Wynajem/Szymanowskiego_umowa_wypowiedzenie.pdf" target="_blank">Termination of rental agreement for flat located at Szymanowskiego 5/10 in Krakow, Poland, where the events of January 2014 took place, rendered shortly after these events</a>

* <a href="/pliki/Wynajem/Szymanowskiego_umowa.pdf" target="_blank">Rental agreement for flat located at Szymanowskiego 5/10 in Krakow, Poland, where the events of January 2014 took place</a>


### Address box

* <a href="/pliki/Wynajem/MHSiemaszko_Address-box_Rental-agreement.pdf" target="_blank">Address box rental agreement for the Karmelicka 55, Krakow address, where decision refusing to initiate investigation was sent in 2014</a>

* <a href="/pliki/Wynajem/MHSiemaszko_Address-box_Terms-conditions.pdf" target="_blank">Terms and conditions for the Karmelicka 55, Krakow address box rental agreement, which clearly do not contain any exclusions allowing delivery of mail in criminal proceedings to unauthorized persons</a>

* <a href="/pliki/Wynajem/MHSiemaszko_Address-box_Authorization.pdf" target="_blank">Authorization document for the Karmelicka 55, Krakow address box rental agreement, which clearly does not authorize accepting mail from law enforcement in my name</a>

<hr class="thin-separator" />

## Business (10/2013 - 2/2014)

1. <a href="/pliki/Wspolpraca/Zerochaos_wypowiedzenie-umowy.pdf" target="_blank">Termination of 2-year business contract started less than 3 months before the events of January 2014, rendered shortly after events of January 2014, on 11.02.2014</a>

2. Zurich, Switzerland delegation in December 2014, shortly before events of January 2014:
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf</a>
   * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf</a>

3. <a href="/pliki/Wspolpraca/Zerochaos_umowa.pdf" target="_blank">2-year business contract started less than 3 months before the events of January 2014</a>

<hr class="thin-separator" />

## Other

### Criminal background check certificates

#### 2014

 * <a href="/pliki/Inne/mhs_no-criminal-record_2014.pdf" target="_blank">Criminal background check certificate from 2014</a> (Polish language version, original)


#### 2015

 * <a href="/pliki/Inne/mhs_no-criminal-record_2015.pdf" target="_blank">Criminal background check certificate from 2015</a> (Polish language version, original)


#### 2017

 * <a href="/pliki/Inne/mhs_no-criminal-record_2017.pdf" target="_blank">Criminal background check certificate from 2017</a> (Polish language version, original)


#### 2018

 * <a href="/pliki/Inne/mhs_no-criminal-record_2018.pdf" target="_blank">Criminal background check certificate from 2018</a> (Polish language version, original)


### Studies

* <a href="/pliki/Inne/AkademiaEkonomiczna_zawiadomienie_20041011.pdf" target="_blank">Affidavit confirming I studied at Cracow University of Economics, B.A. International Relations – degree programme not completed due to professional commitments</a> (Polish language version, original)


### Marital status

* <a href="/pliki/Inne/mhs_marital-status-divorced_20180518.pdf" target="_blank">Certificate of marital status from 2018</a>

### USA (1998 - 2003)

#### FOIA

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_response.2017.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_response.2017.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_cover-letter.2017.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_FOIA_cover-letter.2017.pdf</a>

#### INS

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_G-325a.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_G-325a.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-131.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-131.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-485.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-485.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765.12012001.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765.12012001.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765_receipt-2002.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_I-765_receipt-2002.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_documents-2002.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_INS_documents-2002.pdf</a>

#### Social Security

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Social-Security_statement-2000.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Social-Security_statement-2000.pdf</a>

#### IRS

* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_1999-tax-return_refund.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_1999-tax-return_refund.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_2000-tax-return_refund.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_2000-tax-return_refund.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_w2_1998-2002.pdf" target="_blank">/pliki/Inne/USA_1988-2003/MHSiemaszko_Taxes_w2_1998-2002.pdf</a>

#### Certificates

* <a href="/pliki/Inne/USA_1988-2003/Certificate-of-birth_K-Siemaszko.pdf" target="_blank">/pliki/Inne/USA_1988-2003/Certificate-of-birth_K-Siemaszko.pdf</a>
* <a href="/pliki/Inne/USA_1988-2003/Certificate-of-death_Z-Siemaszko.pdf" target="_blank">/pliki/Inne/USA_1988-2003/Certificate-of-death_Z-Siemaszko.pdf</a>
