---
layout: home
permalink: "index.html"
ref: index
lang: pl
image: /pliki/Prezentacja-201907/not-surgery_presentation.jpg
---

<div class="videoWrapper">
  <iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/AmwlRVHCslm4/"></iframe>
</div>

<div class="videoExtras">
  <strong>Pobierz</strong>: <a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.mp4" | relative_path }}" target="_blank">wersja MP4</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.webm" | relative_path }}" target="_blank">wersja WEBM</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-slides.pdf" | relative_path }}" target="_blank">slajdy (PDF)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-narration.pdf" | relative_path }}" target="_blank">narracja (PDF)</a>
</div>

<br/>

<div style="text-transform: uppercase;">
<strong>Ważne</strong>: <a href="{{ "/inne/2021/03/14/zamkniecie-serwisu-przez-GitHub.html" | relative_path }}" target="_blank">alternatywne metody dostępu do serwisu z uwagi na niedawne zamknięcie serwisu przez GitHub</a>
</div>

<br/>

<img src="/pliki/Inne/not-surgery_infographic-with-photos.PL.png" title="Eksperymenty medyczne/naukowe przeprowadzone na mnie bez mojej zgody" />

<br/>

Chciałem Państwa zainteresować sprawą, która ciągnie się już od wielu lat i w której miały miejsce liczne wątki, które połączone ze sobą i skrupulatnie dokumentowane dają obraz bardzo poważnych naruszeń najbardziej podstawowych i najważniejszych praw człowieka, zapisanych w artykułach Konstytucji RP i Europejskiej Konwencji Praw Człowieka, a mianowicie:
* Art. 2 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności (“prawo do życia”)
* Art. 39 Konstytucji RP (“zakaz wykonywania zabiegów medycznych bez wyrażonej zgody”)
* Art. 3 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności oraz Art. 40 Konstytucji RP (“zakaz nieludzkiego lub poniżającego traktowania”)

Sam fakt wydarzenia, którego przestępstwo początkowo dotyczyło – tj. trwałego uszkodzenia ciała w wyniku napadu w ówczesnym miejscu zamieszkania, mieszkaniu wynajmowanym w Krakowie – nie byłby może wystarczająco interesujący, ponieważ takie sytuacje zdarzają się niestety, ale to co miało miejsce od tego czasu przez ponad 4 lata już aby tą sprawę właściwie udokumentować w celu wyjaśnienia i wyegzekwowania swoich praw, tj.:

1\. Fakt rażącego naruszenia obowiązków procesowych i nieprzeprowadzenia śledztwa przez organy ścigania, które ewidentnie tuszowały tą sprawę jak tylko mogły, poprzez m.in.:

* wydanie opinii medycznej przez organy ścigania bez powołania biegłego i wykonania jakiejkolwiek obdukcji – na podstawie artykułu z Wikipedii,

* nieprzeprowadzenie oględzin miejsca wydarzeń,

* nie przyjecie wniosku o ściganie, pomimo upływu 7 dni od wydarzeń i już na tym etapie trwałych obrażeń ciała,

* niepoprawne doręczenie decyzji o odmowie wszczęcia śledztwa przez skrytkę adresowa, uniemożliwiając jej terminowe zaskarżenie,

* kompletne zignorowanie ponownie złożonego zawiadomienia o popełnieniu przestępstwa, pomimo istotnej różnicy w tożsamości czynów i powołania kompletu nowych dowodów – w tym przede wszystkim dokumentacji medycznej, która bezsprzecznie potwierdza działanie osób trzecich, wynikiem którego jest trwałe uszkodzenie ciała, w tym ukł. moczowo-płciowego i nerwowego, oraz blizny pozostałe po ranach kłutych w okolicach pachwiny, oraz uwierzytelnionej komunikacji z tego okresu – z których jednoznacznie wynika iż do popełnienia przestępstwa doszło, określa krąg osób podejrzanych, oraz zaprzecza absurdalnej tezie wyrażonej w uzasadnieniu odmowy wszczęcia śledztwa wydanej na podstawie artykułu z Wikipedii, bez powołania lekarza biegłego i przeprowadzeniu obdukcji, jakoby przyczyną trwałych obrażeń ciała była kandydoza ukł. pokarmowego,

* ewidentne niezapoznanie się z dowodami załączonymi do zawiadomienia – Prokuratorzy Prokuratury Rejonowej Kraków-Krowodrza nie poradzili sobie nawet z właściwym zidentyfkowaniem spraw dla których wskazane były sygnatury a komplet dokumentacji procesowej dołączony do złożonego zawiadomienia,

* bagatelizowanie gróźb karalnych poprzedzających i spełnionych w wyniku wydarzeń stycznia 2014 r.

2\. Fakt wielokrotnego uniemożliwiania kompleksowego zdiagnozowania stanu zdrowia i uzupełnienia dowodów sprawy o opinie medyczno-sądowe, tym samym pogarszając stan zdrowia i powodując drastyczne pogorszenie jakości życia – ponad 20 lekarzy w Polsce oraz placówek wykonujących opinie medyczno-sądowe – prywatni lekarze, placówki wykonujące opinie na zlecenie prywatne wielokrotnie odmawiały mi wykonania niezbędnych badań i wydania opinii; badania w końcu w wykonałem poza Polska, m.in. w Wiedniu, Austrii;

3\. Fakt, iż trwałe uszkodzenie ciała dotyczy najbardziej intymnych części ciała (ukł. moczowo-płciowy) i było wykonane z premedytacją – wydarzenia były poprzedzone groźbami karalnymi wystosowanymi do mnie w komunikacji SMS kilka dni wcześniej; krakowskie organy ścigania uznały, iż te groźby karalne, profesjonalnie uwierzytelnione przez informatyka śledczego i spełnione w wyniku wydarzeń stycznia 2014, wynikiem których jest pozbawienie dorosłego, 35-letniego mężczyzny, możliwości współżycia seksualnego “nie stanowią czynu zabronionego”;

Takie same działania – tj. celowe tuszowanie przestepstwa kryminalnego, wielokrotne uniemożliwianie przeprowadzenia kompleksowych badań w tym wykonania opinii medyczno-sądowych w celu uzupełnienia dowodów, stawianie falszywych diagnoz, ukrywanie / bagatelizowanie gróźb karalnych poprzedzających wydarzenia stycznia 2014 r. – zostało już wielokrotnie rozpoznane w orzeczeniach Trybunału Praw Człowieka w Strasburgu jako bardzo istotna przesłanka i podstawa do wydania wyroku o naruszeniu Artykułu 3 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolnościa ("zakaz nieludzkiego i poniżającego traktowania"), ponadto jest naruszeniem Art. 2 ("prawo do życia"), Art. 6 ("prawo do rzetelnego procesu sądowego") oraz Art. 14 ("zakaz dyskryminacji") Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności.

Zapraszam Państwa aby we własnym zakresie zapoznać się z kompletem dokumentacji medycznej, pism procesowych, chronologią wydarzeń, i innymi opracowaniami, opublikowanymi w serwisie, który stworzyłem w celu nagłośnienia tej sprawy <https://not.surgery/> i gdzie kolejne informacje i dokumenty dot. tej sprawy będę publikował.

<hr class="thin-separator" />
