---
layout: home
permalink: "index.en.html"
ref: index
lang: en
image: /pliki/Prezentacja-201907/not-surgery_presentation.jpg
---

<div class="videoWrapper">
  <iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/AmwlRVHCslm4/"></iframe>
</div>

<div class="videoExtras">
  <strong>Download</strong>: <a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.mp4" | relative_path }}" target="_blank">MP4 version</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation.webm" | relative_path }}" target="_blank">WEBM version</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-slides.pdf" | relative_path }}" target="_blank">slides (PDF)</a>&nbsp;|&nbsp;<a href="{{ "/pliki/Prezentacja-201907/not-surgery_presentation-narration.pdf" | relative_path }}" target="_blank">narration (PDF)</a>
</div>

<br/>

<div style="text-transform: uppercase;">
<strong>Important</strong>: <a href="{{ "/other/2021/03/14/site-shutdown-by-GitHub.html" | relative_path }}" target="_blank">alternative ways to access this site due to recent shutdown by GitHub</a>
</div>

<br/>

<img src="/pliki/Inne/not-surgery_infographic-with-photos.png" title="Medical / scientific experimentation performed on me WITHOUT my consent" />

<br/>

I would like to draw your attention to a matter that has been going on for many years now and in which there have been numerous threads which, combined with each other and meticulously documented, give a picture of very serious violations of the most fundamental and most important human rights, as enshrined in the articles of the Polish Constitution and the European Convention on Human Rights, namely:
 * Article 2 of the European Convention on Human Rights (“the right to life”)
 * Article 39 of the Constitution of the Republic of Poland (“prohibition to perform medical procedures without consent”)
 * Article 3 of the European Convention on Human Rights and Article 40 of the Constitution of the Republic of Poland (“prohibition of inhuman and degrading treatment”)

The sole fact of the event, which the crime initially concerned - i.e. permanent bodily injury caused by an assault in a flat rented in Krakow, Poland - would not be interesting enough, because such situations happen unfortunately, but what has been going on since then for over 4 years now in order to document this matter properly in order to explain it and to enforce my rights, i.e.

1\. The fact of gross violations of procedural obligations and the lack of conducting an investigation by law enforcement authorities, who manifestly concealed the case as far as they could, through, inter alia:

 * issuing of medico-legal opinions by law enforcement authorities without appointing a medico-legal expert and performing a forensic examination of any kind - on the basis of an article from Wikipedia,

 * failure to carry out an inspection of the place of the events,

 * failure to accept motion for prosecution, despite the fact that 7 days have passed since the events and bodily injuries were still present,

 * incorrect delivery of the decision refusing to initiate an investigation by an address box, making it impossible to file a timely complaint,

 * complete disregard of the re-submitted notification of a crime, despite the significant difference in the identity of the facts and the establishment of a set of new evidence – particularly medical documentation, which undoubtedly confirms the actions of third parties, resulting in permanent bodily injury, including genitourinary and nervous system damage and scars left from stab wounds in the groin area, as well as certified communication from this period – from which it is clear that the crime was committed, defines the circle of suspects, and contradicts the absurd thesis expressed in the justification of the refusal to initiate an investigation issued on the basis of an article from Wikipedia, without appointing a medico-legal expert and performing a forensic examination of any kind, that the cause of permanent bodily injuries was candidiasis of the digestive system

 * evident failure to recognize the evidence attached to the notification - the Prosecutors of the District Prosecutor's Office of Kraków-Krowodrza did not even manage to properly identify the cases for which the signatures were indicated and the complete set of procedural documentation attached to the submitted notification,

 * ignoring the punishable threats that preceded and were fulfilled as a result of the January 2014 events


2\. The fact that I was repeatedly prevented from conducting comprehensive testing and diagnosis of my state of health and from supplementing the evidence of the case with medico-legal opinions, thereby worsening my state of health and causing a drastic deterioration in the quality of life - more than 20 doctors in Poland and institutions performing medico-legal opinions - private doctors, institutions performing opinions on private orders, repeatedly refused to carry out the necessary examinations and to issue opinions; finally, comprehensive testing and diagnosis was carried out outside Poland, mainly in Vienna, Austria;

3\. The fact that permanent bodily injury concerns the most intimate parts of the body (genitourinary) and was deliberately carried out - the events were preceded by criminal threats sent to me in SMS communication a few days earlier; the Krakow law enforcement authorities decided that these criminal threats, professionally authenticated by a forensic IT expert, and fulfilled as a result of the events of January 2014, which resulted in depriving an adult, 35-year-old man of the possibility of sexual intercourse "do not constitute a criminal offence";

The same actions - i.e. deliberate cover-up of a criminal offense, repeatedly preventing comprehensive testing and diagnosis including the execution of medico-legal opinions in order to supplement the evidence,  making false diagnoses, concealing / ignoring punishable threats which preceded the events of January 2014 - has been repeatedly recognized in the judgments of the Human Rights Tribunal in Strasbourg as a very important premise and basis for issuing a judgment on the violation of Article 3 of the European Convention on Human Rights ("prohibition of inhuman and degrading treatment"), moreover it is also a violation of Article 2 ("the right to life"), Article 6 ("the right to a fair trial") and Article 14 ("non-discrimination") of the European Convention on Human Rights.

I invite you to read on your own the complete set of medical documentation, pleadings, chronology of events, and other articles published on the website I created in order to publicize this matter <https://not.surgery/> and where further information and documents concerning this case will be published.

<hr class="thin-separator" />
