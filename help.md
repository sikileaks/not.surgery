---
title: How to help
layout: page
permalink: "/help/index.html"
ref: help
lang: en
sortkey: 3
---

1. Share information published on this site

2. Donate for medical and legal costs

   a) cryptocurrencies – send to one of the following:

    * **Ethereum (ETH)**: 0x54da7C20Cf6271B94d731A7A10e4c6fBFc8d264d

    * **Bitcoin (BTC)**: 39RG9qBj9AKK7pgquX2GEorEMtKF1G23Qu

    * **Ripple (XRP)**: rPoPpyLJxo5pSY4jGPhG4Fi3qaUkdpgRYu

    * **Bitcoin Cash (BCH)**: qrg5fkczzy84hmcqx28yucz7qxugdy2vcygkn308gl

    * **Bitcoin Gold (BTG)**: AbCRaUvc6WokN13d5nfxAuRouKPrjV8fzU

    * **Dogecoin (DOGE)**: DFQz2wAieUUYmaZNuW2MKaN1t2csUrjS1Y

    * **Litecoin (LTC)**: MH2KkBN86vQBLKfEuKbW4QRVewSGKyw21W

    * **Dash (DASH)**: XpVXHcQ7gAoFR4XJb3i5WXbGGHcsYZgqWo

   b) PayPal – send to <strong>contact@not.surgery</strong>

3. Support my Indiegogo fundraising campaign to write and publish book about this dark period – <a href="https://www.indiegogo.com/projects/state-terrorism-in-the-21st-century/" target="_blank">"State terrorism in the 21st century: How mass surveillance, sold as means to fight terrorism, is being used to terrorize populations and implement “invisible” totalitarianism"</a>

4. Support me via Patreon: <a href="https://www.patreon.com/not_surgery" target="_blank">https://www.patreon.com/not_surgery</a>

5. Order IT services and/or products I offer directly via my OpenBazaar store: <a href="https://openbazaar.into.software/" target="_blank">https://openbazaar.into.software/</a> (Peer ID: QmWJrydWcsYx9gHnAwCDWC8PPTHpHCzeowdasdr7mHX9Nn) – for more information about my IT services & products, visit <a href="https://ideas.into.software/" target="_blank">https://ideas.into.software/</a>
