---
title: Contact
layout: page
permalink: "/contact/index.html"
ref: contact
lang: en
sortkey: 5
---

**Email**: <a href="mailto:contact@not.surgery" title="contact@not.surgery">contact@not.surgery</a><br/>

**Mailing address**: <br/>
 &nbsp;Michal Siemaszko<br/>
 &nbsp;ul. Krolewska 51<br/>
 &nbsp;30-081 Krakow
