---
title: Kontakt
layout: page
permalink: "/kontakt/index.html"
ref: contact
lang: pl
sortkey: 5
---

**Email**: <a href="mailto:contact@not.surgery" title="contact@not.surgery">contact@not.surgery</a><br/>

**Adres korespondencyjny**: <br/>
 &nbsp;Michał Siemaszko<br/>
 &nbsp;ul. Królewska 51<br/>
 &nbsp;30-081 Kraków
