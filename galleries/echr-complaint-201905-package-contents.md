---
layout: gallery
title: ECHR complaint (2019/05) – package contents
permalink: "/echr-complaint-201905-package-contents/index.html"
ref: echr-complaint-201905-package-contents
exclude: true
image: /assets/galleries/echr-complaint-201905-package-contents/20190521_131720.thumb.png
support: [jquery, gallery]
lang: en
---

Contents of priority mail package mailed to ECHR on May 21st 2019 – all required documentation meticulously marked and included in package. Each image contains original EXIF metadata.

{% include gallery-layout.html gallery=site.data.galleries.echr-201905 %}
