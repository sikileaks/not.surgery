---
layout: gallery
title: Skarga do ETPC (2019/05) – zawartość przesyłki
permalink: "/etpc-skarga-201905-zawartosc-przesylki/index.html"
ref: echr-complaint-201905-package-contents
exclude: true
image: /assets/galleries/echr-complaint-201905-package-contents/20190521_131720.thumb.png
support: [jquery, gallery]
lang: pl
---

Zawartość paczki priorytetowej, przesłanej do ETPC w dniu 21 maja 2019 r. – całość wymaganej dokumentacji została dokładnie oznaczona i zawarta w paczce. Każde ze zdjęć zawiera oryginalne metadane EXIF.

{% include gallery-layout.html gallery=site.data.galleries.echr-201905 %}
