---
title: Chronologia wydarzeń
layout: page
permalink: "/chronologia/index.html"
ref: chronology
lang: pl
sortkey: 1
---

Chronologia wydarzeń od 2013 roku do dnia dzisiejszego.

{:#toc}
- {:.year-element} [2019](#2019)
  * {:.date-element} [21.05.2019 (21 maj 2019 r.)](#21052019-21-maj-2019-r)
  * {:.date-element} [06.05.2019 (6 maj 2019 r.)](#06052019-6-maj-2019-r)
  * {:.date-element} [20.02.2019 (20 luty 2019 r.)](#20022019-20-luty-2019-r)
  * {:.date-element} [06.02.2019 (6 luty 2019 r.)](#06022019-6-luty-2019-r)
  * {:.date-element} [04.02.2019 (4 luty 2019 r.)](#04022019-4-luty-2019-r)
  * {:.date-element} [03.01.2019 (3 styczeń 2019 r.)](#03012019-3-styczeń-2019-r)
  * {:.date-element} [02.01.2019 (2 styczeń 2019 r.)](#02012019-2-styczeń-2019-r)
- {:.year-element} [2018](#2018)
  * {:.date-element} [27.12.2018 (27 grudzień 2018 r.)](#27122018-27-grudzień-2018-r)
  * {:.date-element} [11.12.2018 (11 grudzień 2018 r.)](#11122018-11-grudzień-2018-r)
  * {:.date-element} [3.12.2018 (3 grudzień 2018 r.)](#3122018-3-grudzień-2018-r)
  * {:.date-element} [13.08.2018 (13 sierpień 2018 r.)](#13082018-13-sierpień-2018-r)
  * {:.date-element} [19.07.2018 (19 lipiec 2018 r.)](#19072018-19-lipiec-2018-r)
  * {:.date-element} [12.07.2018 (12 lipiec 2018 r.)](#12072018-12-lipiec-2018-r)
  * {:.date-element} [13.06.2018 (13 czerwca 2018 r.)](#13062018-13-czerwca-2018-r)
  * {:.date-element} [5.06.2018 (5 czerwiec 2018 r.)](#5062018-5-czerwiec-2018-r)
  * {:.date-element} [20.04.2018 (20 kwiecień 2018 r.)](#20042018-20-kwiecień-2018-r)
  * {:.date-element} [17.04.2018 (17 kwiecień 2018 r.)](#17042018-17-kwiecień-2018-r)
  * {:.date-element} [13.04.2018 (13 kwiecień 2018 r.)](#13042018-13-kwiecień-2018-r)
  * {:.date-element} [6.04.2018 (6 kwiecień 2018 r.)](#6042018-6-kwiecień-2018-r)
  * {:.date-element} [4.04.2018 (4 kwiecień 2018 r.)](#4042018-4-kwiecień-2018-r)
  * {:.date-element} [07.03.2018 (7 marzec 2018 r.)](#07032018-7-marzec-2018-r)
  * {:.date-element} [28.02.2018 – 16.03.2018 (28 luty 2018 do 16 marzec 2018 r.)](#28022018--16032018-28-luty-2018-do-16-marzec-2018-r)
  * {:.date-element} [19.02.2018 (19 luty 2018 r.)](#19022018-19-luty-2018-r)
  * {:.date-element} [15.02.2018 (15 luty 2018 r.)](#15022018-15-luty-2018-r)
  * {:.date-element} [13.02.2018 (13 luty 2018 r.)](#13022018-13-luty-2018-r)
  * {:.date-element} [16.01.2018 (16 styczeń 2018 r.)](#16012018-16-styczeń-2018-r)
  * {:.date-element} [15.01.2018 – 20.04.2018 (15 styczeń 2018 do 20 kwiecień 2018 r.)](#15012018--20042018-15-styczeń-2018-do-20-kwiecień-2018-r)
  * {:.date-element} [9.01.2018 (9 styczeń 2018 r.)](#9012018-9-styczeń-2018-r)
- {:.year-element} [2017](#2017)
  * {:.date-element} [17.11.2017 (17 listopad 2017 r.)](#17112017-17-listopad-2017-r)
  * {:.date-element} [30.10.2017 – 19.01.2018 (30 październik 2017 r. do 19 styczeń 2018 r.)](#30102017--19012018-30-październik-2017-r-do-19-styczeń-2018-r)
  * {:.date-element} [22.06.2017 – 16.01.2018 (22 czerwiec do 16 styczeń 2018 r.)](#22062017--16012018-22-czerwiec-do-16-styczeń-2018-r)
  * {:.date-element} [17.06.2017 – 17.08.2017 (17 czerwiec do 17 sierpień 2017 r.)](#17062017--17082017-17-czerwiec-do-17-sierpień-2017-r)
  * {:.date-element} [22.03.2017 – 29.03.2017 (22 marzec 2017 do 29 marzec 2017 r.)](#22032017--29032017-22-marzec-2017-do-29-marzec-2017-r)
- {:.year-element} [2016](#2016)
  * {:.date-element} [16.01.2016 – 29.06.2016 (16 styczeń do 29 czerwiec 2016 r.)](#16012016--29062016-16-styczeń-do-29-czerwiec-2016-r)
  * {:.date-element} [05.01.2016 – 15.01.2016 (5 styczeń 2016 do 15 styczeń 2016 r.)](#05012016--15012016-5-styczeń-2016-do-15-styczeń-2016-r)
- {:.year-element} [2015](#2015)
  * {:.date-element} [13.12.2015 – 14.12.2015 (13 grudzień do 14 grudzień 2015 r.)](#13122015--14122015-13-grudzień-do-14-grudzień-2015-r)
  * {:.date-element} [4.11.2015 – 11.11.2015 (4 listopad 2015 do 11 listopad 2015 r.)](#4112015--11112015-4-listopad-2015-do-11-listopad-2015-r)
  * {:.date-element} [17.09.2015 – 08.10.2015 (17 wrzesień 2015 do 8 październik 2015 r.)](#17092015--08102015-17-wrzesień-2015-do-8-październik-2015-r)
  * {:.date-element} [20.06.2015 – 1.12.2015 (20 czerwiec 2015 do 1 grudnia 2015 r.)](#20062015--1122015-20-czerwiec-2015-do-1-grudnia-2015-r)
- {:.year-element} [2014](#2014)
  * {:.date-element} [12.12.2014 (12 grudzień 2014 r.)](#12122014-12-grudzień-2014-r)
  * {:.date-element} [7.11.2014 (7 listopad 2014 r.)](#7112014-7-listopad-2014-r)
  * {:.date-element} [05.11.2014 (5 listopad 2014 r.)](#05112014-5-listopad-2014-r)
  * {:.date-element} [14.04.2014 (14 kwiecień 2014 r.)](#14042014-14-kwiecień-2014-r)
  * {:.date-element} [21.03.2014 (21 marzec 2014 r.)](#21032014-21-marzec-2014-r)
  * {:.date-element} [15.03.2014 (15 marzec 2014 r.)](#15032014-15-marzec-2014-r)
  * {:.date-element} [04.03.2014 – 30.03.2014 (4 marzec 2014 do 30 marzec 2014 r.)](#04032014--30032014-4-marzec-2014-do-30-marzec-2014-r)
  * {:.date-element} [03.01.2014 (1 marzec 2014 r.)](#03012014-1-marzec-2014-r)
  * {:.date-element} [28.02.2014 (28 luty 2014 r.)](#28022014-28-luty-2014-r)
  * {:.date-element} [11.02.2014 (11 luty 2014 r.)](#11022014-11-luty-2014-r)
  * {:.date-element} [29.01.2014 (29 styczeń 2014 r.)](#29012014-29-styczeń-2014-r)
  * {:.date-element} [22.01.2014 (22 styczeń 2014 r.)](#22012014-22-styczeń-2014-r)
  * {:.date-element} [16.01.2014 (16 styczeń 2014 r.)](#16012014-16-styczeń-2014-r)
  * {:.date-element} [9.01.2014 (9 styczeń 2014 r.)](#9012014-9-styczeń-2014-r)
- {:.year-element} [2013](#2013)
  * {:.date-element} [28.12.2013 (28 grudzień 2013 r.)](#28122013-28-grudzień-2013-r)
  * {:.date-element} [18.12.2013 – 14.01.2014 (18 grudzień 2013 r. do 14 styczeń 2014 r.)](#18122013--14012014-18-grudzień-2013-r-do-14-styczeń-2014-r)
  * {:.date-element} [16.12.2013 – 18.12.2013 (16 grudzień 2013 r. do 18 grudzień 2013 r.)](#16122013--18122013-16-grudzień-2013-r-do-18-grudzień-2013-r)
  * {:.date-element} [12.12.2013 (12 grudzień 2013 r.)](#12122013-12-grudzień-2013-r)
  * {:.date-element} [13.11.2013 (13 listopad 2013 r.)](#13112013-13-listopad-2013-r)
  * {:.date-element} [6.10.2013 (6 październik 2013 r.)](#6102013-6-październik-2013-r)
  * {:.date-element} [19.09.2013 (19 wrzesień 2013 r.)](#19092013-19-wrzesień-2013-r)
  * {:.date-element} [08.2013 (sierpień 2013 r.)](#082013-sierpień-2013-r)

<hr class="thick-separator" />

## 2019

### 21.05.2019 (21 maj 2019 r.)
> W dniu 21 maja 2019 r. złożyłem skargę do Europejskiego Trybunału Praw Człowieka.
>
> Formularz zgłoszeniowy: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG.20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG.20190521.pdf</a>
>
> Załącznik do formularza zgłoszeniowego, sekcji E: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_Statement-of-the-facts_Annex.20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_Statement-of-the-facts_Annex.20190521.pdf</a>
>
> Załącznik do formularza zgloszeniowego, sekcji I: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_List-of-accompanying-documents_Annex.20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_List-of-accompanying-documents_Annex.20190521.pdf</a>
>
> Potwierdzenie nadania przesyłki: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-postage_20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-postage_20190521.pdf</a>
>
> Potwierdzenie transmisji faksu: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-fax-transmission_20190522.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-fax-transmission_20190522.pdf</a>
>
> Kopia wiadomości email: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Email_20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Email_20190521.pdf</a>
>
> Potwierdzenie odbioru przesyłki: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-delivery_20190603.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-delivery_20190603.pdf</a>
>
> Komplet zdjęć (wraz z oryginalnymi metadanymi EXIF) dokumentujących zawartość przesyłki: <a href="/etpc-skarga-201905-zawartosc-przesylki/" target="_blank">/etpc-skarga-201905-zawartosc-przesylki/</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 06.05.2019 (6 maj 2019 r.)
> Kolejne absurdalne pisma, które otrzymałem w ostatnich kilku miesiącach od organów procesowych, poskutkowały złożeniem kolejnych 3 zawiadomień o popełnieniu przestępstwa.
>
> “Zawiadomienie o prawdopodobieństwu popełnienia przestępstwa z art. 270 § 1 oraz art. 276 Kodeksu Karnego”, z dnia 6 maja 2019 r.: <a href="/pliki/Prawne/falszowanie-nieprawda/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_falszowanie-nieprawda.pdf" target="_blank">/pliki/Prawne/falszowanie-nieprawda/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_falszowanie-nieprawda.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Prądnik Biały Dorotę Bojanowską”, z dnia 6 maja 2019 r.: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_db.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_db.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Okręgowej w Krakowie Rafała Babińskiego”, z dnia 6 maja 2019 r.: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rb.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rb.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 20.02.2019 (20 luty 2019 r.)
> W dniu 20 lutego 2019 r. złożyłem osobiście w Małopolskim Wydziale Departamentu do Spraw Przestępczości Zorganizowanej i Korupcji wniosek o podjęcie interwencji.
>
> “Wniosek o podjęcie interwencji” skierowany do Departamentu do Spraw Przestępczości Zorganizowanej i Korupcji dnia 20 lutego 2019 r.: <a href="/pliki/Prawne/DdSPZiK/II-Kp-9-19-K_zgloszenie-do-DdSPZiK.20190220.pdf" target="_blank">/pliki/Prawne/DdSPZiK/II-Kp-9-19-K_zgloszenie-do-DdSPZiK.20190220.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 06.02.2019 (6 luty 2019 r.)
> W dniu 6 lutego 2019 r. złożyłem zawiadomienie o popełnieniu przestępstwa przez p. Monikę Hudykę.
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 156 §1, art. 158 §, art. 159, art. 193, art. 233 § 1, art. 234, art. 236 § 1, art. 238, art. 239 § 1, art. 258 § 1 Kodeksu Karnego przez p. Monikę Hudykę”, z dnia 6 lutego 2019 r.: <a href="/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf</a>
>
> Więcej informacji: <a href="/prawne/2019/02/06/hudyka-zawiadomienie-o-popelnieniu-przestepstwa.html" target="_blank">/prawne/2019/02/06/hudyka-zawiadomienie-o-popelnieniu-przestepstwa.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />


### 04.02.2019 (4 luty 2019 r.)
> Kolejne 4 zawiadomienia z art. 231 §1 k.k. przeciwko prokuratorom i policji w sprawie połączonej zostały złożone.
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Prądnik Biały Tomasza Pawlika”, z dnia 4 lutego 2019 r.: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez asp. Komisariatu III Policji w Krakowie Roberta Cygana”, z dnia 4 lutego 2019 r.: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez st. sierż. Komisariatu III Policji w Krakowie Monikę Górowską”, z dnia 4 lutego 2019 r.: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Prądnik Biały Beatę Sichelską”, z dnia 4 lutego 2019 r.: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf</a>  
>
> Więcej informacji: <a href="/prawne/2019/02/04/sciganie-karne-prokuratorow-i-policji-polaczone.html" target="_blank">/prawne/2019/02/04/sciganie-karne-prokuratorow-i-policji-polaczone.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 03.01.2019 (3 styczeń 2019 r.)
> Pierwszy z pozwów o zaniedbanie medyczne, złożony w połowie sierpnia 2018 r. w Sądzie w Nowym Delhi, Indiach, został pomyślnie rozstrzygnięty, zaraz przed terminem złożenia w Sądzie odpowiedzi na sprzeciw.
>
> Odpowiedz na sprzeciw: <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf" target="_blank">/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf</a>
>
> Index: <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.index.pdf" target="_blank">/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.index.pdf</a>
>
> Więcej informacji: <a href="/medyczne/2019/01/03/pierwszy-pozew-o-zaniedbanie-medyczne-rozstrzygniety.html" target="_blank">/medyczne/2019/01/03/pierwszy-pozew-o-zaniedbanie-medyczne-rozstrzygniety.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 02.01.2019 (2 styczeń 2019 r.)
> Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez funkcjonariusza Policji zostało złożone.
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 § 1 Kodeksu Karnego przez st. post. Komisariatu IV Policji w Krakowie Darię Curzydło”, z dnia 2 stycznia 2019 r.: <a href="/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf" target="_blank">/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf</a>
>
> Więcej informacji: <a href="/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html" target="_blank">/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thick-separator" />

## 2018
### 27.12.2018 (27 grudzień 2018 r.)
> Dnia 27 grudnia 2018 r., za pośrednictwem Prokuratury Rejonowej w Tarnowie do Sądu zostało skierowane zażalenie na postanowienie o odmowie wszczęcia śledztwa w sprawie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez 4 krakowskich prokuratorów.
>
> “Zażalenie na postanowienie Prokuratora Rejonowego w Tarnowie z dnia 22 listopada 2018 r., wydane w sprawie o sygn. akt. PR 4 DS360.2018 w przedmiocie odmowy wszczęcia śledztwa, doręczone w dniu 20 grudnia 2018 r.”, z dnia 27 grudnia 2018 r.: <a href="/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf</a>
>
> Więcej informacji: <a href="/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html" target="_blank">/legal/2019/01/04/sciganie-karne-prokuratorow-i-policji.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 11.12.2018 (11 grudzień 2018 r.)
> W związku z błędnym uznaniem charakteru procesowego złożonego przeze mnie dnia 9 stycznia 2018 r. zawiadomienia o popełnieniu przestępstwa za wniosek o wszczęcie śledztwa w sprawie sygn. akt 2 Ds. 385/14–pomimo tego, iż czyny wskazane nie były przedmiotem zainteresowania i prawnokarnego odniesienia w postępowaniu z zawiadomienia złożonego 28.02.2014–oraz nowych i istotnych dowodów i wątków, dnia 11 grudnia 2018 r. zostało złożone nowe zawiadomienie o popełnieniu przestępstwa.
>
> Wraz z zawiadomieniem złożyłem również kolejny wniosek o interwencje do Prokuratora Generalnego.
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 156 §1, art. 157 §1, art. 160 §1, art. 162 §1, art. 192 §1, art. 193, art. 239, art. 258 oraz art. 268 Kodeksu Karnego, równocześnie naruszenie art. 30, art. 32, art. 38, art. 39, art. 40, art. 45, art. 47 oraz art. 77 Konstytucji RP, w zw. z art. 44 Konstytucji RP, jak również art. 2, art. 3, art. 6, art. 13 oraz art. 14 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności, w zw. z art. 9 Konstytucji RP” z dnia 11 grudnia 2018 r.: <a href="/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf" target="_blank">/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf</a>
>
> ”Wniosek o podjęcie interwencji” skierowany do Ministra Sprawiedliwości/Prokuratora Generalnego RP dnia 11 grudnia 2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20181211.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20181211.pdf</a>
>
> Więcej informacji: <a href="/prawne/2018/12/11/nowe-zawiadomienie-o-popelnieniu-przestepstwa.html" target="_blank">/prawne/2018/12/11/nowe-zawiadomienie-o-popelnieniu-przestepstwa.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 3.12.2018 (3 grudzień 2018 r.)
> Drugi pozew o zaniedbanie medyczne złożony.
>
> Wersja w j. angielskim: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf</a>
>
> Wersja w j. czeskim: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf</a>
>
> Więcej informacji: <a href="/medyczne/2018/12/03/drugi-pozew-o-zaniedbanie-medyczne.html" target="_blank">/medyczne/2018/12/03/drugi-pozew-o-zaniedbanie-medyczne.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 13.08.2018 (13 sierpień 2018 r.)
> Pierwszy z pozwów o zaniedbanie medyczne złożony.
>
> Pozew: <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf" target="_blank">/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf</a>
>
> Index: <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Index.EN.20180730.pdf" target="_blank">/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Index.EN.20180730.pdf</a>
>
> Więcej informacji: <a href="/medyczne/2018/08/20/pierwszy-pozew-o-zaniedbanie-medyczne.html" target="_blank">/medyczne/2018/08/20/pierwszy-pozew-o-zaniedbanie-medyczne.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 19.07.2018 (19 lipiec 2018 r.)
> Sprawą rażących naruszeń przepisów w postępowaniach prowadzonych w związku z wydarzeniami stycznia 2014 r. zainteresował się Zespół Prawa Karnego Biura Rzecznika Praw Obywatelskich, zwracając się do Prokuratora Regionalnego w Krakowie z prośba o zbadanie akt sprawy pod kątem oceny ich prawidłowości.
>
> "Wystąpienie Zespołu Prawa Karnego Biura Rzecznika Praw Obywatelskich do Prokuratora Regionalnego w Krakowie, z dnia 19 lipca 2018 r.": <a href="/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf" target="_blank">/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf</a>
>
> Więcej informacji: <a href="/prawne/2019/01/05/wystapienie-zespolu-prawa-karnego-rpo.html" target="_blank">/prawne/2019/01/05/wystapienie-zespolu-prawa-karnego-rpo.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 12.07.2018 (12 lipiec 2018 r.)
> Przeciwko 4 krakowskim prokuratorom zostały złożone zawiadomienia o popełnieniu przestępstwa z art. 231 Kodeksu Karnego.
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Okręgowej w Krakowie Marię Zębalę”, z dnia 12 lipca 2018 r.: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Krowodrza w Krakowie Małgorzatę Lipską”, z dnia 12 lipca 2018 r.: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Krowodrza w Krakowie Edytę Kulik”, z dnia 12 lipca 2018 r.: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf</a>
>
> “Zawiadomienie o popełnieniu przestępstwa z art. 231 Kodeksu Karnego przez prokuratora Prokuratury Rejonowej Kraków-Krowodrza w Krakowie Bartłomieja Legutko”, z dnia 12 lipca 2018 r.: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 13.06.2018 (13 czerwca 2018 r.)
> Dnia 13 czerwca 2018 r. złożyłem osobiście w Warszawie–w Prokuraturze Krajowej oraz Biurze Rzecznika Praw Obywatelskich–wnioski o podjecie interwencji.
>
> “Wniosek o podjęcie interwencji w sprawie bezprawnych działań i zaniechań przez Prokuraturę Okręgową w Krakowie i Prokuraturę Rejonową Kraków-Krowodrza w Krakowie” skierowany do Rzecznika Praw Obywatelskich dnia 13 czerwca 2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-rpo_20180613.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_wniosek-do-rpo_20180613.pdf</a>
>
> “Wniosek o interwencję w sprawie bezprawnych działań i zaniechań przez Prokuraturę Okręgową w Krakowie i Prokuraturę Rejonową Kraków-Krowodrza w Krakowie” skierowany do Ministra Sprawiedliwości/Prokuratora Generalnego RP dnia 13 czerwca 2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20180613.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20180613.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 5.06.2018 (5 czerwiec 2018 r.)
> Pomimo upływu blisko 5 miesięcy od złożenia zawiadomienia dnia 9.01.2018 r., żadne z czynności wymaganych przez k.p.k i obowiązujące rozporządzenia Ministerstwa Sprawiedliwości w następstwie otrzymania zawiadomienia o popełnieniu przestępstwa nie zostały wykonane – tj. żadne z pism dotychczas otrzymanych w tej sprawie nie jest postanowieniem o wszczęciu lub odmowie wszczęcia postępowania – w związku z czym złożyłem skargę na przewlekłość postępowania przygotowawczego do Sądu Rejonowego Kraków-Krowodrza w Krakowie. Rozpoznanie skargi jest w toku.
>
> Kopia pisma “Skarga na naruszenie prawa strony do rozpoznania sprawy w postępowaniu karnym przygotowawczym bez nieuzasadnionej zwłoki” z dnia 5.06.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_skarga-na-przewleklosc-postepowania-karnego-przygotowawczego_20180605.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_skarga-na-przewleklosc-postepowania-karnego-przygotowawczego_20180605.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 20.04.2018 (20 kwiecień 2018 r.)
> Wyjazd do Wiednia, Austrii, w celu przeprowadzenia dodatkowej konsultacji z lekarzem specjalistą neurologii i chirurgi plastycznej oraz odebrania oryginałów wyników przeprowadzonych badań i konsultacji.
>
> Kopia orzeczenia lekarskiego lekarza specjalisty neurologii i chirurgii plastycznej z konsultacji i badań wykonanych 6 i 20 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. nerwowego, wykonanych 6 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 17.04.2018 (17 kwiecień 2018 r.)
> Złożyłem w Prokuraturze Okręgowej w Krakowie, w oparciu o dodatkowe argumenty – tj. kontynuacje tego samego aroganckiego, zbywającego zachowania, przekraczania obowiązków, postępowania wbrew obowiązującym przepisom i rozporządzeniom – wniosek o wyłączenie Prokuratury Rejonowej Kraków-Krowodrza w Krakowie.
>
> Ja blisko 4 lata czasu uzupełniałem dowody i pomimo ewidentnego tuszowania sprawy przez krakowskie organy ścigania – jest to oczywiste na tym etapie – komplet dowodów, w tym dokumentacji medycznej, został jednak zebrany. Swoje zadanie domowe – dot. obowiązujących przepisów i rozporządzeń, włącznie z konstytucją RP, Konwencją Praw Człowieka oraz wiążącymi polskie organy ścigania orzeczeniami sadów międzynarodowych – również odrobiłem.
>
> Kopia pisma “Wniosek o wyznaczenie do prowadzenia postępowania przez jednostkę spoza obszaru właściwości Prokuratury Rejonowej Kraków-Krowodrza w Krakowie ..” z dnia 17.04.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie.20180417.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie.20180417.zlozone.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 13.04.2018 (13 kwiecień 2018 r.)
> Prokuratura Rejonowa Kraków-Krowodrza w Krakowie wystosowała do mnie kolejne pismo w którym wymijająco udziela mi odpowiedzi na zadane pytania, błędnie twierdząc iż decyzja o odmowie wszczęcia śledztwa w 2014 r. została mi właściwie doręczona, w dalszym ciągu próbując zmieniać moje oświadczenie woli i charakter procesowy zawiadomienia złożonego przeze mnie 9 stycznia 2018 r.. Na tym etapie było już ewidentne iż po raz kolejny ta sama prokuratura chce mi uniemożliwić dochodzenie swoich praw, wystosowując do mnie wymijające / aroganckie pisma z których nic nie wynika, zamiast przejść do wykonywania swoich obowiązków.
>
> Tego samego dnia, złożyłem w Prokuraturze Okręgowej w Krakowie skargę na prokuratora Prokuratury Rejonowej Kraków-Krowodrza w Krakowie.
>
> Kopia pisma z Prokuratury Rejonowej Kraków-Krowodrza w Krakowie z dnia 13.04.2018 r.: <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180413.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180413.pdf</a>
>
> Kopia pisma “Skarga na prokuratora Edytę Kulik” z dnia 13.04.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_ek.20180413.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_ek.20180413.zlozone.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 6.04.2018 (6 kwiecień 2018 r.)
> Wyjazd do Wiednia, Austrii, w celu przeprowadzenia konsultacji z lekarzem specjalistą neurologii i chirurgi plastycznej. Lekarz wszystkie opisywane przeze mnie objawy wziął pod uwagę i na podstawie dostępnych wyników badań neurografii / rezonansu magnetycznego ukł. nerwowego oraz dodatkowych badań wykonanych na miejscu – w tym m.in. USG ukł. nerwowego – wydał orzeczenie, które spójnie łączy opisywane objawy z obserwacjami jego i wynikami przeprowadzonych badań. Podczas pobytu w Wiedniu wykonałem również badanie OCT (Optical Coherence Tomography) blizn po ranach kłutych w okolicy pachwiny.
>
> Kopia orzeczenia lekarskiego lekarza specjalisty neurologii i chirurgii plastycznej z konsultacji i badań wykonanych 6 i 20 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. nerwowego, wykonanych 6 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 4.04.2018 (4 kwiecień 2018 r.)
> Złożyłem w Prokuraturze Rejonowej Kraków-Krowodrza w Krakowie wniosek o udzielenie informacji dot. prowadzonego postępowania oraz poprawnego doręczenia postanowienia o odmowie wszczęcia śledztwa z 2014 r.
>
> Tego samego dnia w Prokuraturze Okręgowej złożyłem zażalenie na bezczynność prokuratora Prokuratury Rejonowej Kraków-Krowodrza, który zgodnie z obowiązującymi przepisami miał 6 tygodni na wydanie decyzji o wszczęciu lub odmowie wszczęcia śledztwa.
>
> Kopia pisma “Wniosek o udzielenie informacji” z dnia 4.04.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-udzielenie-informacji.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-udzielenie-informacji.20180404.zlozone.pdf</a>
>
> Kopia pisma “Wniosek o prawidłowe doręczenie postanowienia o odmowie wszczęcia śledztwa z dnia 14 kwietnia 2014 roku” z dnia 4.04.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-doreczenie.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-doreczenie.20180404.zlozone.pdf</a>
>
> Kopia pisma “Zażalenie na bezczynność Prokuratora” z dnia 4.04.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zazalenie-na-bezczynnosc.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zazalenie-na-bezczynnosc.20180404.zlozone.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 07.03.2018 (7 marzec 2018 r.)
> Prokuratura Rejonowa Kraków-Krowodrza w Krakowie wystosowała do mnie pismo z którego wynika że nie zapoznali się ani z treścią zawiadomienia ani kompletem dowodów załączonych do zawiadomienia, próbując zmienić moje oświadczenie woli dot. charakteru procesowego tego pisma, który wynika wprost z przepisów, ponieważ dotyczy zupełnie innych czynów, powołuje komplet nowych dowodów na poparcie zawartych twierdzeń, a zebrana dokumentacja medyczna jasno wskazuje iż decyzja o odmowie wszczęcia śledztwa w 2014 była bezzasadna, jednocześnie będąc bezprawnym przekroczeniem uprawnień organów ścigania.
>
> Dodatkowo, nie zostały przeprowadzone najprostsze czynności sprawdzające – prokuratorzy nie poradzili sobie nawet z odnalezieniem spraw wymienionych we wniosku, pomimo wskazania ich sygnatur i załączeniem dokumentacji dot. tych spraw do zawiadomienia, włącznie z postanowieniami gdzie jasno widniała nazwa prokuratury, itd.
>
> Kopia pisma z Prokuratury Rejonowej Kraków-Krowodrza z dnia 07.03.2018 r.: <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180307.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180307.pdf</a>
>
> Kopia odpowiedzi na pismo z Prokuratury Rejonowej Kraków-Krowodrza z dnia 03.04.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180404.zlozone.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 28.02.2018 – 16.03.2018 (28 luty 2018 do 16 marzec 2018 r.)
> Wyjazd do Indii w celu wykonania badań wysokiej rozdzielczości rezonansu magnetycznego ukł. nerwowego (neurografii) i ukł. moczowego oraz wysokiej rozdzielczości tomografii komputerowej ukł. moczowego.
>
> Wynik badania neurografii / rezonansu magnetycznego ukł. nerwowego jasno wskazuje uszkodzenie nerwowe m.in. w miejscu gdzie do dzisiejszego dnia widoczna jest blizna oraz nerwów ukł. płciowego.
>
> Kopia wyników badań rezonansu magnetycznego ukł. nerwowego, wykonanych 12 marca 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 19.02.2018 (19 luty 2018 r.)
> Przekazanie zawiadomienia z Prokuratury Okręgowej w Krakowie do Prokuratury Rejonowej Kraków-Krowodrza w Krakowie.
>
> Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 19.02.2018 r.: <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180219.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180219.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 15.02.2018 (15 luty 2018 r.)
> Prokuratura Okręgowa w Krakowie oddaliła mój wniosek o wyłączenie prokuratorów Prokuratury Rejonowej Kraków-Krowodrza w Krakowie, pomimo rażącego przekroczenia obowiązków jakiego dopuścili się w sprawie z zawiadomienia z lutego 2014 r. i sprawach zgłaszanych od tego czasu z mojego zawiadomienia, m.in. dot. przywłaszczenia mojego mienia i gróźb karalnych wystosowanych do mnie i spełnionych w wyniku wydarzeń stycznia 2014 r..
>
> Kopia pisma z Prokuratury Okręgowej w Krakowie z dnia 15.02.2018 r.: <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180215.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180215.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 13.02.2018 (13 luty 2018 r.)
> Wyjazd do Wiednia, Austrii, w celu wykonania badan diagnostyki obrazowej. Podczas tego wyjazdu zostało wykonane badanie tomografii komputerowej, które jasno stwierdziło widoczność blizny i uszkodzeń wewnętrznych, poprawnie skorelowanych z występującymi objawami.
>
> Kopia wyników badań tomografii komputerowej, wykonanych 13 lutego 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf</a><br/>
> * Wersja w j. niemieckim, oryginał: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 16.01.2018 (16 styczeń 2018 r.)
> Przekazanie zawiadomienia z Departamentu Postępowania Przygotowawczego Prokuratury Krajowej do Prokuratury Okręgowej w Krakowie.
>
> Kopia pisma z Prokuratury Krajowej z dnia 16.01.2018 r.: <a href="/pliki/Prawne/Odpowiedzi/PK-II_Ko2_128.2018_20180116.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/PK-II_Ko2_128.2018_20180116.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 15.01.2018 – 20.04.2018 (15 styczeń 2018 do 20 kwiecień 2018 r.)
> Wykonałem kompleksowe badania – włącznie z diagnostyką obrazową oraz konsultacjami ze specjalistami medycyny nuklearnej, radiologi, neurologii i chirurgii plastycznej – w celu uzupełnienia dokumentacji medycznej.
>
> Z racji wielokrotnego wydawania błędnych diagnoz przez lekarzy w Polsce oraz odmawiania wykonania kompleksowych badań i opinii medyczno-sądowych na prywatne zlecenie jeszcze kilka miesięcy wcześniej przez blisko 20 placówek w Polsce, badania i konsultacje musiały zostać wykonane poza Polską. Etap rozsyłania zapytań do wytypowanych placówek, sprawdzania kompletności otrzymanej oferty, organizacji transportu i noclegu, przeprowadzenia badań, oraz tłumaczenia wyników badań.
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 9.01.2018 (9 styczeń 2018 r.)
> Złożyłem w Prokuraturze Krajowej w Warszawie zawiadomienie o popełnieniu przestępstwa wraz z wnioskiem o wyłączenie Prokuratury Rejonowej w Krakowie, która dotychczas zajmowała się sprawami składanymi przeze mnie i w każdej jednej sprawie kompletnie nic nie zostało zrobione – czy to w sprawie wydarzeń wynikiem których doznałem trwałego uszkodzenia ciała, czy w sprawach dot. przywłaszczenia mojego mienia.
>
> Zawiadomienie o popełnieniu przestępstwa, z racji trwałego uszkodzenia ciała będącego wynikiem wydarzeń stycznia 2014, wskazuje zupełnie inne czyny, powołuje komplet nowych dowodów wraz z dokumentacją medyczną, z której jasno wynika, iż odmówienie wszczęcia śledztwa w 2014 było nie tylko bezzasadne ale rażącym przekroczeniem uprawnień organów ścigania. Organy ścigania wówczas bez powołania biegłego i przeprowadzenia obdukcji wydały “opinie medyczną” iż kandydoza jest przyczyną trwałego uszkodzenia ciała, nie doręczyły mi poprawnie decyzji o odmowie wszczęcia śledztwa zamykając tym samym drogę sądową, nie przyjęły wniosku o ściganie pomimo upływu wielokrotności 7 dni.
>
> Kopia pisma “Zawiadomienie o popełnieniu przestępstwa z art. 156 §1, art. 157 §1, art. 160 §1, art. 162 §1, art. 192 §1 oraz art. 193 Kodeksu karnego, równocześnie naruszenie art. 2 („prawo do życia”), art. 3 („zakaz nieludzkiego lub poniżającego traktowania”), art. 6 („prawo do rzetelnego procesu sądowego”) oraz art. 14 („zakaz dyskryminacji”) Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności” z dnia 9 stycznia 2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_zawiadomienie_20180109.zlozone.pdf " target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_zawiadomienie_20180109.zlozone.pdf</a>
>
> Kopia pisma “Wniosek o wyłączenie prokuratorów Prokuratury Rejonowej w Krakowie” z dnia 9.01.2018 r.: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_wniosek-o-wylaczenie-prokuratorow.20180109.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_wniosek-o-wylaczenie-prokuratorow.20180109.zlozone.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thick-separator" />

## 2017
### 17.11.2017 (17 listopad 2017 r.)
> Spotkałem się z lekarzem medycyny prowadzącym leczenie kandydozy w 2013 r. Lekarz przeprowadził obdukcje, zapoznał się z do tej pory zebranymi wynikami badań, kategorycznie zaprzeczył twierdzeniom iż zaobserwowane przez niego obrażenia ciała mogły być spowodowane przez kandydozę czy lekarstwa przyjmowane w trakcie kuracji.
>
> Kopia zaświadczenia, że kandydoza nie jest przyczyna zaobserwowanych obrażeń ciała i uszkodzenia ukł. moczowo-płciowego, wystawione przez lek. medycyny prowadzącego kuracje na kandydozę w 2013 r.: <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 30.10.2017 – 19.01.2018 (30 październik 2017 r. do 19 styczeń 2018 r.)
> W okresie od 30 października 2017 r. do 19 stycznia 2017 r. chciałem nawiązać kontakt z organizacjami pozarządowymi (NGOs) w Polsce i zagranica–jedyne o co prosiłem, i co doskonale mieści się w statutach tych organizacji, to polecenie biegłych medycyny sadowej oraz adwokata wyspecjalizowanego w sprawach dot. poważnych naruszeń praw człowieka.
>
> Ani kontakt osobisty–w listopadzie 2017 r., kiedy w Londynie i Brukseli odwiedziłem siedziby tych organizacji–ani kontakt mailowy nie przyniósł kompletnie żadnych rezultatów.
>
> Bardzo podobnie jak w przypadku kontaktu z placówkami medycznymi w Polsce, z którymi kilka miesięcy wcześniej kontaktowałem się w celu otrzymania oferty wykonania opinii medyczno-sądowych i badan uzupełniających, w 90% przypadków nie otrzymałem żadnej odpowiedzi, a w pozostałych 10% w 90% odpowiedz negatywna albo kompletnie oderwana od rzeczywistości.
>
> Więcej informacji w dziale "Dokumentacja do pobrania" (sekcja "Organizacje pozarządowe (NGOs)"): <a href="/dokumentacja/index.html#organizacje-pozarządowe-ngos" target="_blank">/dokumentacja/index.html#organizacje-pozarządowe-ngos</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 22.06.2017 – 16.01.2018 (22 czerwiec do 16 styczeń 2018 r.)
> Blisko 20 placówek w Polsce, wykonujących opinie medyczno-sądowe na zlecenie prywatne, współpracujących również z ośrodkami medycznymi i posiadające dostęp do lekarzy specjalistów i odpowiedniego sprzętu potrzebnego do uzupełnienia badań w celu postawienia kompletnej diagnozy i wykonania opinii medyczno-sądowej, odmówiło podjęcia się zlecenia. Etap typowania placówek, przygotowania pytań i treści zapytania, rozsyłania zapytań do wytypowanych placówek – najpierw osobiście, następnie przez pełnomocnika – oraz sprawdzania kompletności otrzymanej oferty.
>
> W związku z powyższym – co, warto zaznaczyć, miało miejsce nie po raz pierwszy, gdyż od 2014 r. wielokrotnie lekarze w Polsce, nie wiem na jakiej podstawie, odmawiali mi wykonania badań lub zlecali niewłaściwe badania lub stawiali błędne diagnozy – kompleksowe badania i konsultacje wykonałem w końcu poza Polska: w okresie 15 styczeń 2018 – 20 kwiecień 2018, tj. w niecałe 3 miesiące, coś czego polscy lekarze odmawiali mi blisko 4 lata.
>
> Więcej informacji w dziale "Dokumentacja do pobrania" (podsekcja "Zapytania dot. wykonania badań i opinii medyczno-sądowej w Polsce (2017 r.)"): <a href="/dokumentacja/#zapytania-dot-wykonania-badań-i-opinii-medyczno-sądowej-w-polsce-2017-r" target="_blank">/dokumentacja/#zapytania-dot-wykonania-badań-i-opinii-medyczno-sądowej-w-polsce-2017-r</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 17.06.2017 – 17.08.2017 (17 czerwiec do 17 sierpień 2017 r.)
> Zleciłem uwierzytelnienie wiadomości SMS, które otrzymałem zaraz przed i po wydarzeniach stycznia 2014, tj. w okresie 18/12/2013 do 30/03/2014. Usługa została wykonana przez firmę zajmującą się informatyką śledczą, świadczącą takie same usługi na zlecenie prywatne jak i organów ścigania. Etap typowania, rozsyłania zapytań do wytypowanych firm, sprawdzania kompletności otrzymanej oferty, doprecyzowania zlecenia, dostarczenia telefonów, wykonania zlecenia, odbioru telefonów i uwierzytelnionych kopii.
>
> Kopia uwierzytelniona wiadomości SMS przychodzących z / wychodzących do nr. +48721030078, z okresu od 04/03/2014 do 30/03/2014: <a href="/pliki/Komunikacja/GT-E1200_721030078_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1200_721030078_raport.pdf</a>
>
> Kopia uwierzytelniona wiadomości SMS przychodzących z nr. +48512355495, z dnia 28.12.2013, na kilka dni przed opisywanymi zdarzeniami: <a href="/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf</a>
>
> Kopia uwierzytelniona wiadomości SMS przychodzących z / wychodzących do nr. +48503990172, z okresu od 18/12/2013 do 14/01/2014: <a href="/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf</a>
>
> Kopia protokołu zabezpieczenia komunikacji SMS przez specjalistów z dziedziny informatyki śledczej wraz z fakturą i protokołem odbioru: <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 22.03.2017 – 29.03.2017 (22 marzec 2017 do 29 marzec 2017 r.)
> Udzieliłem pełnomocnictwa i zleciłem zbadanie akt sprawy dot. zawiadomienia złożonego 28.02.2014. Z akt sprawy jasno wynika, iż organy ścigania nie zrobiły kompletnie nic, równocześnie bezprawnie przekraczając swoje uprawnienia – organy ścigania nie mają uprawnień do wydawania opinii medycznych bez powołania biegłego i przeprowadzenia obdukcji – wydając “diagnozę medyczną” na podstawie artykułu z Wikipedii, tj. jakoby obrażenia ciała były spowodowane kandydozą ukł. pokarmowego.
>
> Kopia postanowienia o odmowie wszczęcia śledztwa na postawie art. 17. § 1 pkt 1 kpk, z uzasadnieniem że to kandydoza jest przyczyną zaobserwowanego trwałego uszkodzenia ciała z dnia 14.04.2014 r.: <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thick-separator" />

## 2016
### 16.01.2016 – 29.06.2016 (16 styczeń do 29 czerwiec 2016 r.)
> Jednym z zaleceń chirurga rekonstrukcyjnego ukł. moczowo-płciowego, z którym widziałem się na początku stycznia w Belgradzie, Serbii, było przeprowadzenie konsultacji z neuro-urologiem (tj. lekarzem specjalistą neurologii i urologii). Niestety, od powrotu z Belgradu, pomimo wielomiesięcznych poszukiwań, nie znalazłem takiego specjalisty – albo z powodu braków kadrowych, albo niemożności doprecyzowania kosztorysu za wykonanie usługi konsultacji i przeprowadzenia badań urodynamicznych przez te 2-3 ośrodki, które takich specjalistów posiadało w swoich zespołach.
>
> Z racji wcześniejszych sytuacji, gdzie nawet z jasno doprecyzowanym kosztorysem i zakresem usług, zostałem oszukany – jak np. w Pradze z końcem 2015 roku – chciałem się  zabezpieczyć najlepiej jak to możliwe. Etap typowania placówek, przygotowania zapytania, rozsyłania zapytań do wytypowanych placówek, sprawdzania kompletności otrzymanej oferty, uszczegółowienia.
>
> Kopia wezwania do zapłaty w związku z odmówieniem wykonania badań z dnia 28.01.2018
> * Wersja w j. polskim, tłumaczenie: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf</a>
> * Wersja w j. czeskim, oryginał: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 05.01.2016 – 15.01.2016 (5 styczeń 2016 do 15 styczeń 2016 r.)
> Wizyta w Belgradzie, Serbii, u znanego chirurga rekonstrukcyjnego ukł. moczowo-płciowego, połączona m.in. z badaniem USG, które po raz kolejny wykazało ewidentne objawy pęcherza neurogennego. Jedną z rekomendacji którą otrzymałem przy wizycie było przeprowadzenie konsultacji z neuro-urologiem (tj. lekarzem specjalistą neurologii i urologii).
>
> Kopia wyników badań USG ukł. moczowego (zdjęcia, opis) i rekomendacji wykonania kolejnych badań i konsultacji, wykonanych 11 stycznia 2016 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thick-separator" />

## 2015
### 13.12.2015 – 14.12.2015 (13 grudzień do 14 grudzień 2015 r.)
> Wizyta w Pradze, Republice Czeskiej, w celu wykonania kompleksowych badań – w tym m.in. badań urodynamicznych, rezonansu magnetycznego, badania dopplerowskiego przepływu krwi w genitaliach, badania spermy. Pomimo wcześniejszych ustaleń, dopełnienia wymogu zapłacenia pełnej kwoty 1250 EUR przelewem przed przyjazdem do Pragi, również kosztów transportu i noclegu jakie poniosłem, stawieniu się na czas w placówce medycznej gdzie badania miały być wykonane, zostało mi na ostatnią chwilę, już na miejscu, bez podania żadnej przyczyny, odmówione wykonanie zaplanowanych badań.
>
> Sprawę przekazałem kancelarii adwokackiej w Czeskim Cieszynie w celu wyegzekwowania należności, chcąc skupić się na wykonaniu niezbędnych badań w innej placówce.
>
> Skontaktowałem się z chirurgiem rekonstrukcyjnym ukł. moczowo-płciowego w Belgradzie, Serbii – jedną z innych, wcześniej wytypowanych placówek – i ustawiłem termin konsultacji na 11 stycznia 2016 r.
>
> Kopia wezwania do zapłaty w związku z odmówieniem wykonania badań z dnia 28.01.2018
> * Wersja w j. polskim, tłumaczenie: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf</a>
> * Wersja w j. czeskim, oryginał: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 4.11.2015 – 11.11.2015 (4 listopad 2015 do 11 listopad 2015 r.)
> Zleciłem wykonanie tłumaczeń przysięgłych zebranych wyników badań w celu przeprowadzenia kompleksowych badań poza Polską. Etap typowania, przygotowania zapytania, rozsyłania zapytań do wytypowanych firm, sprawdzania kompletności otrzymanej oferty, zlecenia, dostarczenia oryginałów, odbioru tłumaczeń.
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 17.09.2015 – 08.10.2015 (17 wrzesień 2015 do 8 październik 2015 r.)
> Celem właściwego skompletowania materiałów dowodowych i złożenia nowego zawiadomienia o popełnieniu przestępstwa, konsultowałem się z detektywem – byłym policjantem wydziału kryminalnego.
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 20.06.2015 – 1.12.2015 (20 czerwiec 2015 do 1 grudnia 2015 r.)
> W celu wykonania kompleksowych badań – w tym m.in. badań urodynamicznych, rezonansu magnetycznego, badania dopplerowskiego przepływu krwi w genitaliach, badania spermy – poza Polską, z racji wielokrotnego odmawiania mi wykonania badań, zlecania mi niewłaściwych badań lub stawiania błędnych diagnozy przez polskich lekarzy, zacząłem szukać placówek medycznych posiadających odpowiedni sprzęt i specjalistów. Etap typowania placówek, przygotowania treści zapytania, rozsyłania zapytań do wytypowanych placówek, sprawdzania kompletności otrzymanej oferty, zlecenia wykonania badań, organizacji transportu i noclegu.
>
> Niestety, zostałem oszukany przez placówkę którą wybrałem. Pomimo wcześniejszych ustaleń, dopełnienia wymogu zapłacenia pełnej kwoty 1250 EUR przelewem przed przyjazdem do Pragi, również kosztów transportu i noclegu jakie poniosłem, stawieniu się na czas w placówce medycznej gdzie badania miały być wykonane, zostało mi na ostatnia chwile, już na miejscu, bez podania żadnej przyczyny, odmówione wykonanie zaplanowanych badań.
>
> Kopia wezwania do zapłaty w związku z odmówieniem wykonania badań z dnia 28.01.2018
> * Wersja w j. polskim, tłumaczenie: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf</a>
> * Wersja w j. czeskim, oryginał: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thick-separator" />

## 2014
### 12.12.2014 (12 grudzień 2014 r.)
> Błędne stawiane diagnozy przez krakowskich lekarzy miało miejsce wielokrotnie od wydarzeń stycznia 2014 r.–tym razem podczas badania rezonansu magnetycznego miednicy w CM iMed24 w Krakowie w grudniu 2014 r., gdzie udałem się wg. skierowania urologa na kompleksowe badanie miednicy włącznie z nerkami, jednak dane DICOM i raport, który otrzymałem z tego badania, dotyczy jedynie „miednicy mniejszej” a objętość danych DICOM to ok. 1/10 danych, które otrzymałem przy innym badaniu rezonansu magnetycznego również miednicy, gdzie jasno widoczne są m.in. zmiany bliznowate w ok. lewej pachwiny.
>
> Kopia skierowania na badania rezonansu magnetycznego MIEDNICY (całej miednicy, wraz z nerkami i bliznami w okolicy pachwiny!), z dnia 20 listopada 2014: <a href="/pliki/Medyczne/MHSiemaszko_Uromed_skierowanie_Rezonans-Magnetyczny_20141120.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_Uromed_skierowanie_Rezonans-Magnetyczny_20141120.pdf</a>
>
> Kopia rachunku za badania rezonansu magnetycznego MIEDNICY MNIEJSZEJ, wykonane w CM iMed24 dnia 12 grudnia 2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy_rachunek_20141212.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy_rachunek_20141212.pdf</a>
>
> Kopia wyników badań rezonansu magnetycznego MIEDNICY MNIEJSZEJ, wykonanych w CM iMed24 dnia 12 grudnia 2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy-mniejszej-nie-calej_BZDURA_raport_20141212.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy-mniejszej-nie-calej_BZDURA_raport_20141212.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 7.11.2014 (7 listopad 2014 r.)
> Ponieważ od czasu wydarzeń stycznia 2014 r. nie mogłem wykonywać pracy zarobkowej – do tego stopnia iż 11 lutego 2014 r. musiałem rozwiązać w trybie natychmiastowym dopiero co podpisany dwuletni kontrakt – przez co bylem w kiepskiej sytuacji finansowej, 17 czerwca 2014 r. podpisałem kilkomiesięczny kontrakt na świadczenie usług IT na miejscu u klienta w Londynie i wyjechałem z Krakowa. Jednak do czasu wyjazdu skrytkę regularnie sprawdzałem, lecz ani decyzji o odmowie wszczęcia śledztwa – pomimo upływu blisko dwóch miesięcy od jej wydania – ani awiza pierwszego czy powtórnego nie znalazłem wtedy w skrytce.
>
> Dopiero po powrocie do Krakowa, 7 listopada 2014 r., kopertę z doręczoną już decyzją znalazłem w skrytce, w związku z czym nie mogłem jej terminowo zaskarżyć. Mając odłożone środki finansowe na przeprowadzenie badań, których – oprócz badania USG ukł. moczowego – nie bylem w stanie wykonać po wydarzeniach stycznia 2014 r., chciałem wykonać m.in. właściwa obdukcje u urologa, badanie rezonansu magnetycznego z uwzględnieniem ukł. moczowego i blizny oraz badania urodynamiczne. Niestety, po raz kolejny błędne diagnozowanie czy wręcz absurdalne odmawianie wykonania badań zaważyło na tym iż nie konsultowałem się już więcej z lekarzami w Polsce w celu wykonania niezbędnych badań; w tym przekonaniu zostałem po raz kolejny utwierdzony w 2017 roku, gdy blisko 20 placówek, wykonujących na prywatne zlecenie te same usługi, odmówiło mi wykonania opinii medyczno-sądowej i niezbędnych badań potrzebnych do uzupełnienia dokumentacji medycznej.
>
> Odmawiając pomocy medycznej czy błędnie diagnozując, polscy lekarze narazili mnie na pogorszenie stanu zdrowia; badania których w Polsce przez tyle czasu nie mogłem wykonać, wykonałem w końcu w ciągu niecałych 3 miesięcy w prywatnych placówkach medycznych poza Polska.
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 05.11.2014 (5 listopad 2014 r.)
> Już w listopadzie 2014 r. informacja, iż w styczniu 2014 r. zostałem poddany zabiegowi medycznemu–w wynajmowanym przeze mnie mieszkaniu przy ul. Szymanowskiego 5/10 w Krakowie, i na który to zabieg nigdy zgody nie wyraziłem–była znana choćby p. Tomaszowi Gibasowi, od którego 5 listopada 2014 r. otrzymałem wiadomość email z której wynika, że zostałem poddany operacji, która się nie powiodła i za którą to operacje p. Gibas proponował mi wyegzekwowanie odszkodowania.
>
> Kopia wiadomości email otrzymanej od p. Tomasza Gibasa 5 listopada 2014 r.: <a href="/pliki/Komunikacja/MHSiemaszko_Email_Compensation-for-failed-surgery.20141105.pdf" target="_blank">/pliki/Komunikacja/MHSiemaszko_Email_Compensation-for-failed-surgery.20141105.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 14.04.2014 (14 kwiecień 2014 r.)
> Prokuratura Kraków-Krowodrza w Krakowie wydała postanowienie o odmowie wszczęcia śledztwa w związku z wydarzeniami stycznia 2014 r.; postanowienie zostało wydane na podstawie art. 17. § 1 pkt 1 kpk – tj. że do przestępstwa nie doszło. Co więcej, rzekomo trwałe obrażenia ciała będące wynikiem tych wydarzeń zostały spowodowane przez kandydozę ukł. pokarmowego. Absurdalność tego uzasadnienia oraz fakt, że decyzja do dziś dnia nie została mi poprawnie doręczona i ten sam prokurator prowadzący postępowanie dot. nowego zawiadomienia, złożonego przeze mnie 9.01.2018 r., dopuścił się kolejnych rażących naruszeń obowiązków, tylko utwierdza w przekonaniu, iż miało i ma miejsce celowe ukrycie przestępstwa:
>
> - Organy ścigania nie maja uprawnień do wydawania opinii medycznych bez powołania biegłego i przeprowadzenia obdukcji, i ani prokurator Bartłomiej Legutko ani st. post. Daria Curzydło, prowadzący postępowanie sprawdzające z zawiadomienia złożonego 28.02.2014 r. nie posiadali żadnej wiedzy specjalnej aby w odmowie wszczęcia śledztwa uzasadnić, iż kandydoza jest przyczyną trwałego uszkodzenia ciała. Ponieważ kandydoza nie może być przyczyną uszkodzenia ukł. moczowo-płciowego, nerwowego i ran kłutych w okolicy pachwiny, nie może być przyczyną bólu krocza, podbrzusza, neurogennego pęcherza moczowego i problemów z oddawaniem moczu – czyli wszystkich tych objawów które wtedy wystąpiły, z dnia na dzień, w wyniku wydarzeń stycznia 2014 r. – a żeby organy ścigania mogły cokolwiek stwierdzić, musi zostać powołany lekarz sądowy i przeprowadzona obdukcja wraz z niezbędnymi badaniami;
> - Decyzja o odmowie wszczęcia śledztwa, będąca pismem procesowym w postępowaniu karnym, została mi nieprawidłowo doręczona przez skrytkę pocztową – w związku ze złożeniem zawiadomienia, skrytkę regularnie sprawdzałem do czasu kilkumiesięcznego wyjazdu 15 czerwca 2014 r., pomimo tego kopertę z decyzją znalazłem w skrytce dopiero w listopadzie 2014 r., bez otrzymania uprzednio awiza, ani pierwszego ani powtórnego, nie odbierając jej osobiście, w związku z czym nie mogłem jej terminowo zaskarżyć. Byłoby oczywiście kompletnie wbrew logice i mojemu interesowi aby, wiedząc o tym że decyzja została wydana i doręczona poprawnie, nie zareagować w celu terminowego jej zaskarżenia
>
> Kopia postanowienia o odmowie wszczęcia śledztwa na postawie art. 17. § 1 pkt 1 kpk, z uzasadnieniem że to kandydoza jest przyczyną zaobserwowanego trwałego uszkodzenia ciała z dnia 14.04.2014 r.: <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a>
>
> Kopia zaświadczenia, że kandydoza nie jest przyczyna zaobserwowanych obrażeń ciała i uszkodzenia ukł. moczowo-płciowego, wystawione przez lek. medycyny prowadzącego kuracje na kandydozę w 2013 r.: <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a>
>
> Kopia orzeczenia lekarskiego lekarza specjalisty neurologii i chirurgii plastycznej z konsultacji i badań wykonanych 6 i 20 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. nerwowego, wykonanych 6 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
>
> Kopia wyników badań rezonansu magnetycznego ukł. nerwowego, wykonanych 12 marca 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>
>
> Kopia wyników badań tomografii komputerowej, wykonanych 13 lutego 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf</a><br/>
> * Wersja w j. niemieckim, oryginał: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. moczowego (zdjęcia, opis) i rekomendacji wykonania kolejnych badań i konsultacji, wykonanych 11 stycznia 2016 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. moczowego (zdjęcia, opis), wykonanych 29 stycznia 2014 r., zaraz po wydarzeniach
> * Wersja w j. polskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.pdf</a>
> * Wersja w j. angielskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 21.03.2014 (21 marzec 2014 r.)
> Stawiłem się osobiście w Prokuraturze Rejonowej Kraków-Śródmieście i pod przysięga zeznałem w sprawie wydarzeń stycznia 2014 r., w związku z zawiadomieniem o popełnieniu przestępstwa z 28 lutego 2014 r.
>
> Kopia wezwania do stawienia się na zeznania w sprawie zawiadomienia o popełnieniu przestępstwa z dnia 11.03.2014: <a href="/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 15.03.2014 (15 marzec 2014 r.)
> Odebrałem przesłane na adres skrytki adresowej w Krakowie wezwanie do stawienia się na zeznania w związku z wydarzeniami stycznia 2014 r. w Prokuraturze Rejonowej Kraków-Śródmieście. W związku ze złożeniem zawiadomienia, skrytkę regularnie sprawdzałem.
>
> Kopia wezwania do stawienia się na zeznania w sprawie zawiadomienia o popełnieniu przestępstwa z dnia 11.03.2014: <a href="/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 04.03.2014 – 30.03.2014 (4 marzec 2014 do 30 marzec 2014 r.)
> Kilka tygodni po wydarzeniach stycznia 2014 r., w lutym 2014 r. skontaktowała ze mną p. Monika H., twierdząc kilkakrotnie (przy spotkaniach osobistych jak i uwierzytelnionych SMS-ach, jak w załączonym raporcie) ze “jest moją kobieta” i “kocha mnie”. Nigdy wcześniej nie znałem tej osoby i nie darzyłem / nie darze żadnym uczuciem; nasz kontakt został zainicjowany z jej strony i był wyłącznie fizyczny.
>
> P. Monika H. zachowywała się dziwnie, wydawała się być niezrównoważona psychicznie. Biorąc pod uwagę jej stwierdzenia przy naszych spotkaniach i w uwierzytelnionej komunikacji SMS, a przede wszystkim jej stan psychiczny przy spotkaniach – tj. otępienie, strach, łzy w oczach kilkakrotnie – podejrzewam że była zamieszana w wydarzenia stycznia 2014 r.
>
> Kopia fragmentu komunikacji na Facebook Messenger z Moniką Hudyką, kiedy dnia 3 lutego 2014 r. zainicjowała kontakt ze mną: <a href="/pliki/Komunikacja/Screenshot_20181219-172035_Messenger.pdf" target="_blank">/pliki/Komunikacja/Screenshot_20181219-172035_Messenger.pdf</a>
>
> Kopia wiadomości SMS przychodzących z / wychodzących do nr. +48721030078, należącego do p. M. Hudyki, z okresu od 04/03/2014 do 30/03/2014, uwierzytelniona na potrzeby wykorzystania jako dowód przez specjalistów informatyki śledczej, firmę Mediarecovery: <a href="/pliki/Komunikacja/GT-E1200_721030078_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1200_721030078_raport.pdf</a>
>
> Kopia protokołu zabezpieczenia komunikacji SMS przez specjalistów z dziedziny informatyki śledczej wraz z fakturą i protokołem odbioru: <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 03.01.2014 (1 marzec 2014 r.)
> Pomimo chęci kontynuowania najmu mieszkania przy ul. Szymanowskiego 5/10 w Krakowie – przynajmniej do czasu zabezpieczenia śladów – z powodu niewyrażenia zgody przez Wynajmującego na zainstalowanie dodatkowych zabezpieczeń w postaci łańcucha do drzwi wejściowych oraz kilkakrotne agresywne zachowanie ze strony Wynajmującego, umowę rozwiązałem z dniem 1 marca 2014 r. wyprowadzając się.
>
> Kopia wypowiedzenia umowy wynajmu lokalu mieszkalnego przy ul. Szymanowskiego 5/10 w Krakowie z dnia 1.03.2014: <a href="/pliki/Wynajem/Szymanowskiego_umowa_wypowiedzenie.pdf" target="_blank">/pliki/Wynajem/Szymanowskiego_umowa_wypowiedzenie.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 28.02.2014 (28 luty 2014 r.)
> W związku z wydarzeniami stycznia 2014 r. w wynajmowanym mieszkaniu przy ul. Szymanowskiego 5/10 w Krakowie, adwokat z którym współpracowałem wtedy przygotował i złożył zawiadomienie o popełnieniu przestępstwa.
>
> Kopia pisma “Zawiadomienie o popełnieniu przestępstwa z art. 197 §1 k.k.” z dnia 28.02.2014: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zawiadomienie_20140228.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zawiadomienie_20140228.zlozone.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 11.02.2014 (11 luty 2014 r.)
> Z racji stanu zdrowia w związku z wydarzeniami stycznia 2014 r., musiałem rozwiązać w trybie natychmiastowym dwuletni kontrakt, który dopiero niecałe 4 miesiące wcześniej rozpocząłem; przez wiele miesięcy nie byłem w stanie wykonywać pracy zarobkowej.
>
> Kopia wypowiedzenia umowy współpracy z firmą Zerochaos z dnia 11.02.2014: <a href="/pliki/Wspolpraca/Zerochaos_wypowiedzenie-umowy.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_wypowiedzenie-umowy.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 29.01.2014 (29 styczeń 2014 r.)
> Z racji nasilających się problemów z oddawaniem moczu, oraz bólem min. podbrzusza, prącia przy oddawaniu moczu, pachwiny i krocza, 29 stycznia 2014 r. wykonałem prywatnie badanie USG w Krakowie, które wykazało już wtedy objawy pęcherza neurogennego i bardzo poważne zaleganie moczu, blisko 400 ml.
>
> Na opisywane objawy – tj. problemy z oddawaniem moczu, oraz ból min. podbrzusza, prącia przy oddawaniu moczu, pachwiny i krocza – przy wizytach 16 i 22 stycznia w innym centrum medycznym w Krakowie, gdzie posiadałem w tym czasie ubezpieczenie, lekarze zlecali mi badania i lekarstwa kompletnie nie mające nic wspólnego z właściwym zdiagnozowaniem i wyleczeniem dolegliwości. Np. „Gastroduodenoskopie”, a nie badania, które przy nasilających się dolegliwościach, które jasno opisywałem, rzeczywiście muszą zostać wykonane w celu postawienia poprawnej diagnozy, tj.: badanie USG ukł. moczowego, badanie dopplerowskie przepływu krwi w genitaliach, badania urodynamiczne, obdukcje przez urologa / androloga.
>
> Odnośnie pozostałych badań, w tym obdukcji, zostałem poinformowany przez adwokata, z którym współpracowałem w tym czasie, iż po złożeniu zawiadomienia o popełnieniu przestępstwa zostanie wyznaczony biegły, który obdukcje i badania przeprowadzi.
>
> Kopia wyników badań USG ukł. moczowego (zdjęcia, opis), wykonanych 29 stycznia 2014 r., zaraz po wydarzeniach
> * Wersja w j. polskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.pdf</a>
> * Wersja w j. angielskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 22.01.2014 (22 styczeń 2014 r.)
> Błędne stawiane diagnozy przez krakowskich lekarzy miało miejsce wielokrotnie od wydarzeń stycznia 2014 r.–po raz kolejny podczas drugiej wizyty w CM Medicover w Krakowie–gdzie na objawy opisywane, tj. ból w pachwinie i kroczu, ból podbrzusza, problemy z oddawaniem moczu, ból końcowego otworu przewodu pokarmowego, lekarze zlecali mi badania i lekarstwa kompletnie nie mające nic wspólnego z właściwym zdiagnozowaniem i wyleczeniem dolegliwości.
>
> Kopia odpisu wizyty w CM Medicover w dniu 22.01.2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_22-01-2014.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_22-01-2014.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 16.01.2014 (16 styczeń 2014 r.)
> Błędne stawiane diagnozy przez krakowskich lekarzy miało miejsce wielokrotnie od wydarzeń stycznia 2014 r.–po raz pierwszy podczas wizyty w CM Medicover w Krakowie–gdzie na objawy opisywane, tj. ból w pachwinie i kroczu, ból podbrzusza, problemy z oddawaniem moczu, ból końcowego otworu przewodu pokarmowego, lekarze zlecali mi badania i lekarstwa kompletnie nie mające nic wspólnego z właściwym zdiagnozowaniem i wyleczeniem dolegliwości.
>
> Kopia odpisu z wizyty w CM Medicover w dniu 16.01.2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_16-01-2014.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_16-01-2014.pdf</a>
>       

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 9.01.2014 (9 styczeń 2014 r.)
> W nocy z 8 na 9 stycznia zostałem napadnięty w wynajmowanym mieszkaniu, przy ul. Szymanowskiego 5/10 w Krakowie, skutkiem czego jest trwałe uszkodzenie ciała, w tym ukł. moczowo-płciowego i nerwowego, oraz blizny pozostałe po ranach kłutych.
>
> Komplet dokumentacji medycznej zebranej od czasu tych wydarzeń bezsprzecznie potwierdza działanie osób 3cich, zaprzeczając kategorycznie twierdzeniu, iż kandydoza ukł. pokarmowego – co zostało podane jako rzekoma przyczyna wystąpienia obrażeń ciała przez krakowskie organy ścigania – jest przyczyną zaobserwowanych obrażeń ciała i uszkodzenia ukł. moczowo-płciowego i nerwowego oraz blizn po ranach kłutych.
>
> Kopia postanowienia o odmowie wszczęcia śledztwa na postawie art. 17. § 1 pkt 1 kpk, z uzasadnieniem że to kandydoza jest przyczyną zaobserwowanego trwałego uszkodzenia ciała z dnia 14.04.2014 r.: <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a>
>
> Kopia zaświadczenia, że kandydoza nie jest przyczyna zaobserwowanych obrażeń ciała i uszkodzenia ukł. moczowo-płciowego, wystawione przez lek. medycyny prowadzącego kuracje na kandydozę w 2013 r.: <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a>
>
> Kopia orzeczenia lekarskiego lekarza specjalisty neurologii i chirurgii plastycznej z konsultacji i badań wykonanych 6 i 20 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. nerwowego, wykonanych 6 kwietnia 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
>
> Kopia wyników badań rezonansu magnetycznego ukł. nerwowego, wykonanych 12 marca 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>
>
> Kopia wyników badań tomografii komputerowej, wykonanych 13 lutego 2018 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf</a><br/>
> * Wersja w j. niemieckim, oryginał: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. moczowego (zdjęcia, opis) i rekomendacji wykonania kolejnych badań i konsultacji, wykonanych 11 stycznia 2016 r.
> * Wersja w j. polskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf</a>
> * Wersja w j. angielskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>
>
> Kopia wyników badań USG ukł. moczowego (zdjęcia, opis), wykonanych 29 stycznia 2014 r., zaraz po wydarzeniach
> * Wersja w j. polskim, oryginał: <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.pdf</a>
> * Wersja w j. angielskim, tłumaczenie przysięgłe: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thick-separator" />

## 2013
### 28.12.2013 (28 grudzień 2013 r.)
> Otrzymałem SMSa z groźbami karalnymi “(…) ***byś nigdy nie miał potomstwa*** (…)”, spełnionymi kilka dni później w wyniku wydarzeń stycznia 2014 (trwałe uszkodzenie ciała, w tym ukł. moczowo-płciowego i nerwowego, oraz blizny pozostałe po ranach kłutych w okolicach pachwiny).
>
> Kopia uwierzytelniona wiadomości SMS przychodzących z nr. +48512355495, z dnia 28.12.2013, na kilka dni przed opisywanymi zdarzeniami: <a href="/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf</a>
>
> Kopia protokołu zabezpieczenia komunikacji SMS przez specjalistów z dziedziny informatyki śledczej wraz z fakturą i protokołem odbioru: <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 18.12.2013 – 14.01.2014 (18 grudzień 2013 r. do 14 styczeń 2014 r.)
> Otrzymałem serie SMS-ów od, jak się okazało, osoby podszywającej się pod kogoś innego – zanim zorientowałem się że to osoba mi kompletnie obca podałem mój ówczesny adres zamieszkania, tj. ul. Szymanowskiego 5/10 w Krakowie.
>
> Kopia uwierzytelniona wiadomości SMS przychodzących z / wychodzących do nr. +48503990172, z okresu od 18/12/2013 do 14/01/2014: <a href="/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf</a>
>
> Kopia protokołu zabezpieczenia komunikacji SMS przez specjalistów z dziedziny informatyki śledczej wraz z fakturą i protokołem odbioru:<a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 16.12.2013 – 18.12.2013 (16 grudzień 2013 r. do 18 grudzień 2013 r.)
> Przebywałem na dwudniowej delegacji w Zurychu, Szwajcarii, w związku z wdrażaniem w nowy projekt.
>
> Kopia dokumentacji dotyczącej delegacji do Zurychu, Szwajcarii:
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 12.12.2013 (12 grudzień 2013 r.)
> Trzecia z wizyt u lekarza medycyny w związku z leczeniem kandydozy ukł. Pokarmowego – jedynej dolegliwości do czasu wydarzeń stycznia 2014 r.
>
> Kopia diagnoz i zaleceń wydanych przez lek. med. Andrzeja Gliwę (CM Intermed) podczas wizyt w maju, listopadzie oraz grudniu 2013 r.: <a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 13.11.2013 (13 listopad 2013 r.)
> Druga z wizyt u lekarza medycyny w związku z leczeniem kandydozy ukł. pokarmowego – jedynej dolegliwości do czasu wydarzeń stycznia 2014 r.
>
> Kopia diagnoz i zaleceń wydanych przez lek. med. Andrzeja Gliwę (CM Intermed) podczas wizyt w maju, listopadzie oraz grudniu 2013 r.: <a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 6.10.2013 (6 październik 2013 r.)
> Wynająłem mieszkanie przy ul. Szymanowskiego 5/10 w Krakowie, w związku z podpisaniem dwuletniego kontraktu na świadczenie usług IT.
>
> Kopia umowy wynajmu lokalu mieszkalnego przy ul. Szymanowskiego 5/10 w Krakowie: <a href="/pliki/Wynajem/Szymanowskiego_umowa.pdf" target="_blank">/pliki/Wynajem/Szymanowskiego_umowa.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Wróć do spisu treści">Wróć do spisu treści</a></p>

<hr class="thin-separator" />

### 19.09.2013 (19 wrzesień 2013 r.)
> Podpisałem dwuletni kontrakt na świadczenie usług IT dla firmy w Krakowie.
>
> Kopia umowy współpracy z firmą Zerochaos z dnia 19.09.2013: <a href="/pliki/Wspolpraca/Zerochaos_umowa.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_umowa.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 08.2013 (sierpień 2013 r)
> Wywiad wideo zarejestrowany w sierpniu 2013 r., około **5 miesięcy przed** wydarzeniami stycznia 2014 r.: <a href="/inne/2019/07/23/wywiad-wideo-z-sierpnia-2013.html" target="_blank">/inne/2019/07/23/wywiad-wideo-z-sierpnia-2013.html</a>
>
