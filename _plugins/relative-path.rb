require "pathname"

###########################################################
# Based on https://github.com/jekyll/jekyll/pull/7281 and
#          https://github.com/jekyll/jekyll/issues/6360

module Jekyll
  module CustomURLFilters
    # Produces a path relative to the current page. For example, if the path
    # `/images/me.jpg` is supplied, then the result could be, `me.jpg`,
    # `images/me.jpg` or `../../images/me.jpg` depending on whether the current
    # page lives in `/images`, `/` or `/sub/dir/` respectively.
    #
    # input - a path relative to the project root
    #
    # Returns the supplied path relativized to the current page.
    def relative_path(input)
      return if input.nil?

      input = ensure_leading_slash(input)
      page_url = @context.registers[:page]["url"]
      page_dir = Pathname(page_url).parent
      Pathname(input).relative_path_from(page_dir).to_s
    end

    def ensure_leading_slash(input)
      return input if input.nil? || input.empty? || input.start_with?("/")

      "/#{input}"
    end
  end
end

Liquid::Template.register_filter(Jekyll::CustomURLFilters)
