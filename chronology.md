---
title: Chronology of events
layout: page
permalink: "/chronology/index.html"
ref: chronology
lang: en
sortkey: 1
---

Chronology of events from 2013 to the present day.

{:#toc}
- {:.year-element} [2019](#2019)
  * {:.date-element} [21.05.2019 (May 21, 2019)](#21052019-may-21-2019)
  * {:.date-element} [06.05.2019 (May 6, 2019)](#06052019-may-6-2019)
  * {:.date-element} [20.02.2019 (February 20, 2019)](#20022019-february-20-2019)
  * {:.date-element} [06.02.2019 (February 6, 2019)](#06022019-february-6-2019)
  * {:.date-element} [04.02.2019 (February 4, 2019)](#04022019-february-4-2019)
  * {:.date-element} [03.01.2019 (January 3, 2019)](#03012019-january-3-2019)
  * {:.date-element} [02.01.2019 (January 2, 2019)](#02012019-january-2-2019)
- {:.year-element} [2018](#2018)
  * {:.date-element} [27.12.2018 (December 27, 2018)](#27122018-december-27-2018)
  * {:.date-element} [11.12.2018 (December 11, 2018)](#11122018-december-11-2018)
  * {:.date-element} [3.12.2018 (December 3, 2018)](#3122018-december-3-2018)
  * {:.date-element} [13.08.2018 (August 13, 2018)](#13082018-august-13-2018)
  * {:.date-element} [19.07.2018 (July 19, 2018)](#19072018-july-19-2018)
  * {:.date-element} [12.07.2018 (July 12, 2018)](#12072018-july-12-2018)
  * {:.date-element} [13.06.2018 (June 13, 2018)](#13062018-june-13-2018)
  * {:.date-element} [5.06.2018 (June 5, 2018)](#5062018-june-5-2018)
  * {:.date-element} [20.04.2018 (April 20, 2018)](#20042018-april-20-2018)
  * {:.date-element} [17.04.2018 (April 17, 2018)](#17042018-april-17-2018)
  * {:.date-element} [13.04.2018 (April 13, 2018)](#13042018-april-13-2018)
  * {:.date-element} [6.04.2018 (April 6, 2018)](#6042018-april-6-2018)
  * {:.date-element} [4.04.2018 (April 4, 2018)](#4042018-april-4-2018)
  * {:.date-element} [07.03.2018 (March 7, 2018)](#07032018-march-7-2018)
  * {:.date-element} [28.02.2018 – 16.03.2018 (February 28, 2018 to March 16, 2018)](#28022018--16032018-february-28-2018-to-march-16-2018)
  * {:.date-element} [19.02.2018 (February 19, 2018)](#19022018-february-19-2018)
  * {:.date-element} [15.02.2018 (February 15, 2018)](#15022018-february-15-2018)
  * {:.date-element} [13.02.2018 (February 13, 2018)](#13022018-february-13-2018)
  * {:.date-element} [16.01.2018 (January 16, 2018)](#16012018-january-16-2018)
  * {:.date-element}[15.01.2018 – 20.04.2018 (January 15, 2018 to April 20, 2018)](#15012018--20042018-january-15-2018-to-april-20-2018)
  * {:.date-element}[9.01.2018 (January 9, 2018)](#9012018-january-9-2018)
- {:.year-element} [2017](#2017)
  * {:.date-element} [17.11.2017 (November 17, 2017)](#17112017-november-17-2017)
  * {:.date-element} [30.10.2017 – 19.01.2018 (October 30, 2017 to January 19, 2018)](#30102017--19012018-october-30-2017-to-january-19-2018)
  * {:.date-element} [22.06.2017 – 16.01.2018 (June 22 to January 16, 2018)](#22062017--16012018-june-22-to-january-16-2018)
  * {:.date-element} [17.06.2017 – 17.08.2017 (June 17 to August 17, 2017)](#17062017--17082017-june-17-to-august-17-2017)
  * {:.date-element} [22.03.2017 – 29.03.2017 (March 22, 2017 to March 29, 2017)](#22032017--29032017-march-22-2017-to-march-29-2017)
- {:.year-element} [2016](#2016)
  * {:.date-element} [16.01.2016 – 29.06.2016 (January 16 to June 29, 2016)](#16012016--29062016-january-16-to-june-29-2016)
  * {:.date-element} [05.01.2016 – 15.01.2016 (January 5, 2016 to January 15, 2016)](#05012016--15012016-january-5-2016-to-january-15-2016)
- {:.year-element} [2015](#2015)
  * {:.date-element} [13.12.2015 – 14.12.2015 (December 13 to December 14, 2015)](#13122015--14122015-december-13-to-december-14-2015)
  * {:.date-element} [4.11.2015 – 11.11.2015 (November 4, 2015 to November 11, 2015)](#4112015--11112015-november-4-2015-to-november-11-2015)
  * {:.date-element} [17.09.2015 – 08.10.2015 (September 17, 2015 to October 8, 2015)](#17092015--08102015-september-17-2015-to-october-8-2015)
  * {:.date-element} [20.06.2015 – 1.12.2015 (June 20, 2015 to December 1, 2015)](#20062015--1122015-june-20-2015-to-december-1-2015)
- {:.year-element} [2014](#2014)
  * {:.date-element} [12.12.2014 (December 12, 2014)](#12122014-december-12-2014)
  * {:.date-element} [7.11.2014 (November 7, 2014)](#7112014-november-7-2014)
  * {:.date-element} [05.11.2014 (November 5, 2014)](#05112014-november-5-2014)
  * {:.date-element} [14.04.2014 (April 14, 2014)](#14042014-april-14-2014)
  * {:.date-element} [21.03.2014 (March 21, 2014)](#21032014-march-21-2014)
  * {:.date-element} [15.03.2014 (March 15, 2014)](#15032014-march-15-2014)
  * {:.date-element} [04.03.2014 – 30.03.2014 (March 4, 2014 to March 30, 2014)](#04032014--30032014-march-4-2014-to-march-30-2014)
  * {:.date-element} [03.01.2014 (March 1, 2014)](#03012014-march-1-2014)
  * {:.date-element} [28.02.2014 (February 28, 2014)](#28022014-february-28-2014)
  * {:.date-element} [11.02.2014 (February 11, 2014)](#11022014-february-11-2014)
  * {:.date-element} [29.01.2014 (January 29, 2014)](#29012014-january-29-2014)
  * {:.date-element} [22.01.2014 (January 22, 2014)](#22012014-january-22-2014)
  * {:.date-element} [16.01.2014 (January 16, 2014)](#16012014-january-16-2014)
  * {:.date-element} [9.01.2014 (January 9, 2014)](#9012014-january-9-2014)
- {:.year-element} [2013](#2013)
  * {:.date-element} [28.12.2013 (December 28, 2013)](#28122013-december-28-2013)
  * {:.date-element} [18.12.2013 – 14.01.2014 (December 18, 2013 to January 14, 2014)](#18122013--14012014-december-18-2013-to-january-14-2014)
  * {:.date-element} [16.12.2013 – 18.12.2013 (December 16, 2013 to December 18, 2013)](#16122013--18122013-december-16-2013-to-december-18-2013)
  * {:.date-element} [12.12.2013 (December 12, 2013)](#12122013-december-12-2013)
  * {:.date-element} [13.11.2013 (November 13, 2013)](#13112013-november-13-2013)
  * {:.date-element} [6.10.2013 (October 6, 2013)](#6102013-october-6-2013)
  * {:.date-element} [19.09.2013 (September 19, 2013)](#19092013-september-19-2013)
  * {:.date-element} [08.2013 (August, 2013)](#082013-august-2013)

<hr class="thick-separator" />

## 2019

### 21.05.2019 (May 21, 2019)
> On May 21st, 2019, I filed a complaint to the European Court of Human Rights.
>
> Application form: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG.20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG.20190521.pdf</a>
>
> Annex to Application form, section E. Statement of the Facts: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_Statement-of-the-facts_Annex.20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_Statement-of-the-facts_Annex.20190521.pdf</a>
>
> Annex to Application Form, section I. List of accompanying documents: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_List-of-accompanying-documents_Annex.20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Application_Form_ENG_List-of-accompanying-documents_Annex.20190521.pdf</a>
>
> Proof of postage: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-postage_20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-postage_20190521.pdf</a>
>
> Proof of successful fax transmission: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-fax-transmission_20190522.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-fax-transmission_20190522.pdf</a>
>
> Email sent to ECHR: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Email_20190521.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Email_20190521.pdf</a>
>
> Proof of package delivery: <a href="/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-delivery_20190603.pdf" target="_blank">/pliki/Prawne/ECHR/201905/MHSiemaszko_Proof-of-delivery_20190603.pdf</a>
>
> Complete set of photographs (along with original EXIF metadata) depicting contents of the package: <a href="/echr-complaint-201905-package-contents/" target="_blank">/echr-complaint-201905-package-contents/</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 06.05.2019 (May 6, 2019)
> Subsequent absurd letters, which I received in the last few months from the law enforcement authorities, resulted in filing of another 3 notifications of a crime.
>
> "Notification of a crime under art. 270 § 1 and 276 of the Polish Penal Code", dated 06.05.2019: <a href="/pliki/Prawne/falszowanie-nieprawda/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_falszowanie-nieprawda.pdf" target="_blank">/pliki/Prawne/falszowanie-nieprawda/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_falszowanie-nieprawda.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Dorota Bojanowska", dated 06.05.2019: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_db.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_db.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Rafał Babiński", dated 06.05.2019: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rb.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-regionalna_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rb.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 20.02.2019 (February 20, 2019)
> On February 20, 2019, I submitted in person "Request for intervention" to the Department for Organised Crime and Corruption.
>
> "Request for intervention" addressed to the Department for Organised Crime and Corruption, dated 20.02.2019: <a href="/pliki/Prawne/DdSPZiK/II-Kp-9-19-K_zgloszenie-do-DdSPZiK.20190220.pdf" target="_blank">/pliki/Prawne/DdSPZiK/II-Kp-9-19-K_zgloszenie-do-DdSPZiK.20190220.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 06.02.2019 (February 6, 2019)
> On February 6, 2019, I filed notification of a crime committed by Monika Hudyka.
>
> "Notification of a crime under art. 156 §1, art. 158 §, art. 159, art. 193, art. 233 § 1, art. 234, art. 236 § 1, art. 238, art. 239 § 1, art. 258 § 1 of the Polish Penal Code committed by Monika Hudyka", dated 6.02.2019: <a href="/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf" target="_blank">/pliki/Prawne/Hudyka/mhs_zawiadomienie-hudyka_201902.SIGNED.pdf</a>
>
> More information: <a href="/legal/2019/02/06/hudyka-notification-of-crime.html" target="_blank">/legal/2019/02/06/hudyka-notification-of-crime.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />


### 04.02.2019 (February 4, 2019)
> Four additional notifications of crimes under art. 231 §1 of the Penal Code against public prosecutors and police in a related matter were filed.
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Tomasz Pawlik", dated 04.02.2019: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_tp.SIGNED.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by Police officer Robert Cygan", dated 04.02.2019: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_rc.SIGNED.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by Police officer Monika Górowska", dated 04.02.2019: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_mg.SIGNED.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Beata Sichelska", dated 04.02.2019: <a href="/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf" target="_blank">/pliki/Prawne/MZ_policja-i-prokuratura_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_art-231_bs.SIGNED.pdf</a>  
>
> More information: <a href="/legal/2019/02/04/criminally-prosecuting-law-enforcement-related.html" target="_blank">/legal/2019/02/04/criminally-prosecuting-law-enforcement-related.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 03.01.2019 (January 3, 2019)
> First of the medical negligence lawsuits, filed in mid-August 2018 in New Delhi District Court, was successfully settled, just before Reply to Opposition along with additional evidence was to be filed in Court.
>
> Reply to Opposition: <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf" target="_blank">/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.pdf</a>  
>
> Index: <a href="/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.index.pdf" target="_blank">/pliki/Prawne/Apollo/MHSiemaszko-vs-Apollo-et-al_Reply-To-Opposition.FINAL.index.pdf</a>
>
> More information: <a href="/medical/2019/01/03/first-medical-negligence-lawsuit-settled.html" target="_blank">/medical/2019/01/03/first-medical-negligence-lawsuit-settled.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 02.01.2019 (January 2, 2019)
> Notification of a crime under art. 231 §1 of the Penal Code committed by Police officer was filed.
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by Police officer Daria Curzydło", dated 02.01.2019: <a href="/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf" target="_blank">/pliki/Prawne/policja_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_dc.pdf</a>
>
> More information: <a href="/legal/2019/01/04/criminally-prosecuting-law-enforcement.html" target="_blank">/legal/2019/01/04/criminally-prosecuting-law-enforcement.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thick-separator" />


## 2018

### 27.12.2018 (December 27, 2018)
> On December 27, 2018, through the District Prosecutor's Office in Tarnów, appeal against the decision refusing to initiate an investigation into crimes under art. 231 §1 of the Penal Code committed by four Krakow prosecutors, was sent to the Court.
>
> "Appeal against the decision of the District Prosecutor in Tarnów of November 22, 2018, refusing to initiate an investigation, issued in case no. 1. PR 4 DS360.2018, delivered on December 20 2018", dated 27.12.2018: <a href="/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/PR-4-Ds-360-2018_zazalenie-na-postanowienie.20181227.pdf</a>
>
> More information: <a href="/legal/2019/01/04/criminally-prosecuting-law-enforcement.html" target="_blank">/legal/2019/01/04/criminally-prosecuting-law-enforcement.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 11.12.2018 (December 11, 2018)
> In connection with the erroneous recognition of the procedural nature of the notification of a crime filed on January 9, 2018, as motion to initiate an investigation in case file no. 2 Ds. 385/14–despite the fact that the acts indicated were not the subject of interest nor criminal law reference in the proceedings from the notification submitted on February 28, 2014–as well as new and significant evidence and threads, on December 11, 2018, new notification of a crime was filed.
>
> Along with the notification, I also submitted another request for intervention to the Prosecutor General.
>
> "Notification of a crime under Articles 156 §1, 157 §1, 160 §1, 162 §1, 192 §1, 193, 239, 258 and 268 of the Polish Penal Code, infringement of art. 30, 32, 38, 39, 40, 45, 47 oraz 77 of the Constitution of the Republic of Poland, in connection with art. 44 of the Constitution of the Republic of Poland, as well as violations of Articles 2 („Right to life”), 3 („Prohibition of inhuman and degrading treatment”), 6 („Right to a fair trial”), 13 („Right to an effective remedy”) and 14 („Prohibition of discrimination”) of the European Convention on Human Rights, in connection with art. 9 of the Constitution of the Republic of Poland", dated 11.12.2018: <a href="/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf" target="_blank">/pliki/Prawne/Nowe/MichalSiemaszko_Zawiadomienie_20181211.pdf</a>
>
> "Request for intervention" addressed to the Minister of Justice / Prosecutor General of Republic of Poland, dated 11.12.2018: <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20181211.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20181211.pdf</a>
>
> More information: <a href="/legal/2018/12/11/new-notification-of-a-crime.html" target="_blank">/legal/2018/12/11/new-notification-of-a-crime.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 3.12.2018 (December 3, 2018)
> Second medical negligence lawsuit was filed.
>
> English language version: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.EN.20181128.pdf</a>
>
> Czech language version: <a href="/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf" target="_blank">/pliki/Prawne/EPSUK/MHS_vs_EPS-Uroklinikum-Stolz_Lawsuit.CZ.20181128.pdf</a>
>
> More information: <a href="/medical/2018/12/03/second-medical-negligence-lawsuit.html" target="_blank">/medical/2018/12/03/second-medical-negligence-lawsuit.html</a>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 13.08.2018 (August 13, 2018)
> First of medical negligence lawsuits was filed.
>
> Lawsuit: <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf" target="_blank">/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Lawsuit.EN.20180730.pdf</a>
>
> Index: <a href="/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Index.EN.20180730.pdf" target="_blank">/pliki/Prawne/Apollo/MHS_vs_ST-et-al_Index.EN.20180730.pdf</a>
>
> More information: <a href="/medical/2018/08/20/first-medical-negligence-lawsuit.html" target="_blank">/medical/2018/08/20/first-medical-negligence-lawsuit.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 19.07.2018 (July 19, 2018)
> The Criminal Law Team of the Office of the Ombudsman became interested in the case of flagrant violations of the provisions of law in proceedings conducted in connection with the events of January 2014, asking the Regional Prosecutor in Kraków to examine the case files in terms of assessing their correctness.
>
> Address of the Criminal Law Team of the Office of the Polish Ombudsman to Krakow Regional Prosecutor, dated 19.07.2018: <a href="/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf" target="_blank">/pliki/Prawne/RPO/II.519.576.2018.pismo-do-ProkReg.20181018.pdf</a>
>
> More information: <a href="/legal/2019/01/05/criminal-law-team-of-the-ombudsman-address.html" target="_blank">/legal/2019/01/05/criminal-law-team-of-the-ombudsman-address.html</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 12.07.2018 (July 12, 2018)
> Notifications of crimes under art. 231 §1 of the Penal Code committed by four Krakow prosecutors were filed.
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Maria Zębala", dated 12.07.2018: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_mz.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Małgorzata Lipska", dated 12.07.2018: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ml.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Edyta Kulik", dated 12.07.2018: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_ek.pdf</a>
>
> "Notification of a crime under art. 231 §1 of the Polish Penal Code committed by prosecutor Bartłomiej Legutko", dated 12.07.2018: <a href="/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf" target="_blank">/pliki/Prawne/prokuratorzy_art-231/mhs_prokuratura-krajowa_zawiadomienie-o-popelnieniu-przestepstwa_bl.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 13.06.2018 (June 13, 2018)
> On June 13, 2018, I personally filed in Warsaw–at the National Prosecutor’s Office and the Polish Ombudsman’s Office–requests for intervention.
>
> "Request for intervention" addressed to the Polish Ombudsman, dated 13.06.2018: <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-rpo_20180613.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_wniosek-do-rpo_20180613.pdf</a>
>
> "Request for intervention" addressed to the Minister of Justice / Prosecutor General of Republic of Poland, dated 13.06.2018: <a href="/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20180613.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_wniosek-do-prokuratora-generalnego_20180613.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 5.06.2018 (June 5, 2018)
> Despite the fact that almost 5 months have passed since the notification [of a crime] was submitted on January 9, 2018, none of the actions required by the Code of Penal Procedure or the binding regulations of the Ministry of Justice following the receipt of a notification of a crime were carried out – i.e. none of the letters received so far in this matter is a decision to initiate or refuse to initiate criminal investigation – therefore I filed a complaint against excessive lengthiness of preparatory proceedings at the Krakow-Krowodrza District Court. Recognition of the complaint is pending.
>
> Copy of the document “Complaint for infringement of a party's right to hear the case in preparatory criminal proceedings without undue delay”, dated 5.06.2018: <a href="/pliki/Prawne/Zlozone/mhs_skarga-na-przewleklosc-postepowania-karnego-przygotowawczego_20180605.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_skarga-na-przewleklosc-postepowania-karnego-przygotowawczego_20180605.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 20.04.2018 (April 20, 2018)
> A trip to Vienna, Austria, for additional consultation with a physician specializing in neurology and plastic surgery, as well as to collect the originals of the results of examinations and consultations.
>
> Copy of the medical report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018
> * English language version, original: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
>
> Copy of the results from ultrasound examination of nervous system, conducted on April 6, 2018
> * English language version, original: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 17.04.2018 (April 17, 2018)
> I filed at the District Prosecutor's Office in Krakow, based on additional arguments – i.e. continuation of the same arrogant, divesting behavior, breaches of duty, acting in contravention of applicable laws and regulations – motion to exclude Krakow-Krowodrza Regional Prosecutor’s Office.
>
> For close to four years already I have been supplementing evidence and despite the obvious cover-up of the case by the Krakow law enforcement authorities – this is apparent at this stage – set of evidence, including medical documentation, was nevertheless collected. My homework – with regard to applicable laws and regulations, including the Constitution of the Republic of Poland, the European Convention on Human Rights, as well as case law from international courts binding Polish law enforcement authorities – I have completed.
>
> Copy of the document “Application for designation of entity outside the jurisdiction of Krakow-Krowodrza Regional Prosecutor’s Office to conduct investigation”, dated 17.04.2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie.20180417.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_wniosek-o-wylaczenie.20180417.zlozone.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 13.04.2018 (April 13, 2018)
> The Krakow-Krowodrza Regional Prosecutor’s Office has sent me another letter in which it vaguely responds to the questions I asked, incorrectly claiming that the decision refusing to initiate an investigation [issued] in 2014 was properly delivered to me, continuing to attempt to change my declaration of will and the procedural nature of the notification of a crime submitted by me on January 9, 2018. At this stage it was evident that, again, same Prosecutor’s Office wants to prevent me from asserting my rights, by sending me vague / arrogant letters with no substance, instead of performing their duties.
>
> On the same day, I filed at the District Prosecutor’s Office in Krakow a complaint against the prosecutor of the Krakow-Krowodrza Regional Prosecutor’s Office.
>
> Copy of the letter from the Krakow-Krowodrza Regional Prosecutor’s Office dated 13.04.2018: <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180413.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180413.pdf</a> (Polish language version, original)
>
> Copy of the document “Complaint against the Prosecutor Edyta Kulik”, dated 13.04.2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_ek.20180413.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-okregowa_skarga-na-prokuratora_ek.20180413.zlozone.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 6.04.2018 (April 6, 2018)
> A trip to Vienna, Austria, for consultation with a physician specializing in neurology and plastic surgery. The doctor took into account all the symptoms described by me and, on the basis of the available results of neurography/magnetic resonance examination of the nervous system and additional examinations performed on site, including, among others, ultrasound of the nervous system, issued a medical report which coherently links the symptoms described with his observations and results from examinations conducted. During my stay in Vienna, I also conducted an OCT (Optical Coherence Tomography) exam of scars from puncture wounds in the groin area.
>
> Copy of the medical report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018
> * English language version, original: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
>
> Copy of the results from ultrasound examination of nervous system, conducted on April 6, 2018
> * English language version, original: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 4.04.2018 (April 4, 2018)
> I filed at the Krakow-Krowodrza Regional Prosecutor’s Office a request for information regarding proceedings being conducted as well as request for correct delivery of the decision refusing to initiate an investigation issued in 2014.
>
> On the same day, I filed at the District Prosecutor’s Office in Krakow a complaint for inaction of prosecutor of the Krakow-Krowodrza Regional Prosecutor’s Office, who, in accordance with the applicable regulations, had 6 weeks to issue a decision to initiate or refusing to initiate an investigation.
>
> Copy of the document “Request for information”, dated 4.4.2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-udzielenie-informacji.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-udzielenie-informacji.20180404.zlozone.pdf</a> (Polish language version, original)
>
> Copy of the document “Request for correct delivery of the decision refusing to initiate an investigation issued April 14th 2014”, dated 4.04.2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-doreczenie.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_wniosek-o-doreczenie.20180404.zlozone.pdf</a> (Polish language version, original)
>
> Copy of the document “Complaint for inaction of the Prosecutor”, dated 4.04.2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zazalenie-na-bezczynnosc.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zazalenie-na-bezczynnosc.20180404.zlozone.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 07.03.2018 (March 7, 2018)
> The Krakow-Krowodrza Regional Prosecutor’s Office sent me a letter from which it is clear that they have not familiarized themselves with the content of the notification of a crime nor with the complete set of evidence, attempting to change my declaration of will concerning the procedural nature of notification of a crime, which springs directly from regulations because of difference in the identity of the facts, the establishment of a set of new evidence in support of the claims made, while the medical documentation collected clearly indicates that the decision refusing to initiate investigation was unfounded, at the same time being an unlawful violation of powers on the part of law enforcement authorities.
>
> In addition, most basic examination of evidence was not even carried out – prosecutors did not even manage to find the cases mentioned in the notification [of a crime], despite being provided with case references and case documentation, including decisions where the name of the Prosecutor’s office was clearly indicated.
>
> Copy of the letter from the Krakow-Krowodrza Regional Prosecutor’s Office dated 07.03.2018: <a href="/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180307.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-DS_385-14_20180307.pdf</a> (Polish language version, original)
>
> Copy of the response to the letter from the Krakow-Krowodrza Regional Prosecutor’s Office, dated 03.04.2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180404.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_odpowiedz-na-pismo.20180404.zlozone.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 28.02.2018 – 16.03.2018 (February 28, 2018 to March 16, 2018)
> A trip to India, to perform high-resolution magnetic resonance examination of nervous system (neurography) and urinary tract, as well as high-resolution computer tomography examination of urinary tract.
>
> The result of neurography/magnetic resonance of nervous system, clearly indicates nerve damage, e.g. in the area where to this day scar is visible, as well as nerves of genital system.
>
> Copy of the results from magnetic resonance examination of nervous system, conducted on March 12, 2018
> * English language version, original: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 19.02.2018 (February 19, 2018)
> Submission of a notification [of a crime] from the District Prosecutor’s Office in Krakow to the Krakow-Krowodrza Regional Prosecutor’s Office.
>
> Copy of the letter from the District Prosecutor’s Office in Krakow dated 19.02.2018: <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180219.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180219.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 15.02.2018 (February 15, 2018)
> The District Prosecutor’s Office in Krakow dismissed my request for exclusion of prosecutors of the Krakow-Krowodrza Regional Prosecutor’s Office, despite the fact that they grossly exceeded their obligations in proceedings regarding notification of a crime from February 2014 and cases reported by me since then, including concerning the misappropriation of my property and punishable threats sent to me and fulfilled as a result of events of January 2014.
>
> Copy of the letter from the District Prosecutor’s Office in Krakow dated 15.02.2018: <a href="/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180215.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/PO-III_Ko-34.2018_20180215.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 13.02.2018 (February 13, 2018)
> A trip to Vienna, Austria, to carry out diagnostic imaging tests. During this trip, a computed tomography examination was conducted, which clearly indicated the visibility of the scar and internal damage, correctly correlated with the symptoms.
>
> Copy of the results from computed tomography examination of pelvis, conducted on February 13, 2018
> * German language version, original: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf</a><br/>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 16.01.2018 (January 16, 2018)
> Submission of a notification [of a crime] from the Department of Preparatory Proceedings of the National Prosecutor’s Office to the District Prosecutor's Office in Krakow.
>
> Copy of the letter from the National Prosecutor’s Office dated 16.01.2018: <a href="/pliki/Prawne/Odpowiedzi/PK-II_Ko2_128.2018_20180116.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/PK-II_Ko2_128.2018_20180116.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 15.01.2018 – 20.04.2018 (January 15, 2018 to April 20, 2018)
> I conducted comprehensive examinations – including diagnostic imaging and consultations with specialists in nuclear medicine, radiology, neurology and plastic surgery – in order to supplement medical documentation.
>
> Due to erroneous diagnoses being issued repeatedly by physicians in Poland and the refusal to carry out comprehensive examinations and medico-legal opinions on private orders only several months earlier by nearly 20 facilities in Poland, examinations and consultations had to be conducted outside Poland. The stage of sending inquiries to selected facilities, checking the completeness of the offer received, organizing transport and accommodation, conducting examinations, and translating the results of examinations.
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 9.01.2018 (January 9, 2018)
> I submitted notification of a crime at the National Prosecutor’s Office in Warsaw along with the request for exclusion of Krakow-Krowodrza Regional Prosecutor’s Office, which has so far dealt with cases submitted by me and in each and every case nothing has been done at all – either in the case concerning events which resulted in permanent bodily injury or the case concerning misappropriation of my property and punishable threats fulfilled as a result of the events of January 2014.
>
> The notification of a crime, due to bodily injury resulting from the January 2014 events being permanent, indicates completely different deeds, establishes a set of new evidence along with medical documentation, from which it is clear that the decision refusing to initiate investigation in 2014 was not only unfounded but also a gross violation of powers on the part of law enforcement authorities. At that time,without appointing a medico-legal expert and performing a forensic examination of any kind, law enforcement authorities issued “medical opinion” that candidiasis is the cause of the permanent bodily injuries, also incorrectly delivered the decision refusing to initiate an investigation by an address box, thus making it impossible to file a timely complaint, as well as failed to accept motion for prosecution despite the fact that 7 days have passed.
>
> Copy of the document “Notification of a crime under Articles 156 §1, 157 §1, 160 §1, 162 §1, 192 §1 and 193 of the Penal Code, as well as violations of Articles 2 („the right to life”), 3 („prohibition of inhuman and degrading treatment”), 6 („the right to a fair trial”) and 14 („the prohibition of discrimination”) of the European Convention on Human Rights”, submitted in person at the Stakeholder Service Office of the National Prosecutor’s Office at Rakowiecka 26/30 in Warsaw on January 9, 2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_zawiadomienie_20180109.zlozone.pdf " target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_zawiadomienie_20180109.zlozone.pdf</a> (Polish language version, original)
>
> Copy of the document “Request for exclusion of prosecutors of the Regional Prosecutor’s Office”, dated 9.01.2018: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_wniosek-o-wylaczenie-prokuratorow.20180109.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-krajowa_wniosek-o-wylaczenie-prokuratorow.20180109.zlozone.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thick-separator" />

## 2017
### 17.11.2017 (November 17, 2017)
> I met with physician who led candidiasis treatment in 2013. The doctor conducted examination, familiarized himself with the results of tests collected so far, categorically denied the claims that the injuries he observed could have been caused by candidiasis or medications taken during the course of treatment.
>
> Copy of affidavit, issued by physician who lead candidiasis treatment in year 2013, stating that candidiasis is not the cause of the observed bodily injuries and damage to genitourinary: <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 30.10.2017 – 19.01.2018 (October 30, 2017 to January 19, 2018)
> In the period from 30 October 2017 to 19 January 2017, I wanted to establish contact with non-governmental organizations (NGOs) in Poland and abroad–all I asked for, and what fits perfectly into the statutes of these organizations, was the recommendation of forensic experts and a lawyer specializing in cases of serious human rights violations.
>
> Neither personal contact in November 2017, when I visited the headquarters of these organizations in London and Brussels, nor e-mail contact, brought any results.
>
> Very much like in the case of contact with medical institutions in Poland, which I had contacted a few months earlier in order to receive an offer to carry out medico-legal opinions and complementary examinations, in 90% of cases I did not receive any response, and the remaining 10% in 90% was a negative answer or information completely detached from reality.
>
> More information in "Documentation to download" section ("Non-Governmental Organizations (NGOs)" subsection): <a href="/documentation/index.html#non-governmental-organizations-ngos" target="_blank">/documentation/index.html#non-governmental-organizations-ngos</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 22.06.2017 – 16.01.2018 (June 22 to January 16, 2018)
> Nearly 20 facilities in Poland – issuing medico-legal opinions on private orders and cooperating with medical centers, having access to specialist physicians and appropriate equipment necessary to perform required examinations in order to give a comprehensive diagnosis and to prepare a medico-legal opinion – refused to provide their services. The stage of selecting facilities,framing analytic questions and contents of inquiry, sending inquiries to selected facilities – first in person, then through an attorney – as well as checking the completeness of the offer.
>
> In view of the above – which, it is worth noting, is not first time that this  happened, because since 2014 physicians in Poland have repeatedly, I do not know on what basis, refused to provide their services or ordered incorrect examinations or gave erroneous diagnoses – comprehensive examinations and consultations have finally been conducted outside Poland: in the period of January 15,2018 – April 20, 2018, i.e. in less than three months, something that Polish physicians have refused me for almost four years.
>
> More information in "Documentation to download" section ("Inquiries regarding conducting examinations and medico-legal opinion in Poland (2017)" subsection): <a href="/documentation/#inquiries-regarding-conducting-examinations-and-medico-legal-opinion-in-poland-2017" target="_blank">/documentation/#inquiries-regarding-conducting-examinations-and-medico-legal-opinion-in-poland-2017</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 17.06.2017 – 17.08.2017 (June 17 to August 17, 2017)
> I ordered certification of SMS messages which I received immediately before and after the events of January 2014, i.e. in the period from 18/12/2013 to 30/03/2014. The service was provided by an IT forensics company that provides such services to both private parties and law enforcement authorities. The stage of selecting, sending inquiries to selected companies, checking the completeness of the offer received, clarifying the order, delivering the phones, completing the order, receiving the phones and certified copies.
>
> Certified copy of SMS messages incoming from / outgoing to number +48721030078, in the period from 04/03/2014 to 30/03/2014: <a href="/pliki/Komunikacja/GT-E1200_721030078_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1200_721030078_raport.pdf</a>
>
> Certified copy of SMS messages incoming from / outgoing to number +48512355495, from December 28, 2013, a few days before the described events: <a href="/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf</a>
>
> Certified copy of SMS messages incoming from / outgoing to number +48503990172, in the period from 18/12/2013 to 14/01/2014: <a href="/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf</a>
>
> Protocol of securing SMS communication which took place in the period between December 2013 and end of March 2014, performed by IT forensics specialists in August 2017, along with invoice and acceptance protocol: <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 22.03.2017 – 29.03.2017 (March 22, 2017 to March 29, 2017)
> I granted the power of attorney and commissioned the examination of the case file concerning the notification [of a crime] submitted on 28.02.2014. It is clear from the case file that the law enforcement authorities did absolutely nothing, while unlawfully exceeding their powers – law enforcement authorities have no right to issue medico-legal opinions without appointing a medico-legal expert and performing a forensic examination – by issuing a “medical diagnosis” based on a Wikipedia article, i.e. that supposedly bodily injuries were caused by candidiasis of the digestive system.
>
> Copy of the decision refusing to initiate an investigation pursuant to art. 17. § 1 point 1 of the Code of Penal Procedure, on the grounds that candidiasis is the cause of the observed permanent bodily injuries, dated 14.04.2014: <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a> (Polish language version, original)
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thick-separator" />

## 2016
### 16.01.2016 – 29.06.2016 (January 16 to June 29, 2016)
> One of the recommendations of the reconstructive surgeon of genitourinary, whom I met with at the beginning of January in Belgrade, Serbia, was to consult a neuro-urologist (i.e. physician specialized in neurology and urology). Unfortunately, since my return from Belgrade, despite many months of searching, I did not find such a specialist – either because of staff shortages, or inability to provide a cost estimate for consultation and urodynamic examination by 2-3 facilities which had such specialists on their teams.
>
> Due to previous situations, where even with a clearly defined cost estimate and scope of services, I was deceived – as in Prague at the end of 2015 – I wanted to protect myself as best as possible. The stage of selecting facilities, preparing an inquiry, sending inquiries to selected facilities, checking the completeness of the offer received, particularization.
>
> Copy of the demand for payment letter in connection with refusing to carry out medical tests, dated 28.01.2018
> * Czech language version, original: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf</a>
> * Polish version, translation: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 05.01.2016 – 15.01.2016 (January 5, 2016 to January 15, 2016)
> Visit to Belgrade, Serbia, for a consultation with a renowned reconstructive surgeon of genitourinary, combined with, inter alia, ultrasound examination, which once again showed clear symptoms of neurogenic bladder. One of the recommendations I received during this visit was to consult a neuro-urologist (i.e. physician specialized in neurology and urology).
>
> Copy of the results from ultrasound examination of urinary tract (images, description), conducted on January 11, 2016
> * English language version, original: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thick-separator" />

## 2015
### 13.12.2015 – 14.12.2015 (December 13 to December 14, 2015)
> Visit to Prague, Czech Republic, to perform comprehensive examinations – including urodynamic, magnetic resonance, Doppler ultrasound of blood flow in genitals, sperm tests. Despite previous arrangements, having fulfilled the requirement of paying the entire amount of 1250 EUR by bank transfer before arriving in Prague, also transport and accommodation costs incurred, arriving on time at the medical facility where the tests were to be carried out, at the last minute, already on the spot, without giving any reason, I was refused to have the planned, and paid for, tests done.
>
> I handed over the case to a law firm in Cesky Tesin, in order to enforce the claim, so I can focus on conducting the necessary examinations at a different facility.
>
> I contacted a reconstructive surgeon of genitourinary in Belgrade, Serbia – one of the other pre-selected facilities – and set up a consultation for  January 11, 2016.
>
> Copy of the demand for payment letter in connection with refusing to carry out medical tests, dated 28.01.2018
> * Czech language version, original: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf</a>
> * Polish version, translation: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 4.11.2015 – 11.11.2015 (November 4, 2015 to November 11, 2015)
> I ordered sworn translations of the collected results of examinations in order to conduct comprehensive testing outside Poland. The stage of selecting, preparing an inquiry, sending inquiries to selected companies, checking the completeness of the offer, placing order, delivery of originals, receiving translations.
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 17.09.2015 – 08.10.2015 (September 17, 2015 to October 8, 2015)
> In order to properly collect the evidence and submit a new notification of a crime, I consulted a detective – former police officer of the criminal department.
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 20.06.2015 – 1.12.2015 (June 20, 2015 to December 1, 2015)
> In order to conduct comprehensive examinations outside Poland – including urodynamic, magnetic resonance, Doppler ultrasound of blood flow in genitals, sperm tests – because of repeated refusal to conduct examinations, ordering incorrect examinations or giving erroneous diagnoses by Polish physicians – I started to look for medical facilities with appropriate equipment and specialists. The stage of selecting, preparing an inquiry, sending inquiries to selected facilities, checking the completeness of the offer received, organizing transport and accommodation, conducting examinations.
>
> Unfortunately, I was deceived by facility I selected. Despite previous arrangements, having fulfilled the requirement of paying the entire amount of 1250 EUR by bank transfer before arriving in Prague, also transport and accommodation costs incurred, arriving on time at the medical facility where the tests were to be carried out, at the last minute, already on the spot, without giving any reason, I was refused to have the planned, and paid for, tests done.
>
> Copy of the demand for payment letter in connection with refusing to carry out medical tests, dated 28.01.2018
> * Czech language version, original: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.oryginal.pdf</a>
> * Polish version, translation: <a href="/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/EPS_Lotterova_Przedsadowe-wezwanie-do-zaplaty_20160128.tlumaczenie.PL.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thick-separator" />

## 2014

### 12.12.2014 (December 12, 2014)
> Misdiagnoses committed by Krakow doctors have taken place many times since the events of January 2014–this time during the magnetic resonance imaging (MRI) examination of pelvis at the iMed24 Medical Center in Krakow in December 2014, where I was referred by an urologist to conduct a comprehensive pelvic examination including kidneys, but the DICOM data and the report I received from this examination concern only the "lower pelvis" and the volume of DICOM data is about 1/10 of the data I received from another MRI examination of pelvis, where scarring in the left groin is clearly visible.
>
> Referral for magnetic resonance imaging (MRI) of PELVIS (entire pelvis, including the kidneys and scars in the groin area), dated 20.11.2014: <a href="/pliki/Medyczne/MHSiemaszko_Uromed_skierowanie_Rezonans-Magnetyczny_20141120.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_Uromed_skierowanie_Rezonans-Magnetyczny_20141120.pdf</a>
>
> Bill for magnetic resonance imaging (MRI) of LOWER PELVIS (EXCLUDING kidneys and scars in the groin area!), conducted in iMed24 Medical Center in Krakow, Poland on December 12, 2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy_rachunek_20141212.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy_rachunek_20141212.pdf</a>
>
> Results from magnetic resonance imaging (MRI) of LOWER PELVIS (EXCLUDING kidneys and scars in the groin area!), conducted in iMed24 Medical Center in Krakow, Poland on December 12, 2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy-mniejszej-nie-calej_BZDURA_raport_20141212.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-iMed24_MRI-miednicy-mniejszej-nie-calej_BZDURA_raport_20141212.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 7.11.2014 (November 7, 2014)
> Because since the events of January 2014 I was unable to work – to the extent that I had to immediately terminate a two-year contract which I had just started four months earlier – I was in a poor financial situation; on June 17, 2014, I signed a several-month long contract for the provision of IT services on-site in London and left Krakow. However, until the time of my departure, I checked the address box regularly, but I did not find neither the decision refusing to initiate an investigation – despite two months having passed since it was issued at that time – nor the first or the second advice note.
>
> Only after returning to Krakow, on November 7, 2014, I found an envelope with the decision refusing to initiate an investigation, already delivered via address box, therefore I was unable to file a timely compliant. Having set aside financial resources to conduct examinations, which – apart from ultrasound examination of urinary tract – I was unable to do after the events of January 2014, I wanted to conduct, among other things, proper urology exam, magnetic resonance examination taking into account urinary tract and scar area, as well as urodynamic test. Unfortunately, once again, erroneously diagnosing or even absurdly refusing to conduct examinations, resulted in the fact that I no longer consulted physicians in Poland to conduct the necessary examinations; I was strengthened in this conviction when, once again, in 2017, nearly 20 facilities in Poland, offering same services on private orders, refused to provide their services of preparing medico-legal opinion and conducting examinations needed to supplement medical documentation.
>
> By refusing to provide medical assistance or erroneously diagnosing, Polish physicians put me at risk of deterioration of my health condition; examinations which for so long I was unable to conduct in Poland, I finally conducted within less than 3 months in private medical facilities outside Poland.
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 05.11.2014 (November 5, 2014)
> Already in November 2014, information that in January 2014 I was subjected to a medical procedure–in the flat I rented at ul. Szymanowski 5/10 in Krakow, and for which I never gave my consent–was known even to Mr. Tomasz Gibas, from whom on November 5, 2014 I received an email from which it appears that I was subjected to a surgery which was not successful and for which Mr. Gibas offered me to enforce compensation.
>
> E-mail message received from Mr. Tomasz Gibas on November 5, 2014 regarding the alleged “failed surgery”: <a href="/pliki/Komunikacja/MHSiemaszko_Email_Compensation-for-failed-surgery.20141105.pdf" target="_blank">/pliki/Komunikacja/MHSiemaszko_Email_Compensation-for-failed-surgery.20141105.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 14.04.2014 (April 14, 2014)
> The Krakow-Krowodrza Regional Prosecutor’s Office issued a decision refusing to initiate an investigation in connection with events of January 2014; the decision was issued pursuant to art. 17. para. 1 subpara. 1 of the Code of Penal Procedure – i.e. that crime did not take place. Moreover, supposedly the permanent bodily injury resulting from these events was caused by candidiasis of the digestive system. The absurdity of this justification and the fact that this decision has not been delivered to me correctly to this day, as well as the fact that the same prosecutor, conducting proceedings regarding the new notification of a crime submitted by me on January 9, 2018, committed further gross violations of obligations, only strengthens in conviction that a deliberate cover-up of crime was and is taking place:
>
> - Law enforcement authorities have no right to issue medico-legal opinions without appointing a medico-legal expert and performing a forensic examination, an neither prosecutor Bartlomiej Legutko nor senior constable Daria Curzydło, who were conducting pre-investigation regarding notification of a crime submitted on 28.02.2014, had any special knowledge to base their decision refusing to initiate an investigation on the grounds that candidiasis is the cause of the observed permanent bodily injuries. Because candidiasis cannot be the cause of damage to genitourinary, nervous system as well as scars left from puncture wounds in the groin area, it cannot be the cause of pain in perineum, pain in abdomen, neurogenic bladder nor problems with urination – i.e. all of the symptoms which occurred overnight, as a result of events of January 2014 – and for law enforcement authorities to be able to say anything, forensic doctor must be appointed and a forensic examination must be carried out, along with other necessary examinations;
> - The decision refusing to initiate an investigation, being a procedural document in criminal proceedings, was incorrectly delivered to me by an address box – due to the fact that I submitted notification of a crime, I checked the address box regularly until I left for several months on June 15, 2014, yet I found an envelope with the decision in address box only in November 2014, without being served any advice notes, neither first nor second, and not collecting it in person, therefore I was unable to file a timely compliant. Obviously, it would be completely against logic and my interest, knowing that the decision was issued and delivered correctly, not to react in order to appeal it on time;
>
> Copy of the decision refusing to initiate an investigation pursuant to art. 17. § 1 point 1 of the Code of Penal Procedure, on the grounds that candidiasis is the cause of the observed permanent bodily injuries, dated 14.04.2014: <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a> (Polish language version, original)
>
> Copy of affidavit, issued by physician who lead candidiasis treatment in year 2013, stating that candidiasis is not the cause of the observed bodily injuries and damage to genitourinary:  <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a> (Polish language version, original)
>
> Copy of the medical report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018
> * English language version, original: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
>
> Copy of the results from ultrasound examination of nervous system, conducted on April 6, 2018
> * English language version, original: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
>
> Copy of the results from magnetic resonance examination of nervous system, conducted on March 12, 2018
> * English language version, original: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf</a>
>
> Copy of the results from computed tomography examination of pelvis, conducted on February 13, 2018
> * German language version, original: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf</a><br/>
>
> Copy of the results from ultrasound examination of urinary tract (images, description), conducted on January 11, 2016
> * English language version, original: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf</a>
>
> Copy of the results from ultrasound examination of urinary tract (images, description), conducted on January 29, 2014, immediately after the events
> * English language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>
> * Polish language version, original: <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 21.03.2014 (March 21, 2014)
> I personally appeared in the Krakow-Srodmiescie Regional Prosecutor’s Office and testified under oath regarding the events of January 2014, in connection with the notification of a crime submitted on February 28, 2014.
>
> Copy of the summons to appear for testimony regarding notification of a crime, dated 11.03.2014: <a href="/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 15.03.2014 (March 15, 2014)
> I received summons to appear for testimony in the Krakow-Srodmiescie Regional Prosecutor’s Office in connection with notification of a crime  regarding the events of January 2014, delivered to the address of the address box in Krakow. I checked the address box regularly since filing notification of a crime.
>
> Copy of the summons to appear for testimony regarding notification of a crime, dated 11.03.2014: <a href="/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/Ko-311_14.11-03-2014.wezwanie.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 04.03.2014 – 30.03.2014 (March 4, 2014 to March 30, 2014)
> A few weeks after the events of January 2014, in February 2014, Ms. Monika H. contacted me, claiming several times (during personal meetings, as well as in certified SMS communication, as in the attached report) that she “is my woman” and “loves me”. I have never known this person before and never felt nor feel any affection towards her; our contact, initiated by her, was solely physical.
>
> Ms. Monika H. behaved strangely, she seemed to be mentally unbalanced. Taking into account her statements during our meetings and in certified SMS communication, but most of all her mental state during meetings – i.e. dullness, fear, tears in her eyes several times – I suspect that she was involved in the events of January 2014.
>
> Fragment of communication on Facebook Messenger with Monika Hudyka, when on 3 February 2014 she initiated contact with me: <a href="/pliki/Komunikacja/Screenshot_20181219-172035_Messenger.pdf" target="_blank">/pliki/Komunikacja/Screenshot_20181219-172035_Messenger.pdf</a>
>
> SMS messages incoming from / outgoing to number +48721030078, in the period from 04/03/2014 to 30/03/2014, certified by IT forensics specialists in August 2017: <a href="/pliki/Komunikacja/GT-E1200_721030078_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1200_721030078_raport.pdf</a>
>
> Protocol of securing SMS communication which took place in the period between December 2013 and end of March 2014, performed by IT forensics specialists in August 2017, along with invoice and acceptance protocol: <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 03.01.2014 (March 1, 2014)
> Despite the willingness to continue rental of the flat at  Szymanowskiego 5/10 in Krakow – at least until trace protection – due to Landlord’s lack of consent to install additional security measures in the form of a chain to the front door and repeated aggressive behavior on the part of the Landlord, I terminated the agreement on March 1, 2014, moving out.
>
> Copy of the termination of rental agreement for the flat at Szymanowskiego 5/10 in Krakow, dated 1.03.2014: <a href="/pliki/Wynajem/Szymanowskiego_umowa_wypowiedzenie.pdf" target="_blank">/pliki/Wynajem/Szymanowskiego_umowa_wypowiedzenie.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 28.02.2014 (February 28, 2014)
> In connection with the events of January 2014 in the rented flat at Szymanowskiego 5/10 in Krakow, the lawyer with whom I worked at that time prepared and submitted a notification of a crime.
>
> Copy of the notification of a crime under art. 197 §1 of the Penal Code, dated 28.02.2014: <a href="/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zawiadomienie_20140228.zlozone.pdf" target="_blank">/pliki/Prawne/Zlozone/mhs_prokuratura-rejonowa_zawiadomienie_20140228.zlozone.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 11.02.2014 (February 11, 2014)
> Due to my health condition, in connection with the events of January 2014, I had to immediately terminate a two-year contract, which I had just started four months earlier; for many months I was unable to work.
>
> Copy of the termination of the cooperation agreement with “Zerochaos” company, dated 11.02.2014: <a href="/pliki/Wspolpraca/Zerochaos_wypowiedzenie-umowy.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_wypowiedzenie-umowy.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 29.01.2014 (January 29, 2014)
> Due to worsening problems with urination, pain in the lower abdomen, pain in urethra during urination, pain in groin and perineum, on January 29, 2014, I conducted an ultrasound of urinary tract at a private healthcare provider in Krakow, which already then showed symptoms of neurogenic bladder and severe urine retention, nearly 400 ml.
>
> For the symptoms – i.e. problems with urination, pain in the lower abdomen, pain in urethra during urination, pain in groin and perineum – described during visits on January 16 and 22, 2014, at a different private healthcare provider in Krakow, where I had insurance at that time, physicians ordered tests and medicines which had absolutely nothing to do with diagnosing and treating these ailments. For example, “gastroduodenoscopy”, instead of examinations, which in the case of worsening of the symptoms I clearly described during visits, must be conducted in order to give a proper diagnosis, i.e. ultrasound of the urinary tract, Doppler ultrasound of blood flow in genitals, urodynamic tests, and urologist / andrologist exam.
>
> With regard to the other examinations, including forensic examination, I was informed by a lawyer with whom I cooperated at that time, that after submitting notification of a crime, a forensic expert will be appointed to conduct these.
>
> Copy of the results from ultrasound examination of urinary tract (images, description), conducted on January 29, 2014, immediately after the events
> * English language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>
> * Polish language version, original: <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 22.01.2014 (January 22, 2014)
> Misdiagnoses committed by Krakow doctors have taken place many times since the events of January 2014–once again during the second visit to the Medicover Medical Center in Krakow–during which doctors ordered examinations and medicines that had nothing to do with the symptoms described, i.e. pain in the lower abdomen, pain in urethra during urination, pain in groin and perineum, problems with urination,pain in the final opening of the digestive tract.
>
> Documentation regarding visit to the Medicover Medical Center in Krakow, Poland, on January 22 2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_22-01-2014.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_22-01-2014.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />


### 16.01.2014 (January 16, 2014)
> Misdiagnoses committed by Krakow doctors have taken place many times since the events of January 2014–first time during the visit to the Medicover Medical Center in Krakow–during which doctors ordered examinations and medicines that had nothing to do with the symptoms described, i.e. pain in the lower abdomen, pain in urethra during urination, pain in groin and perineum, problems with urination,pain in the final opening of the digestive tract.
>
> Documentation regarding visit to the Medicover Medical Center in Krakow, Poland, on January 16 2014: <a href="/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_16-01-2014.pdf" target="_blank">/pliki/Medyczne/MHSiemaszko_CM-Medicover_wizyta_16-01-2014.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />


### 9.01.2014 (January 9, 2014)
> On the night of January 8-9, 2014, I was assaulted in a rented flat at Szymanowskiego 5/10 in Krakow, which resulted in permanent bodily injury, including genitourinary and nervous system damage, as well as scars left from puncture wounds in the groin area.
>
> Set of medical documentation collected since these events unquestionably confirms the action of 3rd parties, categorically denying the claim that candidiasis of the digestive system – which was given as an alleged cause of bodily injuries by Krakow law enforcement authorities – is the cause of the observed bodily injuries and damage to the genitourinary and nervous system, as well as scars left from puncture wounds in the groin area.
>
> Copy of the decision refusing to initiate an investigation pursuant to art. 17. § 1 point 1 of the Code of Penal Procedure, on the grounds that candidiasis is the cause of the observed permanent bodily injuries, dated 14.04.2014: <a href="/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf" target="_blank">/pliki/Prawne/Odpowiedzi/2-Ds_385-14.17-04-2014.postanowienie-prokuratury.pdf</a> (Polish language version, original)
>
> Copy of affidavit, issued by physician who lead candidiasis treatment in year 2013, stating that candidiasis is not the cause of the observed bodily injuries and damage to genitourinary: <a href="/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_zaswiadczenie_20171117.pdf</a> (Polish language version, original)
>
> Copy of the medical report from examinations and consultations with physician specializing in neurology and plastic surgery, conducted on April 6 and 20, 2018
> * English language version, original: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Konsultacja_Neurolog&ChirurgPlastyczny_Millesi-Center-Vienna_20180420.tlumaczenie.PL.pdf</a>
>
> Copy of the results from ultrasound examination of nervous system, conducted on April 6, 2018
> * English language version, original: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_PUC-Vienna_20180413.tlumaczenie.PL.pdf</a>
>
> Copy of the results from magnetic resonance examination of nervous system, conducted on March 12, 2018
> * English language version, original: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_MR-Neurography-of-Pelvis_Mahajan-Imaging_20180312.tlumaczenie.PL.pdf</a>
>
> Copy of the results from computed tomography examination of pelvis, conducted on February 13, 2018
> * German language version, original: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_CT_Radiology-Center-Vienna_20180213.tlumaczenie.PL.pdf</a><br/>
>
> Copy of the results from ultrasound examination of urinary tract (images, description), conducted on January 11, 2016
> * English language version, original: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.oryginal.pdf</a>
> * Polish language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf" target="_blank">/pliki/Medyczne/Badania_USG_St-Medica_20160111.tlumaczenie.PL.pdf</a>
>
> Copy of the results from ultrasound examination of urinary tract (images, description), conducted on January 29, 2014, immediately after the events
> * English language version, sworn translation: <a href="/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.tlumaczenie.EN.pdf</a>
> * Polish language version, original: <a href="/pliki/Medyczne/Badania_USG_20140129.pdf" target="_blank">/pliki/Medyczne/Badania_USG_20140129.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thick-separator" />

## 2013
### 28.12.2013 (December 28, 2013)
> I received an SMS with punishable threats “(…) ***may you never have any offspring*** (…)”, fulfilled a few days later as a result of the events of January 2014 (permanent bodily injury, including genitourinary and nervous system damage, as well as scars left from puncture wounds in the groin area).
>
> Certified copy of SMS messages incoming from / outgoing to number +48512355495, from December 28, 2013, a few days before the described events: <a href="/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_512355495_raport.pdf</a>
>
> Protocol of securing SMS communication which took place in the period between December 2013 and end of March 2014, performed by IT forensics specialists in August 2017, along with invoice and acceptance protocol: <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 18.12.2013 – 14.01.2014 (December 18, 2013 to January 14, 2014)
> I received a series of SMSs from, as it turned out, a person who impersonated someone else – before I realized that it was a complete stranger, I gave my address of residence of that time, i.e.  Szymanowskiego 5/10 in Krakow.
>
> Certified copy of SMS messages incoming from / outgoing to number +48503990172, in the period from 18/12/2013 to 14/01/2014: <a href="/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf" target="_blank">/pliki/Komunikacja/GT-E1080W_503990172_raport.pdf</a>
>
> Protocol of securing SMS communication which took place in the period between December 2013 and end of March 2014, performed by IT forensics specialists in August 2017, along with invoice and acceptance protocol: <a href="/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf" target="_blank">/pliki/Komunikacja/MediaRecovery_zamowienie_201707.SIGNED.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 16.12.2013 – 18.12.2013 (December 16, 2013 to December 18, 2013)
> I was on a two-day delegation in Zurich, Switzerland, in connection with the implementation of a new project.
>
> Copy of the documentation regarding the delegation to Zurich, Switzerland:
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_tam.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot_boarding-pass_powrot.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_wycena.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_przelot.pdf</a>,
> * <a href="/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_delegacja-122013_hotel.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 12.12.2013 (December 12, 2013)
> The third visit to physician in connection with treatment of candidiasis of the digestive system – the only ailment until the events of January 2014.
>
> Diagnoses and recommendations issued by physician who lead candidiasis treatment in year 2013, during visits in May, November and December 2013: <a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 13.11.2013 (November 13, 2013)
> The second visit to physician in connection with treatment of candidiasis of the digestive system – the only ailment until the events of January 2014.
>
> Diagnoses and recommendations issued by physician who lead candidiasis treatment in year 2013, during visits in May, November and December 2013: <a href="/pliki/Medyczne/Kandydoza_Intermed_2013.pdf" target="_blank">/pliki/Medyczne/Kandydoza_Intermed_2013.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 6.10.2013 (October 6, 2013)
> I rented a flat at Szymanowskiego 5/10 in Krakow, in connection with signing of a two-year contract for the provision of IT services.
>
> Copy of the rental agreement for the flat at Szymanowskiego 5/10 in Krakow: <a href="/pliki/Wynajem/Szymanowskiego_umowa.pdf" target="_blank">/pliki/Wynajem/Szymanowskiego_umowa.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 19.09.2013 (September 19, 2013)
> I signed a two-year contract for the provision of IT services for a company in Krakow.
>
> Copy of the cooperation agreement with “Zerochaos” company, dated 19.09.2013: <a href="/pliki/Wspolpraca/Zerochaos_umowa.pdf" target="_blank">/pliki/Wspolpraca/Zerochaos_umowa.pdf</a>
>

<p class="back-to-toc"><a href="#toc" title="Back to table of contents">Back to table of contents</a></p>

<hr class="thin-separator" />

### 08.2013 (August, 2013)
> Video interview recorded in August 2013, about **5 months before** the events of January 2014: <a href="/other/2019/07/23/video-interview-from-august-2013.html" target="_blank">/other/2019/07/23/video-interview-from-august-2013.html</a>
>
